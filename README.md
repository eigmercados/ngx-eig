# NgxEig

Este projeto foi gerado pelo [Angular CLI](https://github.com/angular/angular-cli) version 6.1.4.

A ideia deste projeto é disponibilizar o framework de desenvolvimento dos nossos projetos a partir da biblioteca
eig.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Rode o comando `npm rum package`. Este comando compilará o projeto e o compactará no arquivo `dist/eig-xxx.tgz`.

## Utilizando a biblioteca

##### Importando as dependências

É necessário a inclusão da biblioteca como dependência da aplicação, para isso altere o arquivo `package.json` incluindo a biblioteca como dependência.


`"eig": "0.0.0"` *0.0.0 é a versão da biblioteca* 

A aplicação que utilizará a biblioteca precisa, **obrigatoriamente**, importar **TODAS** as dependências que a biblioteca exige.
Segue a lista de dependências necessárias:

```
"dependencies": {
    "@angular-devkit/build-ng-packagr": "^0.10.2",
    "@angular/animations": "^6.1.7",
    "@angular/common": "^6.1.7",
    "@angular/compiler": "^6.1.7",
    "@angular/core": "^6.1.7",
    "@angular/forms": "^6.1.7",
    "@angular/http": "^6.1.7",
    "@angular/platform-browser": "^6.1.7",
    "@angular/platform-browser-dynamic": "^6.1.7",
    "@angular/router": "^6.1.7",
    "@swimlane/ngx-datatable": "13.1.0",
    "bootstrap": "3.3.7",
    "core-js": "^2.5.4",
    "cp": "^0.2.0",
    "crypto-js": "^3.1.9-1",
    "eig": "0.0.0",
    "hammerjs": "^2.0.8",
    "jquery": "3.2.1",
    "jquery.scrollbar": "0.2.11",
    "moment": "^2.22.2",
    "ng2-file-upload": "1.3.0",
    "ngx-bootstrap": "3.0.1",
    "ngx-cookie-service": "1.0.10",
    "ngx-currency": "^1.5.0",
    "ngx-mask": "6.1.3",
    "ngx-restangular": "3.0.0",
    "ngx-store": "2.0.0",
    "rxjs": "^6.3.2",
    "sweetalert": "2.1.0",
    "zone.js": "~0.8.26"
  },
  "devDependencies": {
    "@angular-devkit/build-angular": "^0.7.5",
    "@angular/cli": "^6.1.5",
    "@angular/compiler-cli": "^6.1.7",
    "@angular/language-service": "^6.1.7",
    "@types/crypto-js": "3.1.37",
    "@types/jasmine": "~2.8.6",
    "@types/jasminewd2": "~2.0.3",
    "@types/jquery": "^3.3.2",
    "@types/lodash": "^4.14.109",
    "@types/node": "~8.9.4",
    "codelyzer": "~4.2.1",
    "jasmine-core": "~2.99.1",
    "jasmine-spec-reporter": "~4.2.1",
    "karma": "~1.7.1",
    "karma-chrome-launcher": "~2.2.0",
    "karma-coverage-istanbul-reporter": "^2.0.4",
    "karma-jasmine": "~1.1.1",
    "karma-jasmine-html-reporter": "^0.2.2",
    "ng-packagr": "^4.3.1",
    "protractor": "^5.4.1",
    "ts-node": "~5.0.1",
    "tslint": "~5.9.1",
    "typescript": "~2.7.2"
  }

```

##### Editando as configurações
Para que tudo funcione, o projeto que estará importando nossa biblioteca precisá alterar o arquivo `angular.json`

No arquivo, procure o caminho `projects.PROJETO.architect.build.options.assets` e adiocione o seguinte item ao array

` {
    "glob": "**/*",
    "input": "node_modules/eig/assets",
    "output": "assets"
  }
`

Altere o objeto `projects.PROJETO.architect.build.options.styles` para

`
"styles": [
              "node_modules/eig/assets/styles.css",
              "node_modules/eig/assets/css/style.css"
            ]
`

Altera o objeto ``projects.PROJETO.architect.build.options.scripts`` para

`
"scripts": [
              "node_modules/jquery/dist/jquery.min.js",
              "node_modules/bootstrap/dist/js/bootstrap.min.js",
              "node_modules/eig/assets/js/HoldOn.js",
              "node_modules/eig/assets/js/sweetalert.js"
            ]
`

Caso sua aplicação use um contexto que não seja `/`, exemplo: `http://localhost:4200/contextoqualquer`,
será necessário verificar o item `projects.PROJETO.architect.build.options.deployUrl`, ele sempre deverá ser `/`

##### Bootstrap da aplicação
Uma vez alterado os arquivos de configurações, precisamos alterar o módulo principal da aplicação para que ela utilize
a estrutura da biblioteca como base.

Altere o arquivo do módulo inicial; geralmente este arquivo é o `src/app/app.module.ts`
Ele terá esta estrutura:

```
    import {BrowserModule} from '@angular/platform-browser';
    import {NgModule} from '@angular/core';
    import {EigAppModule} from 'eig';
    import {environment} from '../environments/environment';
    import {mainRotas} from './app.routes';
    import {ApoioServiceInterno} from './geral/service/apoioService';
    import {Resources} from './resourceRest';
    
    
    @NgModule({
        declarations: [],
        imports: [
            BrowserModule
            , EigAppModule.forRoot(Object.assign({
                clientId: 'scaClientWeb',
                tituloAplicacao: 'SCA - Sistema de Controle de Acessos',
                tituloHeader: 'Sistema de Controle de Acessos',
                rotas: {
                    rotasInternas: mainRotas,
                    rotasSistema: [],
                    rotasMain: []
                },
                resourceRest: Resources,
                apoioService: ApoioServiceInterno,
                scaURL: null
            }, environment))
        ],
        providers: [],
    })
    export class AppModule {
        ngDoBootstrap(app) {
            EigAppModule.doBootstrap(app);
        }
    }

```

Agora é preciso alterar o arquivo `src/index.html`

```
    <!doctype html>
    <html lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="icon" type="image/png" href="favicon.ico">
      <base href="/">
    
    </head>
    <body style="display: unset;margin: unset">
    
    <h1 id="status">
      Carregando ...
    </h1>
    <app-root></app-root>
    </body>
    </html>
```