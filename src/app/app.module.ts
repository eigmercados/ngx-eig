import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {EigAppModule} from '../../projects/eig/src/lib/eig.app.module';
import {EigAppComponent} from '../../projects/eig/src/lib/eig.app.component';
import {environment} from '../environments/environment';

const config = Object.assign({
    clientId: 'scaClientWeb', tituloAplicacao: 'd', tituloHeader: 'kkk', rotas: {
        rotasInternas: [],
        rotasSistema: [],
        rotasMain: []
    },
    resourceRest: null,
    apoioService: null
}, environment);

@NgModule({
    declarations: [
        // AppComponent
    ],
    imports: [
        BrowserModule
        , EigAppModule.forRoot(config)

    ],
    providers: [],
    entryComponents: []
})
export class AppModule {
    ngDoBootstrap(app) {

        // obtain reference to the DOM element that shows status
        // and change the status to `Loaded`
        const statusElement = document.querySelector('#status');
        statusElement.remove();
        // create DOM element for the component being bootstrapped
        // and add it to the DOM
        const componentElement = document.createElement('app-root');
        document.body.appendChild(componentElement);
        app.bootstrap(EigAppComponent);
    }
}
