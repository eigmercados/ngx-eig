// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

const apiURL = 'http://localhost:12311/sca';
// const apiURL = 'https://dev.eigmercados.com.br/sca';

export const environment = {
    production: false,
    siglaSistema: 'SCA',
    apiURL: apiURL,
    apiRest: apiURL + '/rest/',
    oauthUrl: apiURL + '/oauth/',
    scaURL: 'https://dev.eigmercados.com.br/scaweb'
};
