import {Injector, ModuleWithProviders, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AlertComponent} from './geral/alert/alert-component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {EigLoadingComponent} from './geral/directives/eig-loading/eigLoading.component';
import {ModalModule} from 'ngx-bootstrap/modal';
import {EigLoadingService} from './geral/directives/eig-loading/service/eigLoadingService';
import {TranslateModule} from '@ngx-translate/core';

const providers = [
    EigLoadingService
];

@NgModule({
    imports: [
        RouterModule
        , CommonModule
        , FormsModule
        , ModalModule.forRoot()
       /* , TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: CustomTranslateLoader.Translate({locale: 'pt', traducao: pt}, {locale: 'en', traducao: en}, {
                    locale: 'de',
                    traducao: de
                }),
                deps: []
            },
        })*/

    ],
    declarations: [
        AlertComponent
        , EigLoadingComponent
    ],
    exports: [
        AlertComponent
        , CommonModule
        , FormsModule
        , ModalModule
        , EigLoadingComponent
        , TranslateModule
    ],
    entryComponents: []
})
export class SharedModule {
    static injector: Injector = null;

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [...providers]
        };
    }

    constructor(private injector: Injector) {
        SharedModule.injector = injector;
    }
}


