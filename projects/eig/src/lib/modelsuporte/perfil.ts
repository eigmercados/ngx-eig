import {FuncionalidadePerfil} from './funcionalidadePerfil';
import {AbstractCodigoDescricao} from './abstract-codigo-descricao';
import {Validador} from '../geral/decorators/validador.model';
import {SistemaEmpresa} from './sistemaEmpresa';

export class Perfil extends AbstractCodigoDescricao {

    static ADMINISTRADOR: Perfil = Perfil.New(1, 'Administrador');

    static E = [Perfil.ADMINISTRADOR];

    @Validador({
        lookFor: SistemaEmpresa
    })
    sistemaEmpresa: SistemaEmpresa;
    dataCriacao: Date;
    funcionalidadesPerfis: FuncionalidadePerfil[] = [];

    get nomePerfil(): string {
        return this.descricao;
    }

    private static New(codigo: number, descricao: string): Perfil {
        const perfil = new Perfil();
        perfil.codigo = codigo;
        perfil.descricao = descricao;
        return perfil;
    }
}
