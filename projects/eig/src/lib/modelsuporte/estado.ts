import {EntidadeBasica} from './entidadeBasica';

export class Estado extends EntidadeBasica {

  codigo: number;
  sigla: string;
  nome: string;
}
