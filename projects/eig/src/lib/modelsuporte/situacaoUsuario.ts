export class SituacaoUsuario {

    static ATIVO = new SituacaoUsuario('A', 'Ativo');
    static INATIVO = new SituacaoUsuario('I', 'Inativo');
    static PENDENTE = new SituacaoUsuario('P', 'Pendente');

    static E: SituacaoUsuario[] = [SituacaoUsuario.ATIVO, SituacaoUsuario.INATIVO, SituacaoUsuario.PENDENTE];

    codigo: string;
    descricao: string;

    constructor(codigo: string, descricao: string) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    get isInativo(): boolean {
        return SituacaoUsuario.INATIVO.codigo === this.codigo;
    }

    get isPendente(): boolean {
        return SituacaoUsuario.PENDENTE.codigo === this.codigo;
    }

    get isAtivo(): boolean {
        return SituacaoUsuario.ATIVO.codigo === this.codigo;
    }
}
