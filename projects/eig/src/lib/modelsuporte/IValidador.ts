import {ValidatorFn} from '@angular/forms';

export interface IValidador {
  _validadores: CampoValidador[];
}

export interface CampoValidador {
  campo: string;
  lookFor?: any;
  validadores: MensagemValidadores[];
  _campoMensagem?: string;
  _label?: string;
  _construtor?: string;
}

export interface MensagemValidadores {
  mensagem: string;
  validador: ValidatorFn;
}

export interface CampoErroMensagem {
  campo: string;
  mensagem: string;
}

export interface CustomError {
  errorCode: string;
  mensagem: string;
}

