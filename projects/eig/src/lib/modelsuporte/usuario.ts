import {EntidadeBasica} from './entidadeBasica';
import {SituacaoUsuario} from './situacaoUsuario';
import {Pessoa} from './pessoa';
import {Validador} from '../geral/decorators/validador.model';
import {Validators} from '@angular/forms';
import {Perfil} from './perfil';
import {Empresa} from './empresa';
import {UsuarioPerfil} from './UsuarioPerfil';
import {Sistema} from './sistema';
import {EigValidadores} from '../geral/helpers/eig.validadores';

export class Usuario extends EntidadeBasica {

    codigo: number;
    @Validador({validadores: [{validador: Validators.required, mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'}]})
    login: string;

    @Validador({
        validadores: [{
            validador: Validators.required,
            mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'
        }, {validador: Validators.maxLength(8), mensagem: 'O tamanho m\u00E1ximo aceito \u00E9 {0}'}]
    })
    senha: string;
    situacao: SituacaoUsuario;

    @Validador({lookFor: Pessoa})
    pessoa: Pessoa = new Pessoa();

    perfis: Perfil[] = [];
    perfil: Perfil;

    @Validador({
        validadores: [{
            validador: EigValidadores.abstractCodigoValidator,
            mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'
        }]
    })
    empresa: Empresa;

    usuarioPerfis: UsuarioPerfil[];

    sistema: Sistema;

    enviarEmail: boolean;

    private _sistemas: Sistema[];

    set sistemas(value: Sistema[]) {
        this._sistemas = value;
    }

    get sistemas(): Sistema[] {
        if (this._sistemas == null) {
            this._sistemas = [];
            if (this.usuarioPerfis != null) {
                const tmpM = new Map<number, Sistema>();
                this.usuarioPerfis.forEach(usuarioPerfil => {
                    // @ts-ignore
                    const sistema: Sistema = cast(usuarioPerfil.perfilUsuario.sistemaPerfil, Sistema);

                    if (!tmpM.has(sistema.codigo)) {
                        tmpM.set(sistema.codigo, sistema);
                    }
                    if (sistema.perfis == null) {
                        sistema.perfis = [];
                    }
                    sistema.perfis.push(usuarioPerfil.perfilUsuario);
                });

                tmpM.forEach((valor) => {
                    this._sistemas.push(valor);
                });
            }
        }
        return this._sistemas;
    }
}
