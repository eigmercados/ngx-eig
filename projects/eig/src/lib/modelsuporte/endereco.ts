import {EntidadeBasica} from './entidadeBasica';
import {Logradouro} from './logradouro';
import {Pessoa} from './pessoa';
import {Validador} from '../geral/decorators/validador.model';
import {Validators} from '@angular/forms';
import {EigValidadores} from '../geral/helpers/eig.validadores';

export class Endereco extends EntidadeBasica {

    codigo: number;
    @Validador({
        validadores: [{
            validador: EigValidadores.abstractCodigoValidator,
            mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'
        }]
    })
    logradouro: Logradouro;
    pessoa: Pessoa;
    // @Validador({validadores: [{validador: Validators.required, mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'}]})
    descricao: string;
    @Validador({validadores: [{validador: Validators.required, mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'}]})
    numero: string;

}

