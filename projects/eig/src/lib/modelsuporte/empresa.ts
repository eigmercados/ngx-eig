import {EntidadeBasica} from './entidadeBasica';
import {Validador} from '../geral/decorators/validador.model';
import {Validators} from '@angular/forms';

export class Empresa extends EntidadeBasica {

    codigo: number;

    @Validador({validadores: [{validador: Validators.required, mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'}]})
    nome: string;
    @Validador({validadores: [{validador: Validators.required, mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'}]})
    cnpj: string;

    dataCriacao: Date;
    registroAtivo: boolean;
}
