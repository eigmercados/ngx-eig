import {EntidadeBasica} from './entidadeBasica';
import {IRecuperado} from './iRecuperado';
import {Validador} from '../geral/decorators/validador.model';
import {Bairro} from './bairro';
import {Validators} from '@angular/forms';
import {EigValidadores} from '../geral/helpers/eig.validadores';


export class Logradouro extends EntidadeBasica implements IRecuperado {
    codigo: number;

    @Validador({
        validadores: [{
            validador: EigValidadores.abstractCodigoValidator,
            mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'
        }]
    })
    bairro: Bairro;

    @Validador({
        validadores: [{validador: Validators.required, mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'}
            , {validador: Validators.maxLength(150), mensagem: 'Tamanho m\u00E1ximo permitido \u00E9 {0}'}]
    })
    nome: string;
    complemento: string;
    tipo: string;
    nomeSemAcento: string;
    local: string;
    descricaoLocal: string;

    @Validador({validadores: [{validador: Validators.required, mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'}]})
    cep: number;
    dataCriacao: Date;
    dataAtualizacao: Date;

    _recuperado: boolean;

}
