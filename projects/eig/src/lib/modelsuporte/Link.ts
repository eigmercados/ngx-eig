export interface Link {
    url?: string;
    titulo?: string;
    icone?: string;
    tipo?: number;
    disabled?: boolean;
}
