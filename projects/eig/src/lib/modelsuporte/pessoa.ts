import {EntidadeBasica} from './entidadeBasica';
import {TipoPessoa} from './tipoPessoa';
import {Validador} from '../geral/decorators/validador.model';
import {Validators} from '@angular/forms';
import {Endereco} from './endereco';

export class Pessoa extends EntidadeBasica {

    codigo: number;
    @Validador({validadores: [{mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio', validador: Validators.required}]})
    cpfCnpj: string;
    @Validador({validadores: [{mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio', validador: Validators.required}]})
    tipoPessoa: TipoPessoa;
    @Validador({validadores: [{mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio', validador: Validators.required}]})
    nome: string;

    @Validador({
        validadores: [{
            mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio',
            validador: Validators.required
        }]
    })
    telefone: string;

    @Validador({validadores: [{mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio', validador: Validators.required}]})
    email: string;

    @Validador({
        lookFor: Endereco
    })
    endereco: Endereco;

    enderecos: Endereco[] = [];

}
