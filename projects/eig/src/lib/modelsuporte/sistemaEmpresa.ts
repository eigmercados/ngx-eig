import {Sistema} from './sistema';
import {Empresa} from './empresa';
import {Perfil} from './perfil';
import {EntidadeBasica} from './entidadeBasica';
import {Validador} from '../geral/decorators';
import {EigValidadores} from '../geral/helpers/eig.validadores';

export class SistemaEmpresa extends EntidadeBasica {

    codigo: number;

    @Validador({
        validadores: [{
            validador: EigValidadores.abstractCodigoDescricaoValidator,
            mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'
        }]
    })
    sistema: Sistema;

    @Validador({
        validadores: [{
            validador: EigValidadores.abstractCodigoValidator,
            mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'
        }]
    })
    empresa: Empresa;

    perfis: Perfil[];
}
