import {EntidadeBasica} from './entidadeBasica';
import {Perfil} from './perfil';
import {Usuario} from './usuario';
import {SituacaoUsuario} from './situacaoUsuario';

export class UsuarioPerfil extends EntidadeBasica {

    codigo: { codigoUsuario: number, codigoPerfil: number };
    usuario: Usuario;
    perfil: Perfil;
    situacaoUsuario: SituacaoUsuario;
    perfilUsuario: Perfil;

}

