import {HttpResponse} from '@angular/common/http';

export class ArquivoBlob {
    data: Blob;
    nome: string;


    public static getArquivo(resp: HttpResponse<Blob>): ArquivoBlob {
        const arquivo = new ArquivoBlob();
        arquivo.data = resp.body;
        let conteudo = resp.headers.get('content-disposition');
        if (conteudo && conteudo.indexOf('filename') != null) {
            conteudo = conteudo.substring(conteudo.indexOf('=') + 1, conteudo.length);
            arquivo.nome = conteudo;
        }

        return arquivo;
    }
}
