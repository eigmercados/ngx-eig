import {EntidadeBasica} from './entidadeBasica';
import {Validador} from '../geral/decorators/validador.model';
import {Validators} from '@angular/forms';

export abstract class AbstractCodigoDescricao extends EntidadeBasica {

    codigo: any;
    @Validador({validadores: [{mensagem: 'Campo obrigat\u00F3rio', validador: Validators.required}]})
    descricao: string;


    getPropriedadeCodigo(): string {
        return 'codigo';
    }

}
