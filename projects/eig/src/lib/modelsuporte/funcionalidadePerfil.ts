import {EntidadeBasica} from './entidadeBasica';
import {FuncionalidadeAbs} from './funcionalidadeAbs';
import {Perfil} from './perfil';

export class FuncionalidadePerfil extends EntidadeBasica {

    codigo: { codigoFuncionalidade: number, codigoPerfil: number };
    funcionalidade: FuncionalidadeAbs;

    /**
     * atributo usado apenas para trazer a funcionalidade completa e corrigir problemas com o jackson
     */
    funcionalidadeSistema: FuncionalidadeAbs;

    perfil: Perfil;
    ler: boolean;
    editar: boolean;
    excluir: boolean;

    get sigla(): string {
        if (this.funcionalidade) {
            return this.funcionalidade.sigla;
        }
        return null;
    }

    get nomePerfil(): string {
        if (this.perfil) {
            return this.perfil.descricao;
        }
        return null;
    }

}

