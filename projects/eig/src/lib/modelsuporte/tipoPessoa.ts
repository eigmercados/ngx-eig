export class TipoPessoa implements ItipoPessoa {
  static FISICA: ItipoPessoa = new TipoPessoa('F','F\u00EDsica');
  static JURIDICA: ItipoPessoa = new TipoPessoa('J','Jur\u00EDdica');

  static E = [TipoPessoa.FISICA, TipoPessoa.JURIDICA];

  constructor(codigo: string, descricao: string) {
    this.codigo = codigo;
    this.descricao = descricao;
  }

  codigo: string;
  descricao: string;
}

export interface ItipoPessoa {
  codigo: string
  descricao: string;
}
