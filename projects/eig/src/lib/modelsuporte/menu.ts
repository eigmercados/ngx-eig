import {EntidadeBasica} from './entidadeBasica';
import {FuncionalidadeAbs} from './funcionalidadeAbs';

export class Menu extends EntidadeBasica {

    codigo: number;
    descricao: string;
    url: string;
    icone: String;
    menuPai: Menu;
    ordenacao: number;
    ativo: boolean;
    dataCadastro: Date;
    dataDesativacao: Date;

    funcionalidade: FuncionalidadeAbs;

}
