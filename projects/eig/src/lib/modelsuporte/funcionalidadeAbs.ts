import {EntidadeBasica} from './entidadeBasica';

export abstract class FuncionalidadeAbs extends EntidadeBasica {

    codigo: number;
    descricao: string;
    sigla: string;

    ler: boolean;
    editar: boolean;
    excluir: boolean;

    get READ(): FuncionalidadeAbs {
        const tmp = FuncionalidadeAbs.New(this.sigla);
        tmp.sigla = `${tmp.sigla}READ`;
        return tmp;
    }

    get WRITE(): FuncionalidadeAbs {
        const tmp = FuncionalidadeAbs.New(this.sigla);
        tmp.sigla = `${tmp.sigla}WRITE`;
        return tmp;
    }

    get DELETE(): FuncionalidadeAbs {
        const tmp = FuncionalidadeAbs.New(this.sigla);
        tmp.sigla = `${tmp.sigla}DELETE`;
        return tmp;
    }

    get role(): string {
        return this.sigla;
    }

    public static New<E extends FuncionalidadeAbs>(sigla: string): E {

        const tmp = class extends FuncionalidadeAbs {
        };
        const funcionalidade = new tmp();

        funcionalidade.sigla = sigla;
        return <E>funcionalidade;
    }
}
