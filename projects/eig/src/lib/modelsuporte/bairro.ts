import {Municipio} from './municipio';
import {EntidadeBasica} from './entidadeBasica';
import {Validador} from '../geral/decorators/validador.model';
import {Validators} from '@angular/forms';

export class Bairro extends EntidadeBasica {

    codigo: number;

    @Validador({lookFor: Municipio})
    municipio: Municipio;

    @Validador({validadores: [{validador: Validators.required, mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'}]})
    nome: string;


}
