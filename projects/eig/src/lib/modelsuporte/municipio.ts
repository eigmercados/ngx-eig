import {EntidadeBasica} from './entidadeBasica';
import {Estado} from './estado';
import {Validador} from '../geral/decorators/validador.model';
import {EigValidadores} from '../geral/helpers/eig.validadores';

export class Municipio extends EntidadeBasica {

  codigo: number;
  nome: string;

  @Validador({
    validadores: [{
      validador: EigValidadores.abstractCodigoDescricaoValidator,
      mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'
    }]
  })
  estado: Estado;


}
