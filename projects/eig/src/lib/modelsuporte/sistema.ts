import {Validators} from '@angular/forms';
import {FuncionalidadeAbs} from './funcionalidadeAbs';
import {Perfil} from './perfil';
import {SistemaEmpresa} from './sistemaEmpresa';
import {EntidadeBasica} from './entidadeBasica';
import {Validador} from '../geral/decorators';
import {SituacaoUsuario} from './situacaoUsuario';


export class Sistema extends EntidadeBasica {

    codigo: number;

    @Validador({
        validadores: [{
            validador: Validators.required,
            mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'
        }, {validador: Validators.maxLength(10), mensagem: 'O tamanho m\u00E1ximo aceito \u00E9 {0}'}]
    })
    sigla: string;

    @Validador({
        validadores: [{
            validador: Validators.required,
            mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'
        }, {validador: Validators.maxLength(50), mensagem: 'O tamanho m\u00E1ximo aceito \u00E9 {0}'}]
    })
    nome: string;

    @Validador({
        validadores: [{
            validador: Validators.required,
            mensagem: 'O campo {0} \u00E9 obrigat\u00F3rio'
        }, {validador: Validators.maxLength(255), mensagem: 'O tamanho m\u00E1ximo aceito \u00E9 {0}'}]
    })
    descricao: string;
    dataCriacao: Date;

    perfis: Perfil[] = [];
    sistemaEmpresas: SistemaEmpresa[] = [];
    funcionalidades: FuncionalidadeAbs[];

    situacaoUsuario: SituacaoUsuario;
}
