import {Component, Inject, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {EIG_CONFIG, EigConfig} from './eig-config';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class EigAppComponent implements OnInit {

    constructor(@Inject(EIG_CONFIG) private eigConfig: EigConfig, private titleService: Title) {
    }

    ngOnInit(): void {
        this.titleService.setTitle(this.eigConfig.tituloAplicacao);
    }

}
