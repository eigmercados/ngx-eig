import {Inject, Injector, LOCALE_ID, ModuleWithProviders, NgModule} from '@angular/core';

import './geral/utils';

import 'jquery';
import {defineLocale} from 'ngx-bootstrap/chronos';
import {ptBrLocale} from 'ngx-bootstrap/locale';
import {registerLocaleData} from '@angular/common';
import ptBr from '@angular/common/locales/pt';
import {AutenticacaoService} from './geral/services/autenticacao-service';
import {RestangularModule} from 'ngx-restangular';
import {RestangularConfigFactory} from './geral/services/restangularConfigurer';
import {BsLocaleService} from 'ngx-bootstrap/datepicker';
import {Subject} from 'rxjs';
import {CacheService} from './geral/services/CacheService';
import {CookiesStorageService, LocalStorageService, SessionStorageService, WebStorageModule} from 'ngx-store';
import {AlertService} from './geral/alert/alert-service';
import {EIG_CONFIG, EigConfig} from './eig-config';
import {CURRENCY_MASK_CONFIG, CurrencyMaskConfig} from 'ngx-currency/src/currency-mask.config';
import {AppRoutingModule, rotasSistemas} from './app.routes';
import {EigAppComponent} from './eig.app.component';
import {DynamicPathGuard} from './geral/services/DynamicPathGuard';
import {rotasInternas, rotasMain} from './main.rotas';
import {ResourceRestAbs} from './geral/services/resource-rest';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TradutorService} from './geral/services/TradutorService';
import {ApoioServiceAbs} from './geral/services';
import {CustomTranslateLoader} from './CustomTranslateLoader';
import {pt} from './geral/i18n/pt';
import {en} from './geral/i18n/en';
import {de} from './geral/i18n/de';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

defineLocale('pt-br', ptBrLocale);

registerLocaleData(ptBr);

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
    align: 'right',
    allowNegative: true,
    allowZero: true,
    decimal: ',',
    precision: 2,
    prefix: 'R$ ',
    suffix: '',
    thousands: '.',
    nullable: true
};

export function HttpLoaderFactory(httpClient: HttpClient) {
    return new TranslateHttpLoader(httpClient);
}

export const moduloIniciado = new Subject<boolean>();


@NgModule({
    declarations: [
        EigAppComponent
    ],
    imports: [
        BrowserAnimationsModule
        , AppRoutingModule
        // , ErrorsModule
        , RestangularModule.forRoot(RestangularConfigFactory)
        , WebStorageModule
        , TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: CustomTranslateLoader.Translate({locale: 'pt', traducao: pt}, {locale: 'en', traducao: en}, {
                    locale: 'de',
                    traducao: de
                }),
                deps: []
            },
        })
    ],
    exports: [
        EigAppComponent
        , TranslateModule
    ],
    entryComponents: [EigAppComponent]
})
export class EigAppModule {
    static injector: Injector = null;

    static errorListener: Subject<any> = new Subject<any>();

    static sessionTokenObs: Subject<string> = new Subject<string>();

    static alertService: AlertService = null;

    static forRoot(config: EigConfig): ModuleWithProviders {

        rotasInternas.push(...config.rotas.rotasInternas);
        rotasMain.push(...config.rotas.rotasMain);
        rotasSistemas.push(...config.rotas.rotasSistema);

        moduloIniciado.next(true);

        return {
            ngModule: EigAppModule,
            providers: [
                {provide: EIG_CONFIG, useValue: Object.freeze(config)}
                , {provide: LOCALE_ID, useValue: 'pt-PT'}
                , {provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig}
                , {provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig}
                , AutenticacaoService
                , BsLocaleService
                , CacheService
                , AlertService
                , SessionStorageService
                , LocalStorageService
                , CookiesStorageService
                , DynamicPathGuard
                , ResourceRestAbs
                , TradutorService
                , ApoioServiceAbs
            ]
        };
    }

    static doBootstrap(app): void {
        // obtain reference to the DOM element that shows status
        // and change the status to `Loaded`
        const statusElement = document.querySelector('#status');
        statusElement.remove();
        // create DOM element for the component being bootstrapped
        // and add it to the DOM
        const componentElement = document.createElement('app-root');
        document.body.appendChild(componentElement);
        app.bootstrap(EigAppComponent);
    }

    constructor(private injector: Injector, @Inject(EIG_CONFIG) eigConfig: EigConfig) {
        EigAppModule.injector = injector;
        EigAppModule.alertService = injector.get(AlertService);
    }
}
