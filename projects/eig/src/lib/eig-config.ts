import {InjectionToken} from '@angular/core';
import {Route, Routes} from '@angular/router';


export interface EigConfig {
    clientId: string;
    siglaSistema: string;
    apiURL: string;
    apiRest: string;
    oauthUrl: string;
    scaRestApi: string;
    scaWebUrl: string;
    production?: boolean;
    tituloAplicacao: string;
    tituloHeader: string;
    rotas: EigRotas;
    loginRedirect?: LoginRedirect;
}

export const EIG_CONFIG = new InjectionToken<EigConfig>('eig.config');

export interface EigRotas {
    rotasInternas: Routes | Route[];
    rotasMain: Routes | Route[];
    rotasSistema: Routes | Route[];
}

export interface LoginRedirect {
    enviarEmail: boolean;
    url?: string | Function;
    parametros?: Map<string, string>;
}
