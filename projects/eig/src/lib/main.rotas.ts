import {NgModule} from '@angular/core';
import {Route, RouterModule} from '@angular/router';
import {AutenticacaoService} from './geral/services';
import {UsuarioNovoModule} from './componentes/usuario';
import {MainComponent} from './componentes/main/main.component';

export const rotasInternas: Route[] = [];
export const rotasMain: Route[] = [];


const mainRoutes: Route[] = [

    {
        path: 'novo', component: MainComponent,
        children: [
            {
                path: '', loadChildren: lazyModuleLoad(UsuarioNovoModule)
            }, ...(rotasMain != null ? rotasMain : [])
        ]
    },
    {
        path: 'child', component: MainComponent,
        children: rotasMain != null ? rotasMain : []
    },
    {
        path: '', component: MainComponent,
        canActivate: [AutenticacaoService],
        children: rotasInternas != null ? rotasInternas : []
    }

];

@NgModule({
    imports: [RouterModule.forChild(mainRoutes)],
    exports: [RouterModule]
})
export class MainRoutesModule {
}
