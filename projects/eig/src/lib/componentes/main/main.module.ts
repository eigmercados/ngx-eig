import {Injector, NgModule} from '@angular/core';
import {SharedModule} from '../../SharedModule';
import {EigMenuComponent} from '../../geral/directives/eig-menu/eig.menu.component';
import {EigHeaderComponent} from '../../geral/directives/eig-header/eig.header.component';
import {EigMenuService} from '../../geral/directives/eig-menu/service/menuService';
import {MainComponent} from './main.component';
import {EigLoadingComponent} from '../../geral/directives/eig-loading/eigLoading.component';
import {MainRoutesModule} from '../../main.rotas';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {CustomTranslateLoader} from '../../CustomTranslateLoader';
import {pt} from '../login/i18n/pt';
import {en} from '../login/i18n/en';
import {de} from '../login/i18n/de';


@NgModule({
    imports: [
        MainRoutesModule
        , SharedModule.forRoot()
        , TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: CustomTranslateLoader.Translate({locale: 'pt', traducao: pt}, {locale: 'en', traducao: en}, {
                    locale: 'de',
                    traducao: de
                }),
                deps: []
            },
            isolate: false
        })
    ],
    declarations: [
        MainComponent
        , EigMenuComponent
        , EigHeaderComponent
    ],
    entryComponents: [
        EigLoadingComponent
    ],
    exports: [],
    providers: [
        EigMenuService
    ]
})
export class MainAppModule {
    static injector: Injector = null;

    constructor(private injector: Injector) {
        MainAppModule.injector = injector;
    }
}
