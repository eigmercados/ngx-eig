import {Component, OnInit, Renderer2} from '@angular/core';
import {NavigationStart, Router} from '@angular/router';
import {Subject} from 'rxjs';
import {TradutorService} from '../../geral/services/TradutorService';

@Component({
    selector: 'app-home',
    templateUrl: './templates/main.component.html',
    styleUrls: ['./templates/main.component.css'],
    entryComponents: [],
    providers: []
})
export class MainComponent implements OnInit {

    static scriptsCarregados: Subject<boolean> = null;

    private static _credorasUsuario: any[] = [];

    private scripts: string[] = [
        'bootstrap-datepicker.js'
        // , 'bootstrap-select.js'
        // , 'daterangepicker.min.js'
    ];

    private loaded: number;

    private loading: Subject<number>;

    constructor(private tradutorService: TradutorService, router: Router, private renderer: Renderer2) {

        router.events.subscribe(event => {

            if (event instanceof NavigationStart) {
                this.rodaScriptsIniciais();
                MainComponent.scriptsCarregados.next(true);
            }
        });

        MainComponent.scriptsCarregados = new Subject<boolean>();
        this.tradutorService.verificaLinguagem();
    }

    ngOnInit() {

        this.loaded = 0;
        this.loading = new Subject<number>();

        this.loading.asObservable().subscribe(() => {
            if (++this.loaded === this.scripts.length) {
                MainComponent.scriptsCarregados.next(true);
            }
        });

        this.scripts.forEach(js => {
            loadJS(this.renderer, `./assets/js/${js}`).subscribe(value => this.loading.next());
        });
        this.rodaScriptsIniciais();
    }


    private rodaScriptsIniciais(): void {
        setTimeout(function () {
            try {
                $('.input-daterange input').each(function () {
                    // @ts-ignore
                    $(this).datepicker('clearDates');
                });

                const opts = {
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    keepEmptyValues: true
                };
                // @ts-ignore
                $('.date input').not('.dateFuture').datepicker(opts);


                $('.dateFuture input').each(function () {
                    // @ts-ignore
                    $(this).datepicker(Object.assign({endDate: new Date()}, opts));
                });
            } catch (e) {
                console.log(e);
            }
        }, 500);
    }
}
