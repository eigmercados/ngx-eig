export const pt = {
    'login': {
        'escolha_perfil': 'Escolha o Perfil que deseja logar',
        'escolha_sistema': 'Escolha o Sistema que deseja logar',
        'usuario_invalido': 'Usu\u00E1rio ou senha inv\u00E1lidos',
        'msg_tela': 'Sistema Controle de Acesso dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque scelerisque fringilla.',
        'erro': 'Erro',
        'senha': 'Senha',
        'login': 'Login',
        'manter_conectado': 'Manter-se conectado',
        'esqueceu_senha': 'Esqueceu sua senha?',
        'criar_conta': 'Criar conta',
        'definicao_perfil': 'Definição de Perfil'
    }
};
