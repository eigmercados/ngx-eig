export const de = {
    'login': {
        'escolha_perfil': 'Wählen Sie Profil, um zu gelangen',
        'escolha_sistema': 'Wählen Sie das System aus, das Sie betreten möchten',
        'usuario_invalido': 'Ungültiger Benutzer oder Passwort',
        'msg_tela': 'consectetur adipiscing elit. Phasellus scelerisque scelerisque fringilla.',
        'erro': 'Fehler',
        'senha': 'Passwort',
        'login': 'Benutzer',
        'manter_conectado': 'Bleiben Sie in Verbindung',
        'esqueceu_senha': 'Passwort vergessen?',
        'criar_conta': 'registrieren',
        'definicao_perfil': 'Profildefinition'
    }
};
