export const en = {
    'login': {
        'escolha_perfil': 'Choose the Profile you want to log in',
        'escolha_sistema': 'Choose the Application you want to log in',
        'usuario_invalido': 'Invalid user or password',
        'msg_tela': 'sit amet, consectetur adipiscing elit. Phasellus scelerisque scelerisque fringilla.',
        'erro': 'Error',
        'senha': 'Password',
        'login': 'Username',
        'manter_conectado': 'Keep connected',
        'esqueceu_senha': 'Forgot your password?',
        'criar_conta': 'Sign Up',
        'definicao_perfil': 'Choose Profile'
    }
};
