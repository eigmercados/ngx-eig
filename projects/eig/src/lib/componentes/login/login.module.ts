import {NgModule} from '@angular/core';
import {LoginRoutesModule} from './routes/rotas';
import {LoginComponent} from './login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../SharedModule';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';

import {CustomTranslateLoader} from '../../CustomTranslateLoader';
import {pt} from './i18n/pt';
import {en} from './i18n/en';
import {de} from './i18n/de';

@NgModule({
    imports: [
        ReactiveFormsModule
        , LoginRoutesModule
        , SharedModule.forRoot()
        , TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: CustomTranslateLoader.Translate({locale: 'pt', traducao: pt}, {locale: 'en', traducao: en}, {
                    locale: 'de',
                    traducao: de
                }),
                deps: []
            },
            isolate: true
        })
    ],
    declarations: [
        LoginComponent
    ],
    providers: []
})
export class LoginModule {
}
