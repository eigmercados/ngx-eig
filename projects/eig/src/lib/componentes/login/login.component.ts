import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AutenticacaoService} from '../../geral/services/autenticacao-service';
import {Usuario} from '../../modelsuporte/usuario';
import {Sistema} from '../../modelsuporte/sistema';
import {Perfil} from '../../modelsuporte/perfil';
import {EIG_CONFIG, EigConfig} from '../../eig-config';
import {CacheService} from '../../geral/services';
import {TranslateService} from '@ngx-translate/core';
import {TradutorService} from '../../geral/services/TradutorService';
import {isFunction} from 'util';

@Component({
    selector: 'app-login',
    templateUrl: './templates/login.component.html',
    styleUrls: ['../../../assets/css/login.css']
})
export class LoginComponent implements OnInit {

    usuario: string;
    senha: string;
    mensagemErro: string;
    meuForm: FormGroup;
    mostraErro = false;

    usuarioLogado: Usuario;

    sistemaEscolhido: Sistema;
    perfilEscolhido: Perfil;

    url: string;
    sigla: string;

    tituloModal: string;

    private _locale: string;

    constructor(@Inject(EIG_CONFIG) private eigConfig: EigConfig, private autenticacaoService: AutenticacaoService,
                private router: Router, private cacheService: CacheService, private translate: TranslateService,
                private tradutorService: TradutorService) {
        this.usuarioLogado = new Usuario();
        this.url = window.location.href;
        this.sigla = eigConfig.siglaSistema;

        this.mensagemErro = translate.instant('login.usuario_invalido');
    }

    ngOnInit() {
        /*        this.usuario = 'admin';
                this.senha = 'admin';*/
        this.meuForm = new FormGroup({
            usuario: new FormControl(this.usuario, Validators.required),
            senha: new FormControl(this.senha, Validators.required)
        });

        this.verificaLinguagem();
    }

    isLogado(): boolean {
        return this.autenticacaoService.isLogado();
    }

    cancelar(): void {
        this.autenticacaoService.logout();
    }

    escolherSistema(sistema: Sistema): void {
        if (sistema.perfis.length === 1) {
            this.escolherPerfil(sistema.perfis[0]);
        } else {
            this.sistemaEscolhido = sistema;
        }
    }

    escolherPerfil(perfil: Perfil): void {
        this.perfilEscolhido = perfil;
        this.autenticacaoService.setPerfilLogado(this.perfilEscolhido).subscribe(() => {
            // @ts-ignore
            $('#modalLogin').modal('hide');
            this.redirecionarLogado();
        });
    }

    logar() {
        this.autenticacaoService.logar(this.meuForm.controls['usuario'].value, this.meuForm.controls['senha'].value).subscribe(resp => {
            // this.loading.close();
            if (resp != null) {
                this.usuarioLogado = cast(resp, Usuario);

                if (this.usuarioLogado.sistemas.length > 0) {
                    if (this.usuarioLogado.sistemas.length === 1) {
                        if (this.usuarioLogado.sistemas[0].perfis.length > 1) {
                            this.tituloModal = this.translate.instant('escolha_perfil');
                            // @ts-ignore
                            $('#modalLogin').modal('show');
                        } else {
                            this.redirecionarLogado();
                        }
                    } else {
                        this.tituloModal = this.translate.instant('escolha_sistema');
                        // @ts-ignore
                        $('#modalLogin').modal('show');
                    }
                }

                this.mostraErro = false;

                this.tradutorService.trocouLocale.next(this.locale);
            }
        }, err => {
            // console.log(err);
            this.mostraErro = true;
            // this.alertService.erro('Usu\u00E1rio ou senha inv\u00E1lidos');
        });
    }

    private redirecionarLogado(): void {
        this.router.navigate(['/main/home'], {preserveQueryParams: false, preserveFragment: false});
    }

    setLocale(locale: string) {
        this._locale = locale;

        this.translate.setDefaultLang('pt');
        this.translate.use(locale);
        this.cacheService.deleteCookie('localeInfo');
        this.cacheService.addCookie('localeInfo', locale, new Date().add(1, 'year'));
    }

    get locale(): string {
        return this._locale;
    }

    private verificaLinguagem() {
        const cookie: string = this.cacheService.getCookie('localeInfo');
        if (cookie != null) {
            this.setLocale(cookie);
        } else {
            this.setLocale('pt');
        }
    }

    redirecionarParaRegistroNovo() {
        let url = `${this.eigConfig.scaWebUrl}/main/novo?sg=${this.sigla}`;
        if (this.eigConfig.loginRedirect != null) {
            if (this.eigConfig.loginRedirect.url != null) {
                if (isFunction(this.eigConfig.loginRedirect.url)) {
                    url += `&from=${encodeURIComponent((<Function>this.eigConfig.loginRedirect.url)())}`;
                } else if (this.eigConfig.loginRedirect.url.length > 0) {
                    url += `&from=${encodeURIComponent(<string>this.eigConfig.loginRedirect.url)}`;
                }
            } else {
                url += `&from=${encodeURIComponent(this.url)}`;
            }

            this.eigConfig.loginRedirect.parametros.forEach((value, key) => {
                url += `&${key}=${value}`;
            });
        } else {
            url += `&from=${encodeURIComponent(this.url)}`;
        }
        window.location.href = url;
    }
}
