import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ComponentHelper} from '../../geral/helpers/componentHelper';
import {Usuario} from '../../modelsuporte/usuario';
import {Pessoa} from '../../modelsuporte/pessoa';
import {Municipio} from '../../modelsuporte/municipio';
import {Estado} from '../../modelsuporte/estado';
import {Bairro} from '../../modelsuporte/bairro';
import {AbstractControl, Validators} from '@angular/forms';
import * as _ from 'lodash';
import {Perfil} from '../../modelsuporte/perfil';
import {SituacaoUsuario} from '../../modelsuporte/situacaoUsuario';
import {Logradouro} from '../../modelsuporte/logradouro';
import {Endereco} from '../../modelsuporte/endereco';
import {Sistema} from '../../modelsuporte/sistema';
import {Empresa} from '../../modelsuporte/empresa';
import {isArray} from 'rxjs/internal-compatibility';
import {UsuarioPerfil} from '../../modelsuporte/UsuarioPerfil';
import {EigValidadores} from '../../geral/helpers/eig.validadores';
import {EigMenuService} from '../../geral/directives/eig-menu/service/menuService';
import {ResourceRestAbs} from '../../geral/services/resource-rest';
import {Traduzir} from '../../geral/decorators';

@Component({
    selector: 'app-usuario-nao-logado',
    templateUrl: './templates/usuarioNovo.component.html',
    styleUrls: [],
    providers: [EigMenuService]
})
@Traduzir
export class UsuarioNaoLogadoComponent extends ComponentHelper<Usuario> {
    usuario: Usuario;
    confirmarSenha: string;

    perfis: Perfil[] = [];
    situacoesUsuario = SituacaoUsuario.E;
    sistemas: Sistema[];
    sistemasTodos: Sistema[];
    sistemasCombo: Sistema[];
    empresas: Empresa[];

    usuarioExistente = false;

    redirecionar: string;
    sigla: string;
    mostrarMensagem: boolean;
    enviarEmail: boolean;
    mensagemSucesso: string;

    constructor(private route: ActivatedRoute, private resourceRest: ResourceRestAbs) {
        super(Usuario);
        this.init();

        this.route.queryParams.subscribe(params => {
            this.redirecionar = params['from'];
            this.sigla = params['sg'];
            this.mostrarMensagem = params['mm'] === 'true' ? true : false;
            this.enviarEmail = params['em'] === 'true' ? true : false;
            this.mensagemSucesso = params['ms'] != null && params['ms'].length > 0 ? params['ms'] : null;
        });
    }

    private init() {

        this.usuario = new Usuario();
        this.usuario.sistema = new Sistema();
        this.usuario.pessoa = new Pessoa();
        this.usuario.empresa = new Empresa();
        this.usuario.perfil = new Perfil();

        this.usuario.pessoa.endereco = new Endereco();
        this.usuario.pessoa.endereco.logradouro = new Logradouro();
        this.usuario.pessoa.endereco.logradouro.bairro = new Bairro();
        this.usuario.pessoa.endereco.logradouro.bairro.municipio = new Municipio();
        this.usuario.pessoa.endereco.logradouro.bairro.municipio.estado = new Estado();

        this.sistemas = [];
    }


    ngOnInit() {

        // this.addFormControl('perfis', EigValidadores.checkboxesRequiredValidator);
        this.addFormControl('sistema', Validators.required);
        this.addFormControl('perfil', EigValidadores.abstractCodigoValidator);
        this.validarConfirmarSenha();
        this.loadingService.show();

        this.recuperarEmpresas();

        this.recuperarSistemas();

        this.loadingService.close();
        this.removeFormControl('usuario.pessoa.tipoPessoa');

    }


    private removerValidacaoSenha() {
        this.removeFormControl('usuario.senha');
        this.removeFormControl('usuario.login');
        this.removeFormControl('confirmarSenha');
    }

    private adicionarValidacaoSenha(): void {
        this.addFormControl('usuario.senha');
        this.addFormControl('usuario.login');
        this.addFormControl('confirmarSenha');
        this.addValidation(this.validarSenha.bind(this), 'usuario.senha');
    }


    private validarConfirmarSenha() {
        this.addFormControl('confirmarSenha', Validators.maxLength(8));
        this.addValidation((control: AbstractControl) => {
            const senha = this.getFormControl('usuario.senha');
            if (senha) {
                senha.updateValueAndValidity();
            }
            return null;
        }, 'confirmarSenha');
    }


    salvar(): void {
        this.loadingService.show();
        const usuarioCopia = _.cloneDeep(this.usuario);
        const enderecoCopia = _.cloneDeep(this.usuario.pessoa.endereco);

        delete enderecoCopia.pessoa;

        usuarioCopia.pessoa.enderecos = [];
        usuarioCopia.pessoa.enderecos.push(enderecoCopia);

        usuarioCopia.pessoa.cpfCnpj = usuarioCopia.pessoa.cpfCnpj.removerMascara();

        enderecoCopia.logradouro.cep = parseInt(enderecoCopia.logradouro.cep.toString().removerMascara(), 10);
        usuarioCopia.usuarioPerfis = [];

        const usuarioPerfil: UsuarioPerfil = new UsuarioPerfil();
        usuarioPerfil.perfil = _.cloneDeep(usuarioCopia.perfil);
        // @ts-ignore
        delete usuarioPerfil.perfil.sistemaPerfil;
        delete usuarioCopia.perfil;
        usuarioPerfil.situacaoUsuario = SituacaoUsuario.PENDENTE;
        usuarioCopia.usuarioPerfis.push(usuarioPerfil);

        delete usuarioCopia.perfis;

        delete usuarioCopia.sistemas;
        // @ts-ignore
        delete usuarioCopia.endereco;

        usuarioCopia.enviarEmail = this.enviarEmail;

        this.apoioServiceAbs.salvarUsuario(usuarioCopia).subscribe(resposta => {
            if (resposta.sucesso) {
                resposta.mensagem = this.mensagemSucesso == null ? this.traduzir('usr.msg_usuario_criado') : this.mensagemSucesso;
            } else {
                if (_.find(resposta.mensagens, mensagem => {
                    // @ts-ignore
                    return 'DUPLICADO' === mensagem.mensagem.campo;
                }) != null) {
                    this.alertService.erroModal(this.traduzir('usr.usuario_existente'),
                        this.traduzir('usr.msg_usuario_vinculado'), false);
                    this.loadingService.close();
                }
            }
            const processarMensagem = this.processarMensagem(resposta);
            if (processarMensagem != null) {
                (<Promise<any>> processarMensagem).then(resp => {
                    let url = this.redirecionar;
                    if (!this.mostrarMensagem) {
                        url += (url.endsWith('/') ? '' : '/') + resposta.resultado.codigo;
                    }
                    window.location.replace(url);
                });
            }
            this.loadingService.close();
        }, () => this.loadingService.close());
    }

    private validarSenha(control: AbstractControl) {
        const confirmacao = this.getFormControl('confirmarSenha');
        if (confirmacao) {
            const senha = this.getFormControl('usuario.senha');
            if (senha) {
                if (senha.value !== confirmacao.value) {
                    return {
                        mismatch: {
                            mensagem: this.traduzir('usr.senhas_nao_conferem')
                        }
                    };
                } else {
                    return null;
                }
            }
        }
        return null;
    }

    private recuperarSistemas(): void {
        this.apoioServiceAbs.listarTodosSistemas().subscribe(resp => {
            if (resp.sucesso) {
                this.sistemasTodos = resp.resultado;
                this.usuario.sistema = _.find(this.sistemasTodos, sistema => sistema.sigla === this.sigla);
                this.setValue('sistema', this.usuario.sistema);
                this.recuperarPerfis();
            }
        });
    }

    private recuperarPerfis(): void {
        if (this.usuario.sistema != null && this.usuario.sistema.codigo != null) {
            this.apoioServiceAbs.listarPerfilPorSistemaNoAdm(this.usuario.sistema.codigo).subscribe(resposta => {
                this.perfis = resposta.resultado;
            });
        }
    }

    private recuperarEmpresas(): void {
        this.apoioServiceAbs.listarEmpresasPorSistema(this.sigla).subscribe(resposta => {
            if (resposta.sucesso) {
                this.empresas = resposta.resultado;
            }
        });
    }

    recuperarPessoa(): void {
        const control = this.getFormControl('usuario.pessoa.cpfCnpj');
        if (control != null && control.valid) {
            this.loadingService.show();
            this.apoioServiceAbs.recuperarUsuario(control.value.removerMascara()).subscribe(resposta => {
                this.usuarioExistente = false;

                if (resposta == null) {
                    this.recuperarPessoa2();
                } else {

                    if (resposta && resposta.sucesso && !isArray(resposta.resultado)) {

                        const endereco = _.cloneDeep(this.usuario.pessoa.endereco);
                        const sistema = _.cloneDeep(this.usuario.sistema);
                        // delete resposta.resultado.usuarioPerfis;
                        this.usuario = resposta.resultado;
                        this.usuario.sistema = sistema;
                        this.usuario.pessoa.cpfCnpj = control.value;
                        if (resposta.resultado.pessoa.enderecos == null || resposta.resultado.pessoa.enderecos.length === 0) {
                            this.usuario.pessoa.endereco = endereco;
                        } else {
                            this.usuario.pessoa.endereco = resposta.resultado.pessoa.enderecos[0];
                            this.setValue('usuario.pessoa.endereco.logradouro', this.usuario.pessoa.endereco.logradouro);
                        }
                        this.usuario.situacao = resposta.resultado.situacao;

                        if (resposta.resultado.sistemas && resposta.resultado.sistemas.length > 0) {
                            const sist = _.find(resposta.resultado.sistemas, value => value.sigla === this.sigla);
                            if (sist != null) {
                                this.usuarioExistente = true;
                                setTimeout(function () {
                                    this.addError('usuario.pessoa.cpfCnpj',
                                        {errorCode: 'duplicado', mensagem: this.traduzir('usr.usuario_existente')});
                                }.bind(this), 200);


                            } else {
                                this.removeError('usuario.pessoa.cpfCnpj', 'duplicado');
                            }
                        }

                        if (this.usuario.empresa == null) {
                            this.usuario.empresa = new Empresa();
                        }
                        if (this.usuario.perfil == null) {
                            this.usuario.perfil = new Perfil();
                        }

                        this.setValue('usuario.pessoa.nome', this.usuario.pessoa.nome);
                        this.setValue('usuario.pessoa.telefone', this.usuario.pessoa.telefone);
                        this.setValue('usuario.pessoa.email', this.usuario.pessoa.email);
                        this.removeFormControl('usuario.pessoa.endereco.numero');

                        this.removerValidacaoSenha();
                    } else {
                        this.adicionarValidacaoSenha();
                    }
                }
                this.loadingService.close();
            }, () => this.loadingService.close());
        }
    }

    recuperarPessoa2(): void {
        const control = this.getFormControl('usuario.pessoa.cpfCnpj');
        if (control != null && control.valid) {
            this.loadingService.show();
            this.apoioServiceAbs.recuperarPessoa(control.value.removerMascara()).subscribe(resposta => {
                if (resposta && resposta.sucesso && !isArray(resposta.resultado)) {
                    this.usuario = new Usuario();
                    this.usuario.pessoa = resposta.resultado;
                    this.usuario.pessoa.endereco = resposta.resultado.enderecos[0];
                    this.setValue('usuario.pessoa.endereco.logradouro', this.usuario.pessoa.endereco.logradouro);
                    this.setValue('usuario.pessoa.nome', this.usuario.pessoa.nome);
                    this.setValue('usuario.pessoa.telefone', this.usuario.pessoa.telefone);
                    this.setValue('usuario.pessoa.email', this.usuario.pessoa.email);
                    this.removeFormControl('usuario.pessoa.endereco.numero');
                    this.usuario.empresa = new Empresa();
                } else {
                    this.init();
                }
                this.usuario.pessoa.cpfCnpj = control.value;
                this.adicionarValidacaoSenha();
                this.loadingService.close();
            }, () => this.loadingService.close());
        }
    }

    async recuperarCep() {
        this.loadingService.show();
        const respLogradouro = await this.apoioServiceAbs.recuperarLogradouroPorCep(this.usuario.pessoa.endereco.logradouro.cep);
        this.loadingService.close();
        if (respLogradouro.sucesso && respLogradouro.resultado != null && respLogradouro.resultado.codigo != null) {
            this.usuario.pessoa.endereco.logradouro = respLogradouro.resultado;
            this.usuario.pessoa.endereco.logradouro._recuperado = true;

            this.setValue('usuario.pessoa.endereco.logradouro', this.usuario.pessoa.endereco.logradouro);

            this.removeError('usuario.pessoa.endereco.logradouro.cep', 'no-cep');

        } else {
            this.addError('usuario.pessoa.endereco.logradouro.cep',
                {errorCode: 'no-cep', mensagem: this.traduzir('usr.cep_nao_encontrado')});
            const cep = this.usuario.pessoa.endereco.logradouro.cep;
            this.usuario.pessoa.endereco.logradouro = new Logradouro();
            this.usuario.pessoa.endereco.logradouro.bairro.municipio = new Municipio();
            this.usuario.pessoa.endereco.logradouro.bairro.municipio.estado = new Estado();
            this.usuario.pessoa.endereco.logradouro.cep = cep;
        }
    }

    voltar(): void {
        window.location.replace(this.redirecionar);
    }
}
