export const en = {
    'usr': {
        'nome': 'Informe o nome do usuário',
        'sigla': 'Sigla da Funcionalidade',
        'sistema_perfis': 'Sistemas/Perfis',
        'remover_sistema': 'Remover do sistema',
        'situacao': 'Situação do Usuário',
        'motivo_bloqueio': 'Motivo do bloqueio',
        'informe_motivo_bloqueio': 'Informe o Motivo do bloqueio',
        'alert_usuario_existente': '<strong>Alerta! </strong> Este usuário já existe para este Sistema!',
        'dados_login': 'Dados de Login'
    }
};
