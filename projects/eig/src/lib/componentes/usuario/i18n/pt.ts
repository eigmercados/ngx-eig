export const pt = {
    'usr': {
        'nome': 'Informe o nome',
        'sigla': 'Sigla da Funcionalidade',
        'sistema_perfis': 'Sistemas/Perfis',
        'remover_sistema': 'Remover do sistema',
        'situacao': 'Situa\u00E7\u00E3o do Usu\u00E1rio',
        'motivo_bloqueio': 'Motivo do bloqueio',
        'informe_motivo_bloqueio': 'Informe o Motivo do bloqueio',
        'alert_usuario_existente': '<strong>Alerta! </strong> Este usu\u00E1rio j\u00E1 existe para este Sistema!',
        'dados_login': 'Dados de Login',
        'usuario_existente': 'Usu\u00E1rio j\u00E1 existente para este sistema!',
        'cep_nao_encontrado': 'CEP n\u00E3o encontrado.',
        'senhas_nao_conferem': 'As senhas informadas n\u00E3o conferem',
        'msg_usuario_vinculado': 'Este usuário já está vinculado com este Perfil a este Sistema',
        'msg_usuario_criado': 'Seu usu\u00E1rio foi criado, por\u00E9m precisa da aprova\u00E7\u00E3o do Administrador do Sistema.\n\nVoc\u00EA receber\u00E1 uma mensagem assim que for aprovado.'

    }
};
