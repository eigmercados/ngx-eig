import {NgModule} from '@angular/core';
import {FormSharedModule} from '../../FormSharedModule';
import {NgxMaskModule} from 'ngx-mask';
import {EigAutocompleteModule} from '../../geral/directives/eig-autocomplete/eig-autocomplete.module';
import {CnpjCpfModule} from '../../geral/pipes/cnpjModule';
import {EigCnpjModule} from '../../geral/directives/eig-cnpj/eig-cnpj.module';
import {EigCheckboxesModule} from '../../geral/directives/eig-checkboxes/eig.checkboxes.module';
import {EigConfirmModule} from '../../geral/directives/eig-confirm/eig-confirm';
import {UsuarioNaoLogadoComponent} from './usuarioNaoLogado.component';
import {UsuarioNovoRoutesModule} from './routes/rotasNovo';
import {MaskModule, ObscurecerModule} from '../../geral/pipes';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';

import * as pt from './i18n/pt';
import * as en from './i18n/en';
import {CustomTranslateLoader} from '../../CustomTranslateLoader';

@NgModule({
    imports: [
        UsuarioNovoRoutesModule
        , FormSharedModule.forRoot()
        , NgxMaskModule.forRoot()
        , EigAutocompleteModule.forRoot()
        , CnpjCpfModule.forRoot()
        , EigCnpjModule.forRoot()
        , EigCheckboxesModule.forRoot()
        , EigConfirmModule.forRoot()
        , MaskModule.forRoot()
        , ObscurecerModule.forRoot()
        , TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: CustomTranslateLoader.Translate({locale: 'pt', traducao: pt.pt}, {locale: 'en', traducao: en.en}),
                deps: []
            },
            isolate: true
        })

    ],
    declarations: [
        UsuarioNaoLogadoComponent
    ],
    entryComponents: [],
    providers: []
})
export class UsuarioNovoModule {
}
