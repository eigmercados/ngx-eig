import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UsuarioNaoLogadoComponent} from '../usuarioNaoLogado.component';

const usuarioRoutes: Routes = [
    {
        path: '', component: UsuarioNaoLogadoComponent
        // canActivate: [AutenticacaoService]
    }

];

@NgModule({
    imports: [RouterModule.forChild(usuarioRoutes)],
    exports: [RouterModule]
})
export class UsuarioNovoRoutesModule {
}
