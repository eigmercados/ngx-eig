import {Component, ElementRef, OnInit} from '@angular/core';
import {AlertService} from './alert-service';


@Component({
    selector: 'eig-alert',
    templateUrl: './alert-component.html'
})
export class AlertComponent implements OnInit {
    mensagens: MessageAlert[] = [];

    timeouts: number[] = [];

    constructor(private alertService: AlertService, private el: ElementRef) {
        // console.log('el', el);
    }

    ngOnInit() {
        this.alertService.getMessage().subscribe(mensagem => {
            if (mensagem != null && mensagem.text != null && mensagem.text.length > 0) {
                this.mensagens.push(mensagem);

                setTimeout(function () {
                    $(document).scrollTop(<any>$('#div_alerts'));
                }.bind(this), 500);

            } else {
                this.mensagens = [];
            }

            this.clearTimes();
            this.timeouts.push(setTimeout(function () {
                this.montarMensagens();
            }.bind(this), 500));

        });
    }

    private clearTimes(): void {
        this.timeouts.forEach(time => {
            clearTimeout(time);
        });
    }

    // @throttle()
    montarMensagens() {

        // console.log('montando mensagens', this.mensagens);
        const cont = $(this.el.nativeElement);
        cont.html('');
        if (this.mensagens.length > 0) {

            const grupos = this.getGrupos();
            // console.log('grupos', grupos);
            for (const grupo in grupos) {
                // console.log('grupo', grupo);
                const mensagens = grupos[grupo];
                // console.log('msg grupo', mensagens);
                const cc = $('<div>').addClass(this.getMessageClasses(grupo));
                cc.attr('id', Math.random());
                // console.log('cricou o cc');
                cc.appendTo(cont);
                // console.log('fez o append no cc');
                this.addCloseButton(cc);
                // console.log('botao no cc');
                mensagens.forEach(value => {
                    // console.log('dentro do each');
                    $(`<p>${value.text}</p>`)
                        .appendTo(cc);
                    // console.log('fez o append');

                });
            }
            this.mensagens = [];
        }

    }

    private getGrupos() {
        const grupo = [];
        this.mensagens.forEach(value => {
            if (grupo[value.type] == null) {
                grupo[value.type] = [];
            }
            grupo[value.type].push(value);
        });
        return grupo;
    }

    private addCloseButton(comp) {
        const btn = $('<button>').attr('type', 'button').addClass('close');

        btn.append('<span aria-hidden=\'true\'>&times;</span>');
        btn.click(function () {
            const div = $(this).parents('div:eq(0)');
            if (div != null) {
                div.fadeOut(1000, function () {
                    $(this).remove();
                });
            }
        });
        comp.append(btn);

        setTimeout(function () {
            btn.trigger('click');
        }.bind(this), 10000);
    }

    private getMessageClasses(tipo: string) {
        switch (tipo) {
            case 'error':
                return 'alert alert-danger';
            case 'success':
                return 'alert alert-success';
            case 'warn':
                return 'alert alert-warning';
        }
    }
}

export interface MessageAlert {
    type: string;
    text: string;
}
