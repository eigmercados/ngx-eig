import {Injectable} from '@angular/core';
import {NavigationStart, Router} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import swal from 'sweetalert';


@Injectable()
export class AlertService {
    private subject: Subject<any>;
    private keepAfterNavigationChange = false;

    constructor(private router: Router) {

        this.subject = new Subject<any>();

        // limpa as mensagens quando a rota troca
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterNavigationChange) {
                    // manter apenas por uma iteracao
                    this.keepAfterNavigationChange = false;
                } else {
                    // limpa
                    this.subject.next();
                }
            }
        });
    }

    sucesso(message: string, keepAfterNavigationChange = false): void {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({type: 'success', text: message});
    }

    erro(message: string, keepAfterNavigationChange = false): void {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({type: 'error', text: message});
    }

    info(message: string, keepAfterNavigationChange = false): void {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({type: 'warn', text: message});
    }

    sucessoModal(titulo: string, mensagem: string, autoclose = true): Promise<any> {
        return this.showModalAlert(titulo, mensagem, autoclose, 'success');
    }


    erroModal(titulo: string, mensagem: string, autoclose = true): Promise<any> {
        return this.showModalAlert(titulo, mensagem, autoclose, 'error');
    }

    infoModal(titulo: string, mensagem: string, autoclose: boolean): Promise<any> {
        return this.showModalAlert(titulo, mensagem, autoclose, 'info');

    }

    warningModal(titulo: string, mensagem: string, autoclose: boolean): Promise<any> {
        return this.showModalAlert(titulo, mensagem, autoclose, 'warning');
    }

    confirmModal(tipo: TipoModal, titulo: string, mensagem: string): Promise<any> {
        return swal({
            title: titulo,
            text: mensagem,
            icon: tipo,
            dangerMode: true,
            buttons: [true, 'Sim']
        });
    }


    private showModalAlert(titulo: string, mensagem: string, autoclose: boolean, tipo: string): Promise<any> {
        return swal({
            title: titulo,
            text: mensagem,
            icon: tipo,
            timer: autoclose ? 5000 : null
        });
    }


    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}

export declare type TipoModal = 'success' | 'error' | 'info' | 'warning';

