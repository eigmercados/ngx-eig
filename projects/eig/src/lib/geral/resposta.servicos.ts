import {isArray} from 'util';

export class RespostaServico<E> {

    mensagens: ErroCampo[] = [];

    mensagem;
    mensagensErro;
    sucesso: boolean;
    resultado;

    /**
     *
     * @param  mensagem
     * @param  mensagensErro
     * @param  resultado
     */
    constructor(mensagem: string, mensagensErro: Object, resultado: E, sucesso: boolean) {
        this.mensagem = mensagem;
        this.mensagensErro = mensagensErro;
        this.resultado = resultado;
        this.sucesso = sucesso;
        this.converterMensagens(mensagensErro);
        this.converteDatas();
    }

    private converterMensagens(mensagens: Object): void {
        if (mensagens) {
            for (const msg in mensagens) {
                this.mensagens.push(new ErroCampo(msg, mensagens[msg]));
            }
        }
    }

    private converteDatas(): void {
        if (this.resultado != null) {
            if (isArray(this.resultado)) {
                this.resultado.forEach(resultado => {
                    this.convert(resultado);
                });
            } else if (typeof this.resultado === 'object' && this.resultado.conteudo && this.resultado.conteudo.length > 0) {
                this.resultado.conteudo.forEach(resultado => {
                    this.convert(resultado);
                });
            } else {
                this.convert(this.resultado);
            }
        }
    }

    private convert(resultado): void {
        if (resultado != null) {
            Object.keys(resultado).forEach(key => {
                const tmp = resultado[key];
                try {
                    if (/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])/.test(tmp)) {
                        resultado[key] = new Date(tmp);
                    }
                } catch (e) {
                    resultado[key] = tmp;
                }
            });
        }
    }
}

export class ErroCampo {

    constructor(private _campo: string, private _mensagem: string) {
    }

    get campo(): string {
        return this._campo;
    }

    get mensagem(): string {
        return this._mensagem;
    }

    get fullMessage(): string {
        return `${this._campo}: ${this._mensagem}`;
    }
}
