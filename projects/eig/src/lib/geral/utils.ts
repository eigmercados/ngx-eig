import {NgModuleFactory, Renderer2, Type} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import * as moment_ from 'moment';
import {unitOfTime} from 'moment';

const moment = moment_;

export {};

export type Constructor<T = {}> = new (...args: any[]) => T;
export type Unidades = unitOfTime.Base;

declare global {
    interface String {
        format(): string;

        removerMascara(): string;

        aplicarMascara(mascara: string): string;

        titleCase(): string;

        toDate(pattern: string): Date;

    }

    interface Date {
        format(pattern: string): string;

        add(qtd: number, unidade: string): Date;

        isBefore(data: Date): boolean;

        isAfter(data: Date): boolean;

        diasRestantes(dias: number): number;
    }

    /*  interface ComponentLoader {
        _getContentRef(content: string | TemplateRef<any> | any,
                       context?: any): ContentRef;
      }*/


    function loadJS(renderer: Renderer2, caminho: string): Observable<any>;

    function cast<T>(obj: any, cl: { new(...args): T }): T;

    function lazyModuleLoad<T>(cl: Type<T>, callback?: Subject<boolean>): any;

    function parseUrlPathInSegments(fullUrl: string): Array<string>;

    function mixin<TBase extends Constructor>(Base: TBase);
}

const _global = (window /* browser */  /* node */) as any;


String.prototype.format = function (): string {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {
        var regexp = new RegExp('\\{' + i + '\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};

String.prototype.titleCase = function (): string {
    return this !== undefined && this.length > 0 ? this.replace(/\w\S*/g, function (txt) {
        txt[0].toUpperCase() + txt.substr(1).toLowerCase();
    }) : '';
};


/**
 * Remove mascaras de valores formatados
 */
String.prototype.removerMascara = function (): string {
    var exp = /\-|\.|\/|\(|\)| /g;
    return this !== undefined ? this.replace(exp, '') : '';
};


/***
 * Formatador generico.
 * Ex:
 * aplicarMascara('00.000.000/0000-00');
 */
String.prototype.aplicarMascara = function (mascara: string): string {
    if (this === undefined) {
        return '';
    }
    var isCuringaMascara;
    var posicaoCampo = 0;
    var novoValorCampo = '';
    var valorSemMascara = this.removerMascara();
    var tamanhoMascara = valorSemMascara.length;
    for (var i = 0; i <= tamanhoMascara; i++) {
        isCuringaMascara = ((mascara.charAt(i) === '-') || (mascara.charAt(i) === '.') || (mascara.charAt(i) === '/'));
        isCuringaMascara = isCuringaMascara || ((mascara.charAt(i) === '(') || (mascara.charAt(i) === ')') || (mascara.charAt(i) === ' '));

        if (isCuringaMascara) {
            novoValorCampo += mascara.charAt(i);
            tamanhoMascara++;
        } else {
            novoValorCampo += valorSemMascara.charAt(posicaoCampo);
            posicaoCampo++;

        }
    }

    return novoValorCampo;
};

String.prototype.toDate = function (pattern: string): Date {
    return moment(this, pattern).toDate();
};

Date.prototype.format = function (pattern: string): string {
    return moment(this).format(pattern);
};

Date.prototype.add = function (qtd: number, unidade: string): Date {
    const qq: moment_.DurationInputArg1 = qtd;
    return moment(this).add(qq, <moment_.unitOfTime.DurationConstructor> unidade).toDate();
};

Date.prototype.isBefore = function (data: Date): boolean {
    return moment(this).isBefore(data);
};

Date.prototype.isAfter = function (data: Date): boolean {
    return moment(this).isAfter(data);
};

Date.prototype.diasRestantes = function (dias: number): number {
    const _this = moment(this);
    const previsao = _this.add(dias, 'days');
    return moment().diff(previsao, 'days') * -1;
};

_global.loadJS = function (renderer: Renderer2, caminho: string): Observable<any> {
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = caminho;
    renderer.appendChild(document.body, script);
    return Observable.create(function (observer) {
        script.onload = () => {
            observer.next();
        };
    }.bind(this));
};

_global.cast = function <T>(obj: any, cl: { new(...args): T }): T {
    obj.__proto__ = cl.prototype;
    return obj;
};

_global.lazyModuleLoad = function <T>(cl: Type<T>, callback?: Subject<boolean>): any {
    return function (): Type<any> | NgModuleFactory<any> | Promise<Type<any>> | Observable<Type<any>> {
        return new Promise<Type<any>>(resolve => {
            if (callback != null) {
                callback.subscribe(() => {
                    resolve(cl);
                });
            } else {
                resolve(cl);
            }

        });
    };
};


_global.parseUrlPathInSegments = function (fullUrl: string): Array<string> {
    const result = fullUrl
        .split('/')
        .filter(segment => segment)
        .map(segment => {
            let path = segment;
            const paramPos = path.indexOf(';');
            if (paramPos > -1) {
                path = path.substring(0, paramPos);
            }
            const outletPos = path.indexOf(':');
            if (outletPos > -1) {
                path = path.substring(outletPos + 1, path.length - 1);
            }
            return path;
        });
    return result;
};
_global.mixin = function <T extends Constructor>(Base: T) {
    return class extends Base {
    };
};
