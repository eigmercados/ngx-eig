import {ElementRef, Injector, OnInit} from '@angular/core';
import {ServiceHelper} from '../services/serviceHelper';
import {EntidadeBasica} from '../../modelsuporte/entidadeBasica';
import {FormBuilder, FormControl} from '@angular/forms';
import * as _ from 'lodash';
import {Helper} from './helper';
import {Subject} from 'rxjs';
import {Paginador} from './Paginador';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {MainAppModule} from '../../componentes/main/main.module';
import {EigMenuService} from '../directives/eig-menu/service/menuService';

export abstract class ComponentListHelper<T extends EntidadeBasica> extends Helper<T> implements OnInit {

    private filtro: string;
    filtrado: boolean;
    protected carregarAoIniciar = true;

    protected onLoaded: Subject<any> = new Subject<any>();

    protected tabela: DatatableComponent;

    public mensagens = {
        emptyMessage: 'Nenhum registro encontrado',
        totalMessage: 'Total de registros'
    };

    public page: Paginador<T> = {
        pageNumber: 0,
        count: 0,
        pageSize: 0,
        limit: 10,
        offset: 0,
        conteudo: []
    };

    public cssClasses = {
        sortAscending: 'fa fa-sort-asc',
        sortDescending: 'fa fa-sort-desc',
        pagerLeftArrow: 'ico ico-chevron-left',
        pagerRightArrow: 'ico ico-chevron-right',
        pagerPrevious: 'ico ico-skip-back',
        pagerNext: 'ico ico-skip-forward'
    };

    public isLoading: boolean;
    public rows = [];
    public headerHeight = 50;
    public footerHeight = 0;
    public rowHeight = 50;
    protected pageLimit = 12;
    private fnListar: Function;

    private _totalRegistros: number;
    private totalPaginas: number;
    private paginaAtual: number;

    constructor(private service: ServiceHelper<T>, private el: ElementRef) {
        super();
        this.fnListar = service.listar.bind(service);

        const injector = Injector.create([{
            provide: FormBuilder,
            deps: []
        }]);

        this.fb = injector.get(FormBuilder);

        this.meuForm = this.fb.group({
            filtro: new FormControl('')
        });

        this.onLoaded.asObservable().subscribe(value => {
            this.footerHeight = value && value.count > this.pageLimit ? 50 : 50;
        });

        const eigMenuService = MainAppModule.injector.get(EigMenuService);
        eigMenuService.toogleSub.asObservable().subscribe(() => {
            if (this.tabela != null) {
                setTimeout(function () {
                    this.tabela.recalculate();
                    window.dispatchEvent(new Event('resize'));
                }.bind(this), 200);
            }
        });
    }

    /**
     * Funcao utilizada para buscar os registros paginados
     * Caso nao seja informado, a funcao listar(pagina, maximo) sempre sera usada
     * @param funcao
     */
    protected setFnListar(funcao: Function) {
        this.fnListar = funcao.bind(this.service);
    }

    public filtrar() {
        this.filtro = this.meuForm.controls['filtro'].value;

        this.rows = [];
        this.paginaAtual = 0;
        this.totalPaginas = null;
        this._totalRegistros = null;
        if (this.filtro != null || this.filtrado) {
            this.filtrado = true;
            this.loadPage(this.pageLimit);
        }
    }

    protected limpar(filtrar = true) {
        this.filtro = null;
        this.meuForm.controls['filtro'].setValue(null);
        if (filtrar) {
            this.filtrar();
        }
        this.filtrado = false;
    }

    ngOnInit() {
        if (this.carregarAoIniciar) {
            this.setPage(this.page);
        }
    }


    public setPage(page: Paginador<T>) {
        this.page = page;
        this.loadPage(this.page.limit);
    }

    private loadPage(limit: number) {
        this.isLoading = true;

        this.loadingService.show();
        this.fnListar(this.page.offset, limit, this.filtro).subscribe(resp => {
            this.rows = resp.resultado.conteudo || resp.resultado;
            if (resp.resultado && resp.resultado.count > 0) {
                this.page.count = resp.resultado.count || resp.resultado.length;
                this.page.pageSize = limit;
                this.page.pageNumber = this.page.offset;
                this.onLoaded.next(resp.resultado);
            } else {
                this.rows = [];
                this.page.count = 0;
                this.page.pageNumber = 0;
                this.page.pageSize = 0;
            }
            this.isLoading = false;
            this.loadingService.close();
        }, error => {
            this.loadingService.close();
        });
    }

    protected removerRegistroDaLista(row: any): void {
        const tmp = this.rows;
        _.remove(tmp, (linha) => _.isEqual(linha, row));
        this.rows = [...tmp];
        this.page.count--;
    }

    get totalRegistros(): number {
        return this.page.count;
    }
}
