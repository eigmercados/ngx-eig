import {Injector} from '@angular/core';
import {RespostaServico} from '../resposta.servicos';
import {AlertService} from '../alert/alert-service';
import {CustomError} from '../../modelsuporte/IValidador';
import {AbstractControl, AsyncValidatorFn, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {SharedModule} from '../../SharedModule';
import {EigLoadingService} from '../directives/eig-loading/service/eigLoadingService';
import {Subscription} from 'rxjs';
import {EigAppModule} from '../../eig.app.module';
import {ApoioServiceAbs} from '../services/apoio-support.service';
import {AnotacaoAware} from '../decorators';
import {TradutorService} from '../services';
import {TranslateService} from '@ngx-translate/core';

export abstract class Helper<T> implements AnotacaoAware {

    injector: Injector;
    alertService: AlertService;
    apoioServiceAbs: ApoioServiceAbs;
    loadingService: EigLoadingService;
    tradutorService: TradutorService;
    translateService: TranslateService;

    private valueChanges: Map<string, Subscription>;

    meuForm: FormGroup;
    fb: FormBuilder;

    emptyOption = {codigo: null, descricao: 'Selecione'};

    constructor() {

        const injector = Injector.create({
            providers: [{
                provide: FormBuilder,
                deps: []
            }]
        });

        this.fb = injector.get(FormBuilder);

        if (this.fb == null) {
            throw new Error('preciso de um FormBuilder injetado!');
        }

        this.meuForm = this.fb.group({});
        this.injector = SharedModule.injector;

        /* this.apoioService = SharedModule.injector.get(ApoioServiceAbs);
         this.alertService = EigAppModule.alertService;
         this.loadingService = SharedModule.injector.get(EigLoadingService);
         this.tradutorService = EigAppModule.injector.get(TradutorService);
         this.translateService = EigAppModule.injector.get(TranslateService);  */

        this.apoioServiceAbs = this.injector.get(ApoioServiceAbs);
        this.alertService = EigAppModule.alertService;
        this.loadingService = this.injector.get(EigLoadingService);
        this.tradutorService = this.injector.get(TradutorService);
        this.translateService = this.injector.get(TranslateService);

        this.valueChanges = new Map();

        if (this.tradutorService != null) {
            this.tradutorService.verificaLinguagem();
        }
    }

    abstract ngOnInit(): void;

    protected addFormControl(nome, ...validadores: ValidatorFn[]): void {
        this.meuForm.addControl(nome, new FormControl(null, validadores != null ? Validators.compose(validadores) : null));
    }

    protected removeFormControl(nome): void {
        this.meuForm.removeControl(nome);
    }

    protected getFormControl(nome): AbstractControl {
        if (this.meuForm && this.meuForm.controls) {
            return this.meuForm.controls[nome];
        }
        return null;
    }

    protected setValue(nomeFormControl: string, valor: any, emitEvent: boolean = false) {
        const control = this.getFormControl(nomeFormControl);
        if (control != null) {
            control.setValue(valor, {
                emitEvent: !emitEvent,
                onlySelf: emitEvent,
                emitModelToViewChange: !emitEvent,
                emitViewToModelChange: !emitEvent
            });
        }
    }

    protected getValue(nomeFormControl: string) {
        const control = this.getFormControl(nomeFormControl);
        if (control != null) {
            return control.value;
        }
        return null;
    }


    protected addError(nomeFormControl: string, erro: CustomError) {
        const control = this.getFormControl(nomeFormControl);
        if (control) {
            const tmp = [];
            tmp[erro.errorCode] = {mensagem: erro.mensagem};
            control.setErrors(tmp);
        }
    }

    protected removeError(nomeFormControl: string, errorCode: string) {
        const control = this.getFormControl(nomeFormControl);
        if (control && control.errors) {
            delete control.errors[errorCode];
            control.updateValueAndValidity();
        }
    }

    protected addValidation(fnValidacao: ValidatorFn, ...nomeFormControl: string[]) {
        nomeFormControl.forEach(function (nomeForm) {
            let control = this.getFormControl(nomeForm);
            if (control == null) {
                this.addFormControl(nomeForm, Validators.compose([]));
                control = this.getFormControl(nomeForm);
            }
            if (control) {
                const validacoes = control.validator;
                control.setValidators(Validators.compose([validacoes, fnValidacao]));
            }
        }.bind(this));
    }

    protected addAsyncValidation(fnValidacao: AsyncValidatorFn, ...nomeFormControl: string[]) {
        nomeFormControl.forEach(function (nomeForm) {
            let control = this.getFormControl(nomeForm);
            if (control == null) {
                this.meuForm.addControl(nomeForm, new FormControl('', null, Validators.composeAsync([])));
                control = this.getFormControl(nomeForm);
            }
            if (control) {
                const validacoes = control.asyncValidator;
                control.setAsyncValidators(Validators.composeAsync([validacoes, fnValidacao]));
            }
        }.bind(this));
    }

    protected processarMensagem(resposta: RespostaServico<T>, manterMensagens = false, modal = true, tituloModal?: string,
                                autocloseModal = false): Promise<any> | void {
        if ((resposta.mensagem != null && resposta.mensagem.length > 0) /*|| (resposta.mensagens && resposta.mensagens.length > 0)*/) {
            if (resposta.sucesso) {
                if (modal) {
                    return this.alertService.sucessoModal(tituloModal || 'Sucesso!', resposta.mensagem, autocloseModal);
                } else {
                    this.alertService.sucesso(resposta.mensagem, manterMensagens);
                }
            } else {
                if (modal) {
                    return this.alertService.erroModal(tituloModal || 'Erro!', resposta.mensagem, autocloseModal);
                } else {
                    this.alertService.erro(resposta.mensagem, manterMensagens);
                }
            }
        } else if (resposta.mensagens.length > 0) {
            // @ts-ignore
            resposta.mensagens.forEach(msg => {
                // @ts-ignore
                this.alertService.erro(msg.mensagem.fullMessage, manterMensagens);
            });
        } else if (resposta.sucesso) {
            return this.alertService.sucessoModal('Sucesso!', '', autocloseModal);
        } else if (!resposta.sucesso) {
            return this.alertService.erroModal('Erro!', '', autocloseModal);
        }
    }

    comparadorSelect(item1, item2) {
        // return item1 && item2 ? item1.codigo === item2.codigo : item1 === item2;

        if (item1 === item2) {
            return true;
        }

        if (item1 != null && item1.codigo != null && item2 != null && item2.codigo != null) {
            return item1.codigo === item2.codigo;
        }

        if (item2 != null && item2.codigo == null && item1.codigo == null) {
            return true;
        }

        return false;
    }

    protected addValueChange(key: string, subscription: Subscription): void {
        this.valueChanges.set(key, subscription);
    }

    protected removeValueChange(key: string): Subscription {
        const subscription = this.valueChanges.get(key);
        if (subscription) {
            subscription.unsubscribe();
            this.valueChanges.delete(key);
        }
        return subscription;
    }

    protected setLanguage(locale: string): void {
        if (this.tradutorService != null) {
            this.tradutorService.trocouLocale.next(locale);
        }
    }

    protected traduzir(chave: string): string {
        if (this.tradutorService != null) {
            return this.tradutorService.traduzir(chave);
        }
        return chave;
    }

}
