import {Subject} from 'rxjs';

export interface ModalService<T> {

    _fromModal: boolean;
    onAfterSave: Subject<any>;
    onAfterShow?: Function;
    loaded?: Subject<any>;

}
