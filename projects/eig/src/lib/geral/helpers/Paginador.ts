export interface Paginador<T> {
    pageNumber: number;
    count: number;
    pageSize: number;
    limit: number;
    offset: number;
    conteudo?: T[];
}
