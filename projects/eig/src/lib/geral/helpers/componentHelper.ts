import {ValidatorFn, Validators} from '@angular/forms';
import {CampoValidador, IValidador} from '../../modelsuporte/IValidador';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';
import {ServiceHelper} from '../services/serviceHelper';

import {Helper} from './helper';
import {ParameterlessConstructor} from '../../modelsuporte/ParameterlessConstructor';


export abstract class ComponentHelper<T extends IValidador> extends Helper<T> {


    validadores: CampoValidador[] = [];

    private tmpValidadores = {};
    private entidadesValidadas;

    private recuperaAlterado: Observable<any>;

    constructor(private parameterlessEntity: ParameterlessConstructor<T>, ...entidadesValidadas: Array<any>) {
        super();
        this.entidadesValidadas = entidadesValidadas;
        this.iniciaProcessadores();
    }

    private iniciaProcessadores() {

        this.processaEntidades();
        this.processarValidadores();

    }

    private processaEntidades() {
        this.entidadesValidadas = this.entidadesValidadas.map(item => {
            return new item();
        });
        if (this.parameterlessEntity != null) {
            this.entidadesValidadas = [this.getNew()].concat(this.entidadesValidadas);
        }
    }

    private getNew(): T {
        const entidade = new this.parameterlessEntity();
        // console.log('Propriedades da entidade', Object.keys(this.parameterlessEntity));
        return entidade;
    }

    private processarValidadores() {

        this.entidadesValidadas.forEach(function (obj) {
            this.processa(obj, obj.constructor.name.toLowerCase());
        }.bind(this));

        try {
            // console.log('validadores', this.tmpValidadores);
            this.meuForm = this.fb.group(this.tmpValidadores);
            // this.meuForm = this.fb.group({});
        } catch (e) {
        }
    }

    private processa(obj, pai: string): boolean {
        if (obj._validadores != null) {
            obj._validadores.forEach(function (item) {
                // console.log('item', item);
                if (item._construtor === obj.constructor.name
                    || item._construtor === Object.getPrototypeOf(Object.getPrototypeOf(obj).constructor).name) {
                    if (item.lookFor != null) {
                        if (item.validadores != null) {
                            this.incluirValidacoes(pai, item);
                        }
                        if (!this.processa(new item.lookFor(), `${pai}.${item.campo}`)) {
                            this.incluirValidacoes(pai, item);
                        }
                    } else {
                        this.incluirValidacoes(pai, item);
                    }
                }
            }.bind(this));
            return true;
        }
        return false;
    }

    private incluirValidacoes(pai: string, item) {
        this.tmpValidadores[`${pai}.${item.campo}`] = this.processaDepVal(item);
        item._campoMensagem = `${pai}.${item.campo}`;
        this.validadores.push(item);
    }

    private processaDepVal(item) {
        const tmp: ValidatorFn[] = [];
        if (item != null && item.validadores != null) {
            item.validadores.forEach(function (msgValidador) {
                tmp.push(msgValidador.validador);
            });
        }

        return ['', Validators.compose(tmp)];
    }

    initEdicao(servico: ServiceHelper<T>, route: ActivatedRoute): Observable<T> {

        this.recuperaAlterado = Observable.create(function (observer) {

            if (route) {
                route.params.subscribe(params => {

                    const id = params['id'];

                    if (id != null) {
                        // console.log('buscar entidade', id);
                        servico.recuperarPorCodigo(id).subscribe(resp => {
                            if (resp.resultado.codigo) {
                                observer.next(resp.resultado);
                            } else {
                                observer.next(this.getNew());
                            }

                            observer.complete();
                        }, err => observer.error());

                    } else {
                        observer.next(this.getNew());
                        observer.complete();
                    }
                });
            } else {
                observer.error();
            }
        }.bind(this));
        return this.recuperaAlterado;
    }

}
