import {EntidadeBasica} from '../../modelsuporte/entidadeBasica';

/**
 * Classe que representa os checkboxes em tela.
 */
// @dynamic
export abstract class ICheckbox<T extends EntidadeBasica> {
    checked: boolean;
    entidade: T;
    id?: string;
    ler?: boolean;
    editar?: boolean;
    excluir?: boolean;

    /**
     * Transforma a lista de entidades informada em uma lista de ICheckbox
     * Caso a lista esteja vazia retorna null
     *
     * @param entidades
     * @returns
     */
    static wrapper<E extends EntidadeBasica>(entidades: E[]): ICheckbox<E>[] {
        if (entidades != null) {
            return entidades.map(entidade => {
                return {
                    checked: false,
                    entidade: entidade
                };
            });
        }
        return null;
    }

    /**
     * Retira a lista de entidades de uma lista ICheckbox
     *
     * @param checks
     * @returns
     */
    static unwrapSelecionados<E extends EntidadeBasica>(checks: ICheckbox<E>[]): E[] {
        return checks.filter(item => item.checked).map(ent => ent.entidade);
    }
}
