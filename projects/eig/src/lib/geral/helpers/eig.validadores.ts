import {AbstractControl, ValidatorFn} from '@angular/forms';
import * as _ from 'lodash';
import {FileItem, FileUploader} from 'ng2-file-upload';

// @dynamic
export abstract class EigValidadores {

    public static abstractCodigoDescricaoValidator: ValidatorFn = (control: AbstractControl) => {
        if (control != null) {
            console.log('validador', control.value);
            if (control.value == null || control.value.length === 0 || (control.value.codigo == null && control.value.descricao == null)) {
                return {
                    requerido: {
                        mensagem: 'Campo obrigat\u00F3rio'
                    }
                };
            }
        }
        return null;
    };

    public static abstractCodigoValidator: ValidatorFn = (control: AbstractControl) => {
        if (control != null) {
            console.log('validador', control.value);
            if (control.value == null || control.value.length === 0 || (control.value.codigo == null)) {
                return {
                    requerido: {
                        mensagem: 'Campo obrigat\u00F3rio'
                    }
                };
            }
        }
        return null;
    };

    public static checkboxesRequiredValidator: ValidatorFn = (control: AbstractControl) => {
        if (control != null) {
            if (control.value != null && _.isArray(control.value)) {
                let check = false;
                control.value.forEach(checkbox => {
                    if (checkbox.checked) {
                        check = true;
                    }
                });
                if (!check) {
                    return {
                        requerido: {
                            mensagem: 'Campo obrigat\u00F3rio'
                        }
                    };
                }
            }
        }
        return null;
    };


    public static uploadFileRequired = (uploader: FileUploader) => {
        return function (control: AbstractControl) {
            console.log('uploadfilerequieed', control, uploader);
            if (uploader != null) {
                let check = false;
                uploader.getNotUploadedItems().forEach((fileItem: FileItem) => {
                    console.log('fileItem', fileItem);
                    if (fileItem.file != null) {
                        check = true;
                    }
                });

                if (!check) {
                    return {
                        requerido: {
                            mensagem: 'Campo obrigat\u00F3rio'
                        }
                    };
                }
            }
            return null;
        };
    };

    public static validarAtributo(atributo: string) {
        return function (control: AbstractControl) {
            console.log('validarAtributo', control);
            if (control != null && control.value != null) {
                if (control.value[atributo] == null) {
                    return {
                        requerido: {
                            mensagem: 'Campo obrigat\u00F3rio'
                        }
                    };
                }
            }
            return null;
        };
    }

    public static valorMenorIgual = (valor: Function) => {
        return function (control: AbstractControl) {
            return new Promise(resolve => {
                    if (control != null && control.value != null) {
                        if (parseFloat(control.value) <= valor()) {
                            resolve({
                                valorMenorIgual: {
                                    mensagem: 'O valor n\u00E3o pode ser inferior a ' + valor()
                                }
                            });
                        }
                    }
                    resolve(null);
                }
            );
        };
    };

    public static valorMenor = (valor: Function) => {
        return function (control: AbstractControl) {
            return new Promise(resolve => {
                    if (control != null && control.value != null) {
                        if (parseFloat(control.value) < valor()) {
                            resolve({
                                valorMenor: {
                                    mensagem: 'O valor n\u00E3o pode ser inferior a ' + valor()
                                }
                            });
                        }
                    }
                    resolve(null);
                }
            );
        };
    };


    public static valorMaiorIgual = (valor: Function) => {
        return function (control: AbstractControl) {
            return new Promise(resolve => {
                    if (control != null && control.value != null) {
                        if (parseFloat(control.value) >= valor()) {
                            resolve({
                                valorMaiorIgual: {
                                    mensagem: 'O valor n\u00E3o pode ser maior que ' + valor()
                                }
                            });
                        }
                    }
                    resolve(null);
                }
            );
        };
    };

    public static valorMaior = (valor: Function) => {
        return function (control: AbstractControl) {
            return new Promise(resolve => {
                    if (control != null && control.value != null) {
                        if (parseFloat(control.value) > valor()) {
                            resolve({
                                valorMaior: {
                                    mensagem: 'O valor n\u00E3o pode ser maior que ' + valor()
                                }
                            });
                        }
                    }
                    resolve(null);
                }
            );
        };
    };

    public static valorEntre = (menor: Function, maior: Function) => {
        return function (control: AbstractControl) {
            return new Promise(resolve => {
                    if (control != null && control.value != null && menor() && maior()) {
                        const valor = parseFloat(control.value);
                        if (!(valor >= menor() && valor <= maior())) {
                            resolve({
                                between: {
                                    mensagem: `O valor deve estar entre ${menor()} e ${maior()}`
                                }
                            });
                        }
                    }
                    resolve(null);
                }
            );
        };
    };

    public static arquivosRequiredValidator(valor: Function) {
        return function (control: AbstractControl) {
            if (control != null) {
                if (control.value != null && _.isArray(control.value) && control.value.length > 0) {

                    let check = true;

                    valor().forEach(value => {
                        if (value.obrigatorio === true && _.find(control.value, item => item.arquivo.tipoArquivo.codigo === value.tipoArquivo.codigo) == null) {
                            check = false;
                            return;
                        }
                    });

                    if (!check) {
                        return {
                            requerido: {
                                mensagem: 'Insira todos os arquivos obrigat\u00F3rios'
                            }
                        };
                    }

                } else {
                    return {
                        requerido: {
                            mensagem: 'Campo obrigat\u00F3rio'
                        }
                    };
                }
            }
            return null;
        };
    }

    public static dataMenor = (valor: Function) => {
        return function (control: AbstractControl) {
            return new Promise(resolve => {
                    if (control != null && control.value != null) {
                        const valor1 = valor();
                        if (control.value.isAfter(valor1)) {
                            resolve({
                                valorMenor: {
                                    mensagem: 'O valor n\u00E3o pode ser inferior a ' + valor().format('DD/MM/YYYY')
                                }
                            });
                        }
                    }
                    resolve(null);
                }
            );
        };
    };

    public static dataMaior = (valor: Function) => {
        return function (control: AbstractControl) {
            return new Promise(resolve => {
                    if (control != null && control.value != null) {
                        const valor1 = valor();
                        if (control.value.isBefore(valor1)) {
                            resolve({
                                valorMenor: {
                                    mensagem: 'O valor n\u00E3o pode ser inferior a ' + valor().format('DD/MM/YYYY')
                                }
                            });
                        }
                    }
                    resolve(null);
                }
            );
        };
    };

    public static arrayNaoVazio: ValidatorFn = (control: AbstractControl) => {
        if (control != null) {
            if (control.value == null || control.value.length === 0) {
                return {
                    requerido: {
                        mensagem: 'Campo obrigat\u00F3rio'
                    }
                };
            }
        }
        return null;
    };

}
