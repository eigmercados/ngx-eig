import {NgModule} from '@angular/core';
import {SharedModule} from '../../SharedModule';
import {PaginaErroRouteModule} from './routes/rotas';
import {PaginaErroComponent} from './paginaErro.component';


@NgModule({
    imports: [
        PaginaErroRouteModule
        , SharedModule.forRoot()
    ],
    declarations: [
        PaginaErroComponent
    ],
    entryComponents: []
})
export class PaginaErroModule {
}
