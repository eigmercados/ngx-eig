import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'paginaerro',
    templateUrl: './templates/paginaErro.component.html',
    styleUrls: ['../../../assets/css/error-pages.css'],
    entryComponents: [],
    providers: []
})
export class PaginaErroComponent implements OnInit {

    codigoErro: number;
    mensagemErro: string;

    constructor(private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.codigoErro = params['codigo'];
            this.mensagemErro = params['mensagem'];
        });
    }

}
