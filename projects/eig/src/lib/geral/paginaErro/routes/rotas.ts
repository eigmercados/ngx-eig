import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PaginaErroComponent} from '../paginaErro.component';

const paginaErroRoutes: Routes = [
    {
        path: '', component: PaginaErroComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(paginaErroRoutes)],
    exports: [RouterModule]
})
export class PaginaErroRouteModule {
}
