import {Injectable} from '@angular/core';
import {RespostaServico} from '../resposta.servicos';
import {Observable} from 'rxjs';
import {CacheService} from './CacheService';
import {ConverterHelper} from './converterHelper';
import {EigAppModule} from '../../eig.app.module';

@Injectable()
export abstract class ServiceHelper<T> {

    protected cacheService: CacheService;
    private converterHelper: ConverterHelper<T>;

    constructor() {
        this.cacheService = EigAppModule.injector.get(CacheService);
        this.converterHelper = new ConverterHelper<T>();
    }

    abstract recuperarPorCodigo(codigo: number): Observable<RespostaServico<T>>;

    abstract listarTodos(): Observable<RespostaServico<T>>;

    abstract listar(pagina, maximo): Observable<RespostaServico<T>>;

    protected converterResponse(resposta: any, tipo?: any): RespostaServico<T> {
        return this.converterHelper.converterResponse(resposta, tipo);
    }

}
