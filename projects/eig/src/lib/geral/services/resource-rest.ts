import {Restangular} from 'ngx-restangular';
import {EIG_CONFIG, EigConfig} from '../../eig-config';
import {Inject, Injectable} from '@angular/core';

export class ResourceRestAbstract {

    login;
    apoio;
    usuario;
    perfil;
    funcionalidade;
    sistema;
    empresa;

    constructor(eigConfig: EigConfig, private restangular: Restangular) {
        this.apoio = restangular.all('apoio/');
        this.login = restangular.allUrl('oauth', eigConfig.oauthUrl);
        this.usuario = restangular.all('usuario');
        this.perfil = restangular.all('perfil');
        this.funcionalidade = restangular.all('funcionalidade');
        this.sistema = restangular.all('sistema');
        this.empresa = restangular.all('empresa');
    }

    scaWebUrl(): ResourceRestAbs {
        // @ts-ignore
        this.restangular.provider.toogleUrl = true;
        return this;
    }
}

@Injectable()
export class ResourceRestAbs extends ResourceRestAbstract {
    constructor(@Inject(EIG_CONFIG) eigConfig: EigConfig, restangular: Restangular) {
        super(eigConfig, restangular);
    }
}
