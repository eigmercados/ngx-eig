import {ResourceRestAbs} from './resource-rest';
import {ServiceHelper} from './serviceHelper';
import {RespostaServico} from '../resposta.servicos';
import {EntidadeBasica} from '../../modelsuporte/entidadeBasica';
import {IRecuperado} from '../../modelsuporte/iRecuperado';
import {AutenticacaoService} from './autenticacao-service';
import {DomSanitizer} from '@angular/platform-browser';
import {Observable} from 'rxjs';
import {Municipio} from '../../modelsuporte/municipio';
import {FiltroAutoComplete} from '../directives/eig-autocomplete/eig-autocomplete';
import {Logradouro} from '../../modelsuporte/logradouro';
import {map} from 'rxjs/operators';
import {Menu} from '../../modelsuporte/menu';
import {Perfil} from '../../modelsuporte/perfil';
import {Empresa} from '../../modelsuporte/empresa';
import * as _ from 'lodash';
import {Sistema} from '../../modelsuporte/sistema';
import {Usuario} from '../../modelsuporte/usuario';
import {Pessoa} from '../../modelsuporte/pessoa';
import {Injectable} from '@angular/core';
import {Estado} from '../../modelsuporte';


@Injectable()
export class ApoioServiceAbs extends ServiceHelper<IRecuperado> {

    constructor(protected resouceRest: ResourceRestAbs, private sanitizer: DomSanitizer,
                private autenticacaoService: AutenticacaoService) {
        super();
    }

    recuperarPorCodigo(codigo: number): Observable<RespostaServico<EntidadeBasica>> {
        throw new Error('ApoioService nao deve usado para este fim.');
    }

    listarTodos(): Observable<RespostaServico<IRecuperado>> {
        throw new Error('ApoioService nao deve usado para este fim.');
    }

    listar(pagina, maximo): Observable<RespostaServico<IRecuperado>> {
        throw new Error('ApoioService nao deve usado para este fim.');
    }

    listarMunicipios(): Promise<RespostaServico<Municipio[]>> {
        return this.resouceRest.apoio.one('municipios').getList().pipe(map(resposta => {
            return super.converterResponse(resposta, Municipio);
        })).toPromise();
    }

    listarEstados(): Promise<RespostaServico<Estado[]>> {
        return this.resouceRest.scaWebUrl().apoio.one('estados').getList().pipe(map(resposta => {
            return super.converterResponse(resposta, Municipio);
        })).toPromise();
    }

    recuperarMunicipiosPorEstadoENome(filtro: FiltroAutoComplete): Observable<RespostaServico<Municipio[]>> {
        return this.resouceRest.apoio.all('autocomplete').one('estado', filtro.parametros[0].codigo)
            .one('municipios', filtro.filtro).getList().pipe(map(resposta => {
                return super.converterResponse(resposta, Municipio);
            }));
    }

    recuperarLogradouroPorCep(cep: number): Promise<RespostaServico<Logradouro>> {
        return this.resouceRest.apoio.all('logradouro').one('cep', cep).get().pipe(map(resposta => {
            return super.converterResponse(resposta, Logradouro);
        })).toPromise();
    }

    recuperarMenu(): Promise<RespostaServico<Menu>> {
        return this.resouceRest.apoio.one('menu').getList().pipe(map(resposta => {
            return super.converterResponse(resposta, Menu);
        })).toPromise();
    }

    listarPerfilPorSistema(codigoSistema: number): Observable<RespostaServico<Perfil>> {
        return this.resouceRest.perfil.one('sistema', codigoSistema).get().pipe(map(resposta => {
            return super.converterResponse(resposta);
        }));
    }

    listarPerfilPorSistemaNoAdm(codigoSistema: number): Observable<RespostaServico<Perfil>> {
        return this.resouceRest.perfil.one('sistema').one('ad', codigoSistema).get().pipe(map(resposta => {
            return super.converterResponse(resposta);
        }));
    }

    listarTodasEmpresas(): Observable<RespostaServico<Empresa>> {
        const f = _.bind(() => {
            return this.resouceRest.empresa.one('lista').getList().pipe(map(resposta => {
                return super.converterResponse(resposta, Empresa);
            }));
        }, this);
        return this.cacheService.getCachable(f, Empresa, {keys: ['empresas'], sessionScope: true});
    }

    listarTodosSistemas(): Observable<RespostaServico<Sistema>> {
        const f = _.bind(() => {
            return this.resouceRest.sistema.one('lista').getList().pipe(map(resposta => {
                return super.converterResponse(resposta, Sistema);
            }));
        }, this);
        return this.cacheService.getCachable(f, Sistema, {keys: ['sistemas'], sessionScope: true});
    }

    listarEmpresasPorSistema(siglaSistema: string): Observable<RespostaServico<Empresa>> {
        const f = _.bind(() => {
            return this.resouceRest.empresa.one('sistema').one('sg', siglaSistema).getList().pipe(map(resposta => {
                return super.converterResponse(resposta, Empresa);
            }));
        }, this);
        return this.cacheService.getCachable(f, Empresa, {keys: ['empresas'], sessionScope: true});
    }

    recuperarUsuario(cpfCnpj: string): Observable<RespostaServico<Usuario>> {

        const f = _.bind(() => {
            return this.resouceRest.usuario.one('usuario').one('completo', cpfCnpj).get().pipe(map(resposta => {
                return super.converterResponse(resposta);
            }));
        }, this);

        return this.cacheService.getCachable(f, Pessoa, {keys: ['usuario', cpfCnpj + ''], sessionScope: true});
    }

    recuperarPessoa(cpfCnpj: string): Observable<RespostaServico<Usuario>> {

        const f = _.bind(() => {
            return this.resouceRest.usuario.one('pessoa', cpfCnpj).get().pipe(map(resposta => {
                return super.converterResponse(resposta);
            }));
        }, this);

        return this.cacheService.getCachable(f, Pessoa, {keys: ['pessoa', cpfCnpj + ''], sessionScope: true});
    }

    salvarUsuario(usuario: Usuario): Observable<RespostaServico<Usuario>> {
        return this.resouceRest.usuario.all('novo').post(usuario).pipe(map(resposta => {
            return super.converterResponse(resposta);
        }));
    }

    /*    getSafeUrl(url: string) {
            return this.sanitizer.bypassSecurityTrustUrl(url);
        }*/

    temPermissao(sigla?: string): Observable<RespostaServico<Boolean>> {
        return this.resouceRest.apoio.one('temacesso', sigla || this.autenticacaoService.menu.funcionalidade.sigla)
            .get().pipe(map(resposta => {
                const respostaServico = super.converterResponse(resposta, null);
                return respostaServico;
            }));
    }

    permissaoNegada(): void {
        this.autenticacaoService.permissaoNegada();
    }
}
