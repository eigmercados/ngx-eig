import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import * as _ from 'lodash';
import {CacheService} from './CacheService';

@Injectable()
export class DynamicPathGuard implements CanActivate {

    constructor(private router: Router, private cacheService: CacheService) {
    }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {


        const segments = parseUrlPathInSegments(state.url);
        const lastPath = segments.pop();

        const rotas = this.cacheService.get('rotas');
        if (_.find(rotas, rota => _.isEqual(rota, lastPath)) != null) {
            // Trigger change detection so url is known for router
            setTimeout(() => {
                this.router.navigateByUrl(state.url);
            }, 0);
        } else {
            this.router.navigateByUrl('/404');
        }

        return false;
    }
}