import {Inject, Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, NavigationStart, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {ResourceRestAbs} from './resource-rest';
import {CacheService} from './CacheService';
import {Menu} from '../../modelsuporte/menu';
import {Usuario} from '../../modelsuporte/usuario';
import {Perfil} from '../../modelsuporte/perfil';
import {EIG_CONFIG, EigConfig} from '../../eig-config';
import {EigAppModule} from '../../eig.app.module';


@Injectable()
export class AutenticacaoService implements CanActivate {

    private TOKEN = 'JSESSIONID';
    private USER_ID = 'USER_ID';

    private _usuarioLogado: any;
    public dadosUsuarioLogado: Subject<any>;
    public menuClicado = new Subject<Menu>();
    public menu: Menu;
    funcionalidade: string;

    constructor(@Inject(EIG_CONFIG) private eigConfig: EigConfig, private resourceRest: ResourceRestAbs
        , private cacheService: CacheService
        , private router: Router) {
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if ((<NavigationStart>event).url === '/' && this.isAutenticado()) {
                    router.navigate(['main']);
                }
            }
        });
        this.dadosUsuarioLogado = new Subject<any>();
        this.menuClicado = new Subject<Menu>();
        this.buscarDadosUsuario(null);

        this.menuClicado.asObservable().subscribe(value => {
            this.menu = value;
        });

        if (this.isAutenticado()) {
            EigAppModule.sessionTokenObs.next(this.getToken());
        }
    }

    public logar(usuario: string, senha: string): Observable<Usuario> {

        const clientId = this.eigConfig.clientId;

        const obs = Observable.create(function (observer) {
            this.resourceRest.login.all('token').customPOST(undefined, undefined, {
                username: usuario,
                password: senha, //CryptoJS.MD5(senha).toString(),
                grant_type: 'password',
                client_id: clientId
            }, {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Basic ' + btoa(clientId + ':secret')
            }).subscribe(
                data => {
                    if (data) {
                        data = data[0];
                        if (data.access_token) {
                            this.saveToken(data.access_token, data.expires_in);
                            this.buscarDadosUsuario(observer);
                        }
                    }
                },
                () => {
                    observer.error();
                }
            );
        }.bind(this));

        return obs;

    }

    private buscarDadosUsuario(observer): void {
        const tmp = sessionStorage.getItem(this.USER_ID);
        if (this.isLogado() && tmp == null) {
            this.resourceRest.apoio.all('usuarioLogado').customPOST(undefined, undefined, undefined, {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.getToken()}`
            }).subscribe(resposta => {
                this._usuarioLogado = resposta[0].resultado;
                sessionStorage.setItem(this.USER_ID, JSON.stringify(this.usuarioLogado));
                this.dadosUsuarioLogado.next(this._usuarioLogado);
                if (observer != null) {
                    observer.next(this._usuarioLogado);
                    observer.complete();
                }
            });
        } else if (tmp != null) {
            this._usuarioLogado = JSON.parse(tmp);
            this.dadosUsuarioLogado.next(this._usuarioLogado);
            if (observer != null) {
                observer.next(this._usuarioLogado);
                observer.complete();
            }
        }

    }

    public setPerfilLogado(perfil: Perfil): Observable<any> {
        const obs = Observable.create(function (observer) {
            this.resourceRest.apoio.one('perfil').one('set', perfil.codigo).customGET(undefined, undefined, undefined, {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.getToken()}`
            }).subscribe(resposta => {
                this._usuarioLogado = resposta[0].resultado;
                sessionStorage.setItem(this.USER_ID, JSON.stringify(this.usuarioLogado));
                this.dadosUsuarioLogado.next(this._usuarioLogado);

                observer.next();
                observer.complete();
            }, () => {
                observer.complete();
            });
        }.bind(this));

        return obs;
    }

    private saveToken(accessToken: string, expire: number): void {
        EigAppModule.sessionTokenObs.next(accessToken);
        this.cacheService.addCookie(this.TOKEN, accessToken, new Date().add(expire, 'minutes'));
    }

    getToken(): string {
        return this.cacheService.getCookie<string>(this.TOKEN);
    }

    logout(redirecionar = true) {

        this._usuarioLogado = null;
        this.resourceRest.apoio.one('logout', this.getToken()).get();
        sessionStorage.removeItem(this.USER_ID);
        this.cacheService.deleteCookie(this.TOKEN);
        EigAppModule.sessionTokenObs.next(null);
        // this.cookieService.deleteAll();

        if (redirecionar) {
            // setTimeout(function () {
            // this.router.navigate(['/login'], {preserveFragment: false, preserveQueryParams: false, replaceUrl: false});
            this.router.navigate(['/login']);
            // }.bind(this), 500);
        }
    }

    private isAutenticado(): boolean {
        return this.cacheService.getCookie(this.TOKEN) != null;
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (!this.isAutenticado()) {
            // this.alertService.erro('Voc\u00EA n\u00E3o est\u00E1 autorizado a acessar essa p\u00E1gina', true);
            this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
            return false;
        }
        return true;
    }

    /*getToken(): string {
        return this.cookieService.get(this.TOKEN);
    }*/

    isLogado(): boolean {
        // const token = this.getToken();
        // return token != null && token.length > 0;
        return this.isAutenticado();
    }

    get usuarioLogado(): any {
        return this._usuarioLogado;
    }

    permissaoNegada() {
        this.router.navigate(['/denied']);
    }

    erro(codigoErro: number, mensagemErro: string): void {
        this.router.navigate(['/erro'], {queryParams: {codigo: codigoErro, mensagem: mensagemErro}});
    }

}
