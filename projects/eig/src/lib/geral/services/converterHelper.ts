import {RespostaServico} from '../resposta.servicos';
import {isArray} from 'rxjs/internal-compatibility';

export class ConverterHelper<T> {


    public converterResponse(resposta: any, tipo?: any): RespostaServico<T> {
        if (isArray(resposta)) {
            resposta = resposta[0];
        }

        const respostaServico = new RespostaServico<T>(resposta.mensagemSucesso, resposta.mensagens,
            resposta.resultado || resposta.page, resposta.sucesso);
        if (respostaServico.resultado !== null && typeof respostaServico.resultado === 'object') {
            if ((respostaServico.resultado && isArray(respostaServico.resultado) && respostaServico.resultado.length > 0) ||
                (respostaServico.resultado.conteudo && isArray(respostaServico.resultado.conteudo) && respostaServico.resultado.conteudo.length > 0)) {
                (respostaServico.resultado.length > 0 ? respostaServico.resultado : respostaServico.resultado.conteudo).map(item => {
                    try {
                        return this.adicionaCodigo(item, tipo);
                    } catch (e) {
                        return item;
                    }

                });

                if (respostaServico.resultado.extras != null) {
                    Object.assign(respostaServico.resultado, respostaServico.resultado.extras);
                    delete respostaServico.resultado.extras;
                }

            } else if (!isArray(respostaServico.resultado)) {
                respostaServico.resultado = this.adicionaCodigo(respostaServico.resultado, tipo);
            }
        }

        return respostaServico;
    }

    private adicionaCodigo(item: any, tipo: any) {
        if (tipo && item && typeof item === 'object') {
            const novo = cast(item, tipo);
            /* const propriedadeCodigo = (<EntidadeBasica>novo).getPropriedadeCodigo();
             if (propriedadeCodigo != null && propriedadeCodigo !== '') {
                 novo['codigo'] = novo[propriedadeCodigo];
             }*/
            return novo;
        }
        return item;
    }
}