import {AutenticacaoService} from './autenticacao-service';
import {EigAppModule} from '../../eig.app.module';
import {isArray} from 'rxjs/internal-compatibility';
import {ErroCampo} from '../resposta.servicos';
import * as _ from 'lodash';
import {EIG_CONFIG, EigConfig} from '../../eig-config';
import {TradutorService} from './TradutorService';


export function RestangularConfigFactory(RestangularProvider) {

    const eigConfig: EigConfig = EigAppModule.injector.get(EIG_CONFIG);

    RestangularProvider.setBaseUrl(eigConfig.apiRest);
    // RestangularProvider.setDefaultHeaders({'Content-Type': applicationXWwwFormUrlencoded});
    RestangularProvider.setDefaultHeaders({'Content-Type': 'application/json'});
    // RestangularProvider.setDefaultHttpFields({withCredentials: true});

    RestangularProvider.setFullResponse(false);

    const urlsBlob = [
        //logar
        // 'oauth/token'

    ];

    const removerNulos = function (element: Object): Object {
        if (!isArray(element)) {
            const keys = Object.keys(element);
            const tmp = {};
            if (keys.length > 0) {
                for (const key in keys) {
                    let valor = element[keys[key]];
                    if (valor != null && valor != undefined) {
                        if (typeof valor === 'object') {
                            tmp[keys[key]] = removerNulos(valor);
                        } else {
                            tmp[keys[key]] = valor;
                        }
                        valor = element[keys[key]];
                        if (valor == null) {
                            delete tmp[keys[key]];
                        }
                    }
                }
                return tmp;
            }
            return null;
        } else {
            return element.map(item => removerNulos(item));
        }
    };

    const manageError = function (response) {
        if (response.status === 0 || response.status === 403 || response.status === 401 ||
            (response.data && response.data.codeError === 10) ||
            (response.body && (response.body.returnCode === 10 || response.body.returnCode === 401))
        ) {
            const autenticacaoService: AutenticacaoService = EigAppModule.injector.get(AutenticacaoService);
            EigAppModule.errorListener.next(true);
            if (response.message === 'Http failure response for (unknown url): 0 Unknown Error') {
                autenticacaoService.erro(302, response.message);
            } else {
                autenticacaoService.permissaoNegada();
            }
            return false;
        }
        /*else if (response.status !== 200) {
                   const autenticacaoService: AutenticacaoService = EigAppModule.injector.get(AutenticacaoService);
                   autenticacaoService.erro(response.status, response.message);
               } else if (response.status === 200 && response.codeError !== 0) {
                   // autenticacaoService.erro(302, response.message);
                   EigAppModule.errorListener.next(true);
               }*/

        return true;
    };

    const verificaUrlBlob = function (url: string): boolean {
        return urlsBlob.find(value => url.endsWith(value)) != null;
    };

    const processarMensagens = function (data: any, novoResponse: any) {
        if (novoResponse.sucesso) {
            novoResponse.mensagemSucesso = (typeof novoResponse.resultado === 'string'
                ? novoResponse.resultado : (data.resultado && data.resultado.message) ? data.resultado.message : null);
        } else {
            novoResponse.mensagem = (typeof novoResponse.resultado === 'string'
                ? novoResponse.resultado : (data.returnMessage) ? data.returnMessage : null);

            if (typeof novoResponse.mensagens === 'object') {

                const tmpArray = _.cloneDeep(novoResponse.mensagens);
                novoResponse.mensagens = [];
                const keys = Object.keys(tmpArray);

                if (keys.length > 0) {
                    for (const key in keys) {
                        const chave = keys[key];
                        const valor = tmpArray[chave];
                        const erroCampo = new ErroCampo(chave, valor);
                        novoResponse.mensagens.push(erroCampo);
                    }
                }
                // });

            }
        }
        ;
    };

    RestangularProvider.addResponseInterceptor((data, operation, what, url, response) => {
        // console.log('ResponseInterceptor', data, operation, what, url, response);
        // console.log('header', response.headers);
        if (response.config.params.responseType === 'blob') {
            return data;
        }

        manageError(response);

        let novoResponse: any = data;
        novoResponse.resultado = (data.resultado != null) ? data.resultado : [];

        if (data && novoResponse.resultado.content && novoResponse.resultado.content.length > 0) {

            novoResponse.page = {
                count: novoResponse.resultado.totalElements,
                pageSize: novoResponse.resultado.pageable.pageSize,
                conteudo: novoResponse.resultado.content,
                extras: novoResponse.resultado.extras || null
            };
            delete novoResponse.resultado;
        }

        processarMensagens(data, novoResponse);


        delete novoResponse.data;
        novoResponse = [novoResponse];
        // }
        return novoResponse;
    });


    RestangularProvider.addFullRequestInterceptor((element, operation, path, url, headers, params) => {
        const resp = {};
        try {
            const autenticacaoService: AutenticacaoService = EigAppModule.injector.get(AutenticacaoService);
            const tradutorService: TradutorService = EigAppModule.injector.get(TradutorService);
            const bearerToken = autenticacaoService.getToken();
            if (url !== eigConfig.oauthUrl + 'token' && bearerToken != null) {
                // @ts-ignore
                resp.headers = Object.assign({}, headers, {
                    'Authorization': `Bearer ${bearerToken}`,
                    'Accept-Language': tradutorService.localeCorrente()
                });
            }

            if (element != null) {
                const tmp = removerNulos(element);
                /*if (headers && (headers['Content-Type'] == null || headers['Content-Type'] === applicationXWwwFormUrlencoded)) {
                    // @ts-ignore
                    resp.element = $.param(cast(tmp, Object));
                } else {
                    // @ts-ignore
                    resp.element = cast(tmp, Object);
                }*/
                // @ts-ignore
                resp.element = tmp;

            }
            if (params != null) {
                const tmp = {};
                const keys = Object.keys(params);
                if (keys.length > 0) {
                    for (const key in keys) {
                        const valor = params[keys[key]];
                        if (valor != null && valor != undefined) {
                            tmp[keys[key]] = valor;
                        }
                    }
                    // @ts-ignore
                    resp.params = tmp;
                    // resp.params = $.param(tmp);
                }
            }
        } catch (e) {
        }
        RestangularProvider.setFullResponse(verificaUrlBlob(url));

        if (RestangularProvider.toogleUrl) {
            delete RestangularProvider.toogleUrl;
            RestangularProvider.setBaseUrl(eigConfig.scaRestApi);
        } else {
            RestangularProvider.setBaseUrl(eigConfig.apiRest);
        }

        return resp;
    });

    /*   RestangularProvider.addFullRequestInterceptor((element, operation, path, url, headers, params) => {
           console.log('addFullRequestInterceptor', element, params);

           const bearerToken = this.autenticacaoService.getToken();
           if (url !== environment.oauthUrl + 'token') {
               return {
                   headers: Object.assign({}, headers, {'Authorization': `Bearer ${bearerToken}`})
               };
           } else {
               return {};
           }
       });*/

    RestangularProvider.addErrorInterceptor((response, subject, responseHandler) => {

        // console.log('erroInterceptor', response, subject, responseHandler);
        if (verificaUrlBlob(response.request.url)) {
            return true;
        }
        EigAppModule.errorListener.next(true);
        return manageError(response);
    });

}
