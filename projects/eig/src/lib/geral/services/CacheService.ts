import {Injectable} from '@angular/core';
import {CookiesStorageService, LocalStorageService, SessionStorageService, WebStorageService} from 'ngx-store';
import {Observable} from 'rxjs';
import {ConverterHelper} from './converterHelper';
import {isArray} from 'rxjs/internal-compatibility';
import {EigAppModule} from '../../eig.app.module';
import {Resource} from 'ngx-store/src/service/resource';

@Injectable()
export class CacheService {
    private readonly PREFIXO: string;
    private SESSION_TOKEN: string;

    private readonly converterHelper: ConverterHelper<any> = new ConverterHelper<any>();

    constructor(private sessionStorageService: SessionStorageService, private localStorageService: LocalStorageService,
                private cookieStorageService: CookiesStorageService) {

        this.PREFIXO = sessionStorageService.config.prefix;
        this.load();

        EigAppModule.sessionTokenObs.asObservable().subscribe(token => {
            if (token != null) {
                this.SESSION_TOKEN = token;
            } else {
                this.sessionStorageService.clear('all');
            }
        });
    }

    private load(): void {

        this.sessionStorageService.keys.forEach(key => {
            const valor: EigCache = cast(this.sessionStorageService.get(key.replace(this.PREFIXO, '')), EigCache);
            if (valor && valor.expirou()) {
                this.sessionStorageService.remove(key);
            }
        });

        this.localStorageService.keys.forEach(key => {
            const valor: EigCache = cast(this.localStorageService.get(key.replace(this.PREFIXO, '')), EigCache);
            if (valor && valor.expirou()) {
                this.localStorageService.remove(key.replace(this.PREFIXO, ''));
            }
        });
    }

    add(cache: EigCache): void {
        const chave = this.getChave(cache.keys);

        if (cache.persistente) {
            this.localStorageService.set(chave, cache);
        } else {
            // this.sessionStorageService.set(chave, cache);
            const resource = this.sessionStorageService.load(chave);
            if (cache.sessionScope) {
                resource.changePrefix(this.SESSION_TOKEN).save(cache);
                // this.changePrefix(this.sessionStorageService.load(chave));
            } else {
                resource.save(cache);
            }
        }

        this.setRemoveTimeout(chave, cache);
    }

    addCookie(key: string, valor: any, dataExpiracao: Date) {
        this.cookieStorageService.set(key, valor, dataExpiracao);
    }

    getCookie<T>(key: string, tipo?: any): T {
        const valor = this.cookieStorageService.get(key);
        if (valor != null) {
            if (tipo != null) {
                return cast(valor, tipo);
            }
            return valor;
        }
    }

    deleteCookie(key: string): void {
        this.cookieStorageService.remove(key);
    }

    private setRemoveTimeout(chave: string, cache: EigCache): void {
        if (cache.expiraEm && cache.expiraEm > 0) {
            setTimeout(function () {
                this.remove(chave);
            }.bind(this), cache.expiraEm * 60000);
        }
    }

    get<T>(...key: string[]): T {
        const chave = this.getChave(key);

        return this.getValue(chave, this.sessionStorageService) || this.getValue(chave, this.localStorageService);
    }

    private getValue(chave: string, service: WebStorageService) {

        const resource = service.load(chave);

        let valor = resource.value;
        if (valor == null && this.SESSION_TOKEN != null) {
            valor = resource.setPrefix(this.SESSION_TOKEN).value;
        }
        return valor ? valor.valor : null;

        /*let tmp = service.get(chave);

        if (tmp == null && this.SESSION_TOKEN) {
            service.config.prefix = this.SESSION_TOKEN;
            tmp = service.get(chave);
            service.config.prefix = null;
        }
        return tmp ? tmp.valor : null;*/
    }

    remove(...key: string[]): void {

        /**
         * correcao no biblioteca
         */
        Resource.prototype.remove = /**
         * Removes item stored under current key
         * @returns {this}
         */
        function () {
            this.service.utility.remove(this.key, {prefix: this._prefix});
            return this;
        };


        const chave = this.getChave(key);

        let resource = this.localStorageService.load(chave);

        if (resource.value == null && this.SESSION_TOKEN != null) {
            resource.setPrefix(this.SESSION_TOKEN);
        }
        if (resource.value != null) {
            resource.remove();
            return;
        }

        resource = this.sessionStorageService.load(chave);

        if (resource.value == null && this.SESSION_TOKEN != null) {
            resource.setPrefix(this.SESSION_TOKEN);
        }
        if (resource.value != null) {
            resource.remove();
        }
    }

    private getChave(key: string[]): string {
        if (key.length === 1) {
            return key[0];
        }

        return key.reduce((previousValue, currentValue) => previousValue + currentValue + '#', '');
    }

    private changePrefix(resource: Resource<any>): void {
        if (this.SESSION_TOKEN) {
            resource.changePrefix(this.SESSION_TOKEN);
        }
    }

    public getCachable(callBack: Function, tipo: any, cacheable: EigCacheable): any {

        return Observable.create(function (obs) {

            const chave = this.getChave(cacheable.keys);
            const valor = this.get(chave);

            if (valor != null) {
                obs.next(this.converterHelper.converterResponse(valor, tipo));
                obs.complete();
            } else {
                callBack().subscribe(respostaServico => {

                    if ((respostaServico.resultado && isArray(respostaServico.resultado) && respostaServico.resultado.length > 0) ||
                        (respostaServico.resultado && respostaServico.resultado.conteudo && isArray(respostaServico.resultado.conteudo) && respostaServico.resultado.conteudo.length > 0) ||
                        (respostaServico.resultado && !isArray(respostaServico.resultado))
                    ) {
                        this.add(new EigCache(cacheable, respostaServico));
                        obs.next(respostaServico);
                        obs.complete();
                    } else {
                        obs.next(null);
                        obs.complete();
                    }
                });
            }
        }.bind(this));
    }
}

export interface EigCacheable {
    /**
     * Caso true, sera armazenada a localSession. Quer dizer que persistira ao fechar do navegador
     *
     * valor padrao: false
     */
    persistente?: boolean;

    /**
     * Caso true, o cache sera removido caso o usuario faca logoff.
     *
     * valor padrao: true
     */
    sessionScope?: boolean;
    /**
     * Quantidade de minutos que o cache ficarah ativo
     *
     * valor padrao: 0 (infinito)
     */
    expiraEm?: number;

    /**
     * Lista de chaves unicas que identificarao o cache
     */
    keys: string[];
}

export class EigCache implements EigCacheable {
    criacao: Date;
    expiraEm: number;
    keys: string[];
    persistente: boolean;
    sessionScope = true;
    valor: any;

    constructor(cacheable: EigCacheable, valor: any) {
        Object.assign(this, cacheable);
        this.valor = valor;
        this.criacao = new Date();
    }

    expirou(): boolean {
        if (this.criacao && this.expiraEm && this.expiraEm > 0) {
            let tmp = this.criacao;
            if (typeof this.criacao === 'string') {
                // @ts-ignore
                tmp = this.criacao.toDate();
            }
            return tmp.add(this.expiraEm, 'minute').isAfter(new Date());
        }
    }
}

