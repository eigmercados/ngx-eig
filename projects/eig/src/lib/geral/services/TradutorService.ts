import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Subject} from 'rxjs';
import {CacheService} from './CacheService';
import {CustomTranslateLoader, I18n} from '../../CustomTranslateLoader';

@Injectable()
export class TradutorService {

    trocouLocale: Subject<string> = null;

    constructor(private translateService: TranslateService, private cacheService: CacheService) {
        this.trocouLocale = new Subject<string>();

        this.trocouLocale.subscribe(locale => {
            this.setLocale(locale);
        });
    }

    traduzir(chave: string): string {
        return this.translateService.instant(chave);
    }

    private setLocale(locale: string) {
        this.translateService.setDefaultLang('pt');
        this.translateService.use(locale);
        this.cacheService.deleteCookie('localeInfo');
        this.cacheService.addCookie('localeInfo', locale, new Date().add(1, 'year'));
    }

    public verificaLinguagem() {
        const cookie: string = this.cacheService.getCookie('localeInfo');
        if (cookie != null) {
            this.trocouLocale.next(cookie);
        } else {
            this.trocouLocale.next('pt');
        }
    }

    localeCorrente(): string {
        return this.translateService.currentLang;
    }

    public use(locale: string): void {
        this.translateService.use(locale);
    }

    public addTraducoes(...i18n: I18n[]): void {
        const customTranslateLoader: CustomTranslateLoader = <CustomTranslateLoader>this.translateService.currentLoader;
        customTranslateLoader.mergeTraducoes(...i18n);
    }
}
