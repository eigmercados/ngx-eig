import {Component, forwardRef, Input} from '@angular/core';
import {ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALUE_ACCESSOR, Validators} from '@angular/forms';

@Component({
    selector: 'eig-data',
    templateUrl: './templates/data.html',
    styleUrls: [],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => EigDataComponent),
        multi: true
    }]
})
export class EigDataComponent implements ControlValueAccessor {

    @Input()
    placeholder: string;

    @Input()
    cssColuna: string;

    @Input()
    label: string;

    formulario: FormGroup;

    msgErro: string;

    id: string;


    private propagateChange = (_: any) => {
    };


    constructor(private fb: FormBuilder) {
        this.formulario = fb.group({});
        this.id = Math.floor((Math.random() * 1000) + 1) + '';

        this.formulario.statusChanges.subscribe(function (campo) {
            if (this.formulario.controls['data']) {
                if (this.formulario.valid) {
                    this.propagateChange(this.formulario.controls['data'].value);
                }
            }
        }.bind(this));

        /*        MainComponent.scriptsCarregados.asObservable().subscribe(() => {
                    this.rodaScript();
                });*/

    }

    writeValue(value: Date) {
        this.criarControls(value);
        setTimeout(function () {
            this.rodaScript();
        }.bind(this), 500);

        this.placeholder = this.placeholder != null ? this.placeholder : 'Insira a data';
    }

    private criarControls(valor: Date) {

        this.formulario.addControl('data', new FormControl(valor, Validators.required));
    }


    private rodaScript(): void {
        const contexto = this;


        const campo = $('#' + this.id);

        const opts = {
            format: 'dd/mm/yyyy',
            autoclose: true,
            immediateUpdates: true,
            language: 'pt-BR'
        };


        // @ts-ignore
        campo.datepicker('clearDates', opts);

        campo.on('changeDate', function () {
            // @ts-ignore
            const data = $(this).data('datepicker').getDate();
            contexto.formulario.controls['data'].setValue(data, {emitEvent: true});
        });


        campo.on('blur', function () {
            if ($(this).val() === '') {
                contexto.formulario.controls['data'].setValue(null, {emitEvent: true});
            }
        });

    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched(fn) {
    }

    validate(control) {
        // Custom logic to validate the parent control. In this case,
        // we may choose to union all childrens' errors.

        // const errors = Object.assign(this.localControl.errors || {}, this.remoteControl.errors || {});
        // return Object.keys(errors).length ? errors : null;
    }
}

