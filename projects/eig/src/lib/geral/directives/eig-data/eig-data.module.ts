import {ModuleWithProviders, NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../../SharedModule';
import {EigDataComponent} from './eig-data';

const providers = [];

@NgModule({
    imports: [
        SharedModule.forRoot()
        , ReactiveFormsModule
        // , BsDatepickerModule.forRoot()
    ],
    declarations: [
        EigDataComponent
    ],
    exports: [
        EigDataComponent
    ]
})
export class EigDataModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: EigDataModule,
            providers: [...providers]
        };
    }
}
