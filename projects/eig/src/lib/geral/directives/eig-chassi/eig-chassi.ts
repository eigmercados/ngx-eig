import {Directive, ElementRef, HostListener, OnInit} from '@angular/core';
import {NgControl} from '@angular/forms';

@Directive({
  selector: '[eigChassi]'
})
export class EigChassiDirective implements OnInit {

  private DEL_KEYS = [8, 46];
  private KEYS_PERMITIDAS = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 90, 88, 67, 86, 66,
    78, 77, 83, 68, 70, 71, 72, 74, 75, 76, 87, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105,
    69, 82, 84, 89, 85, 79, 80].concat(this.DEL_KEYS);

  constructor(private control: NgControl, private el: ElementRef) {
    $(el.nativeElement).attr('maxlength', 17);
  }

  ngOnInit(): void {

  }

  @HostListener('keydown', ['$event'])
  onKeyDown($event) {
    console.log('keydown', this.control.control.value, $event.keyCode);
    if (this.KEYS_PERMITIDAS.indexOf($event.keyCode) !== -1) {
      return true;
    }
    return false;
  }

  @HostListener('keyup', ['$event'])
  onKeyUp($event) {
    console.log('keyup', $event, this.control);

    if ($event.ctrlKey && $event.key === 'v') {
      if (!this.validaRegex()) {
        this.control.control.setValue('');
        return;
      }
    }
    if (this.control != null && this.control.control != null && this.control.control.value != null) {
      this.validaRegex();
    }
  }

  private validaRegex() {
    if (this.control != null && this.control.control != null && this.control.control.value != null) {
      if (!/[^aiq\Wç]/ig.test(this.control.control.value)) {
        console.log('reprovado');
        this.control.control.setValue('');
        return false;
      }
      return true;
    }
    return false;
  }

}
