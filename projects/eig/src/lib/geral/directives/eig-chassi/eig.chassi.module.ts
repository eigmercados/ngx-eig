import {ModuleWithProviders, NgModule} from '@angular/core';
import {EigChassiDirective} from './eig-chassi';
import {SharedModule} from '../../../SharedModule';

const providers = [];

@NgModule({
  imports: [
    SharedModule.forRoot()
  ],
  declarations: [
    EigChassiDirective
  ],
  exports: [
    EigChassiDirective
  ]
})
export class EigChassiModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: EigChassiModule,
      providers: [...providers]
    };
  }
}
