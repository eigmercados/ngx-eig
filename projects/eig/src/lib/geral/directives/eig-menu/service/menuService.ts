import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {AutenticacaoService, TradutorService} from '../../../services';
import {Menu} from '../../../../modelsuporte/menu';
import {CacheService, EigCache} from '../../../services/CacheService';
import {ApoioServiceAbs} from '../../../services/apoio-support.service';

@Injectable()
export class EigMenuService {

    private static ROTA = '/main';

    private _carregado: Subject<boolean>;
    carregaMenu: Subject<MenuItem[]>;

    private _itensMenu: MenuItem[] = [];
    private menus: Menu[] = [];

    public toogleSub: Subject<any>;

    constructor(private autenticacaoService: AutenticacaoService, private apoioService: ApoioServiceAbs,
                private cacheService: CacheService, private tradutorService: TradutorService) {
        this._carregado = new Subject<boolean>();
        this.toogleSub = new Subject();
        this.carregaMenu = new Subject<MenuItem[]>();
    }


    get carregado(): Subject<boolean> {
        return this._carregado;
    }

    private async carregarMenu() {

        const cache: MenuItem[] = this.cacheService.get('menu', this.tradutorService.localeCorrente());
        if (cache != null && cache.length > 0) {
            this._itensMenu = cache;
            this.carregaMenu.next(this._itensMenu);
            return;
        }

        const resp = await this.apoioService.recuperarMenu();
        if (resp.sucesso) {
            this.menus = resp.resultado;

            const usuarioLogado = this.autenticacaoService.usuarioLogado;
            if (usuarioLogado == null) {
                return;
            }
            this._itensMenu = [];
            const map = new Map<number, MenuItem>();
            if (this.menus) {
                this.menus.sort(function (a, b) {
                    if (a.menuPai == null && b.menuPai != null) {
                        return -1;
                    }
                    ;
                    if (a.menuPai != null && b.menuPai == null) {
                        return 1;
                    }
                    ;
                    return a.ordenacao - b.ordenacao;
                });

                for (let i = 0; i < this.menus.length; i++) {
                    const menu = this.menus[i];
                    menu.url = EigMenuService.ROTA + menu.url;
                    if (menu.menuPai == null && !map.has(menu.codigo)) {
                        map.set(menu.codigo, new MenuItem(menu));
                        continue;
                    }
                    if (menu.menuPai != null) {
                        const tmpM = map.get(menu.menuPai.codigo);
                        tmpM.filhos.push(menu);
                    }

                }
            }
            map.forEach(value => {
                this._itensMenu.push(value);
            });

            this._itensMenu.sort((a, b) => a.menu.ordenacao - b.menu.ordenacao);
            this._itensMenu.map(menu => menu.filhos.sort((a, b) => a.ordenacao - b.ordenacao));
            const eigCache = new EigCache({keys: ['menu', this.tradutorService.localeCorrente()], sessionScope: true}, this._itensMenu);
            this.cacheService.add(eigCache);
            this.carregaMenu.next(this._itensMenu);

        }
    }

    loadMenu() {
        const usuarioLogado = this.autenticacaoService.usuarioLogado;
        if (usuarioLogado == null) {
            return;
        }

        if (this._itensMenu.length > 0) {
            this.carregaMenu.next(this._itensMenu);
        } else {
            this.carregarMenu();
        }
    }
}

export class MenuItem {
    menu: Menu;
    filhos: Menu[] = [];


    constructor(menu: Menu) {
        this.menu = menu;
        this.filhos = [];
    }
}
