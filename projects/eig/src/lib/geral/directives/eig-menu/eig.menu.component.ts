import {Component, Inject, OnInit, Renderer2} from '@angular/core';
import {EigMenuService, MenuItem} from './service/menuService';
import {Menu} from '../../../modelsuporte/menu';
import {AutenticacaoService} from '../../services';
import {Router} from '@angular/router';
import {EIG_CONFIG, EigConfig} from '../../../eig-config';
import {TradutorService} from '../../services/TradutorService';

@Component({
    selector: 'eig-menu',
    templateUrl: './templates/menu.html',
    styleUrls: ['./templates/menu.css']
})
export class EigMenuComponent implements OnInit {

    itensMenu: MenuItem[];
    tituloHeader = 'teste';

    constructor(private renderer: Renderer2, private menuService: EigMenuService, @Inject(EIG_CONFIG) eigConfig: EigConfig,
                private autenticacaoService: AutenticacaoService, private tradutorService: TradutorService, private router: Router) {
        this.menuService.carregaMenu.asObservable().subscribe(itens => {
            if (itens && itens.length > 0) {
                this.itensMenu = itens;
            }
            this.menuService.carregado.next(true);
        });
        this.tituloHeader = eigConfig.tituloHeader;

        this.tradutorService.trocouLocale.subscribe(locale => {
            this.menuService.loadMenu();
        });
    }

    ngOnInit(): void {
        this.loadScripts();
        this.menuService.loadMenu();
    }

    trocar(item: Menu): void {
        this.autenticacaoService.menuClicado.next(item);
        this.autenticacaoService.funcionalidade = item.funcionalidade.sigla;
        this.router.navigate([item.url], /*{queryParams: {desc: item.descricao}}*/);
    }

    private loadScripts() {
        this.menuService.carregado.asObservable().subscribe(function (ok) {
            if (ok) {
                loadJS(this.renderer, './assets/js/scrollBar.js').subscribe(value => {
                    const $sidebar: any = $('.sidebar');
                    $sidebar.scrollBox();
                });

                loadJS(this.renderer, './assets/js/vendor.js').subscribe(value => {
                    loadJS(this.renderer, './assets/js/bundle.js').subscribe(() => {
                        if (this.itensMenu == null) {
                            this.menuService.toogleSub.next(true);
                            $('.sidebar-toggle').trigger('click');
                        }
                    });
                });
            }
        }.bind(this));
    }
}
