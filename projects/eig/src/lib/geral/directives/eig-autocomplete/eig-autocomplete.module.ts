import {ModuleWithProviders, NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {EigAutocompleteComponent} from './eig-autocomplete';
import {TypeaheadModule} from 'ngx-bootstrap/typeahead';
import {SharedModule} from '../../../SharedModule';
const providers = [];

@NgModule({
  imports: [
    SharedModule.forRoot()
    , ReactiveFormsModule
    , TypeaheadModule
  ],
  declarations: [
    EigAutocompleteComponent
  ],
  exports: [
    EigAutocompleteComponent
  ]
})
export class EigAutocompleteModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: EigAutocompleteModule,
      providers: [...providers]
    };
  }
}
