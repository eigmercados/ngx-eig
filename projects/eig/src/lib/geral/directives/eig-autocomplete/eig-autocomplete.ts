import {Component, forwardRef, HostListener, Input, ViewChild, ViewContainerRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Observable} from 'rxjs';
import {TypeaheadMatch} from 'ngx-bootstrap/typeahead';
import * as _ from 'lodash';
import {mergeMap} from 'rxjs/operators';


@Component({
    selector: 'eig-autocomplete',
    templateUrl: './templates/autocomplete.html',
    styleUrls: [],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => EigAutocompleteComponent),
        multi: true
    }]
})
export class EigAutocompleteComponent implements ControlValueAccessor {

    private MASK_CNPJ = '00.000.000/0000-00';
    private MASK_CPF = '000.000.000-00';

    lista: Observable<any>;

    @ViewChild('campo')
    campo;

    @Input()
    placeholder: string;

    private valor: any;

    @Input()
    optionLabel: string;

    @Input()
    optionsLabel: LabelAutoComplete;

    @Input()
    fnListar: string;

    @Input()
    filtroAdicional: FiltroAutoComplete;

    @Input()
    disabled;

    @Input()
    mascara;

    mask: string;

    @Input()
    editavel: boolean;

    private funcao: Function;

    @Input()
    id: string;

    label: string;
    private parentComponent: any;

    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;


    @HostListener('keyup', ['$event'])
    onKeyUp($event) {
        if ($event.target == this.campo.nativeElement) {
            if (this.mascara != null) {
                this.label = this.aplicarMascara(this.label, this.mascara);
            }
        }
    }

    private propagateChange(_: any) {
    }

    constructor(private viewContainerRef: ViewContainerRef) {
        // console.log('construtor autocomplete');

        this.parentComponent = viewContainerRef;
        this.parentComponent = this.parentComponent._view.component;

        // console.log('parentComponente', this.parentComponent);

        // this.id = Math.floor((Math.random() * 1000) + 1) + '';

        this.lista = Observable.create(function (observer: any) {
            observer.next(this.getFiltro());
        }.bind(this)).pipe(mergeMap(function (token: string) {
            // console.log('token', token);
            return Observable.create(function (obs) {
                try {
                    this.funcao(token).subscribe((resultado: any) => {
                        obs.next(resultado != null ? resultado.resultado : []);
                        obs.complete();
                    }, () => {
                        obs.next([]);
                        obs.complete();
                    });
                } catch (e) {
                    obs.next([]);
                    obs.complete();
                }
            }.bind(this));

        }.bind(this)));

        this.bindFuncao();
        this.bindMax();
    }

    private bindMax() {
        if (this.mascara != null) {
            $(this.campo.nativeElement).attr('maxlength', this.mascara === 'cpfCnpj' ? this.MASK_CNPJ.length : this.mascara.length);
        }

    }

    writeValue(value: any) {
        if (value != null && _.isObject(value)) {
            this.setValue(value);
        }
        this.bindFuncao();
        this.bindMax();
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched(fn) {
    }

    private setValue(valor) {
        this.valor = valor;
        if (valor != null) {
            this.label = this.getLabel(this.valor);
        }
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    changeTypeaheadNoResults(e: boolean): void {
        this.typeaheadNoResults = e;
        this.valor = null;
    }

    onSelect(e: TypeaheadMatch): void {
        this.setValue(e.item);
        this.propagateChange(this.valor);
    }

    onBlur(e: TypeaheadMatch): void {

        if (this.valor != null && this.valor.codigo != null) return;

        let tmp = null;
        if (e != null) {
            tmp = e.item;
        } else if (e == null && this.typeaheadNoResults) {
            tmp = null;
        } else if (((this.valor != null && this.valor.codigo == null) || this.valor == null) && this.label && this.label.length > 0) {
            this.valor = {};
            if (this.optionLabel != null) {
                this.valor[this.optionLabel] = this.label;
                tmp = this.valor;
            } else {
                this.valor = this.label;
                tmp = this.valor;
            }
        }

        if (tmp == null && !this.editavel) {
            this.label = null;
            this.setValue(tmp);
            this.propagateChange(tmp);
            console.log('propagate 1', tmp);
        } else {
            if (this.valor == null) {
                this.valor = {};
                if (this.optionLabel != null) {
                    this.valor[this.optionLabel] = this.label;
                } else {
                    this.valor = this.label;
                }
            }
            this.propagateChange(this.valor);
            console.log('propagate 2', this.valor, this.label);
        }

        setTimeout(function () {
            this.typeaheadNoResults = false;
        }.bind(this), 3000);
    }

    private getFiltro() {
        if (this.filtroAdicional != null && this.filtroAdicional.parametros.length > 0) {
            return {
                parametros: this.filtroAdicional.parametros,
                filtro: this.removerMascara(this.label)
            };
        } else {
            return this.removerMascara(this.label);
        }
    }

    private bindFuncao() {
        if (this.fnListar != null && this.parentComponent != null) {

            let tmpObj;
            // recupera a funcao
            const tmp = this.fnListar.split('.');
            tmpObj = tmp.reduce((p, c) => {
                if (_.isFunction(p[c])) {
                    return p[c].bind(p);
                } else {
                    return p[c];
                }
            }, this.parentComponent);

            this.funcao = tmpObj;
        }
    }

    getLabel(item) {
        let valor = '';
        if (this.optionLabel != null) {
            valor = item[this.optionLabel];
        } else if (this.optionsLabel != null) {
            let sep = '';
            if (this.optionsLabel != null && this.optionsLabel.keys) {
                valor = this.optionsLabel.keys.reduce((previousValue, currentValue) => {
                    let tmpValor = item[currentValue.key];
                    if (currentValue.mascara != null && currentValue.mascara.length > 0) {
                        tmpValor = this.aplicarMascara(tmpValor, currentValue.mascara);
                    }
                    if (tmpValor != null) {
                        const val = previousValue + sep + tmpValor;
                        if (sep === '') {
                            sep = this.optionsLabel.separador;
                        }
                        return val;
                    }
                    return '';
                }, '');
            }
        }
        if (valor != null && (valor + '').length > 0) {
            return this.mascara != null && this.mascara.length > 0 ? (valor + '').aplicarMascara(this.mascara) : valor;
        }
        return '';

    }

    private aplicarMascara(valor: string, mascara: string): string {
        if (valor != null) {
            const val = (valor + '');

            if (mascara === 'cpfCnpj') {
                return val.aplicarMascara(val.length <= 14 ? this.MASK_CPF : this.MASK_CNPJ);
            } else {
                return val.aplicarMascara(mascara);
            }
        }
        return null;
    }

    private removerMascara(valor: string): string {
        const exp = /\-|\.|\/|\(|\)/g;
        return valor !== undefined ? valor.replace(exp, '') : '';
    }

}

export interface FiltroAutoComplete {
    parametros: any[];
    filtro?: string;
}

export interface LabelAutoComplete {
    keys: { key: string, mascara?: string }[];
    separador: string;
}
