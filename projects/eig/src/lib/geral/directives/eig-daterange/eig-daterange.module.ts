import {ModuleWithProviders, NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../../SharedModule';
import {EigDaterangeComponent} from './eig-daterange';

const providers = [];

@NgModule({
    imports: [
        SharedModule.forRoot()
        , ReactiveFormsModule
        // , BsDatepickerModule.forRoot()
    ],
    declarations: [
        EigDaterangeComponent
    ],
    exports: [
        EigDaterangeComponent
    ]
})
export class EigDaterangeModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: EigDaterangeModule,
            providers: [...providers]
        };
    }
}
