import {Component, forwardRef, Input, ViewChild} from '@angular/core';
import {AbstractControl, ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALUE_ACCESSOR, Validators} from '@angular/forms';
import {EigValidadores} from '../../helpers/eig.validadores';

@Component({
    selector: 'eig-daterange',
    templateUrl: './templates/daterange.html',
    styleUrls: [],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => EigDaterangeComponent),
        multi: true
    }]
})
export class EigDaterangeComponent implements ControlValueAccessor {

    @Input()
    placeholderIni: string;

    @Input()
    placeholderFim: string;

    @Input()
    cssColuna: string;

    @Input()
    label: string;

    @ViewChild('ini')
    campoIni;

    @ViewChild('fim')
    campoFim;

    formulario: FormGroup;

    dataRange: DataRange;

    msgErro: string;

    idini: string;
    idfim: string;


    private propagateChange = (_: any) => {
    };


    constructor(private fb: FormBuilder) {
        this.formulario = fb.group({});
        this.idini = Math.floor((Math.random() * 1000) + 1) + '';
        this.idfim = Math.floor((Math.random() * 1000) + 1) + '';

        this.formulario.statusChanges.subscribe(function (campo) {
            if (this.formulario.controls['dataIni'] && this.formulario.controls['dataFim']) {
                if (this.formulario.valid) {
                    this.propagateChange({
                        dataIni: this.formulario.controls['dataIni'].value,
                        dataFim: this.formulario.controls['dataFim'].value
                    });
                } else {
                    const erros = Object.assign({}, this.formulario.controls['dataIni'].errors, this.formulario.controls['dataFim'].errors);
                    if (erros['obrigatorio']) {
                        this.msgErro = erros.obrigatorio.mensagem;
                        this.propagateChange(null);
                    } else {
                        this.msgErro = 'Per\u00EDodo inv\u00E1lido';
                    }
                }
            }
        }.bind(this));

        /*        MainComponent.scriptsCarregados.asObservable().subscribe(() => {
                    this.rodaScript();
                });*/

        this.dataRange = {
            dataIni: null,
            dataFim: new Date()
        };
    }

    writeValue(value: DataRange) {
        if (value != null) {
            this.dataRange = value;
        }
        this.criarControls();
        setTimeout(function () {
            this.rodaScript();
        }.bind(this), 500);

        this.placeholderFim = this.placeholderFim != null ? this.placeholderFim : 'Data final';
        this.placeholderIni = this.placeholderIni != null ? this.placeholderIni : 'Data inicial';
    }

    private criarControls() {

        const validador = function (control: AbstractControl) {
            return new Promise(resolve => {
                    if (this.formulario.controls['dataIni'] != null && this.formulario.controls['dataFim'] != null &&
                        (this.formulario.controls['dataIni'].value == null || this.formulario.controls['dataFim'].value == null)) {
                        resolve({
                            obrigatorio: {
                                mensagem: 'Ambas as datas são obrigatórias'
                            }
                        });
                    }
                    resolve(null);
                }
            );
        }.bind(this);


        this.formulario.addControl('dataIni', new FormControl(this.dataRange.dataIni));
        this.formulario.addControl('dataFim', new FormControl(this.dataRange.dataFim));

        this.formulario.controls['dataIni'].setAsyncValidators(
            Validators.composeAsync([validador, EigValidadores.dataMenor(() => this.formulario.controls['dataFim'].value)]));

        this.formulario.controls['dataFim'].setAsyncValidators(
            Validators.composeAsync([validador, EigValidadores.dataMaior(() => this.formulario.controls['dataIni'].value)]));


    }


    private rodaScript(): void {
        const contexto = this;


        const campoini = $('#' + this.idini);
        const campofim = $('#' + this.idfim);

        const opts = {
            format: 'dd/mm/yyyy',
            autoclose: true,
            keepEmptyValues: true,
            immediateUpdates: true
        };

        const setFinal = function (valor) {
            contexto.formulario.controls['dataFim'].setValue(valor, {emitEvent: false});
            contexto.formulario.controls['dataFim'].updateValueAndValidity({onlySelf: true});
            contexto.formulario.controls['dataIni'].updateValueAndValidity({onlySelf: true});
        };

        const setInicio = function (valor) {
            contexto.formulario.controls['dataIni'].setValue(valor, {emitEvent: false});
            contexto.formulario.controls['dataFim'].updateValueAndValidity({onlySelf: true});
            contexto.formulario.controls['dataIni'].updateValueAndValidity({onlySelf: true});
        };

        // @ts-ignore
        campoini.datepicker('clearDates', opts);


        // @ts-ignore
        campofim.datepicker('clearDates', opts);

        campoini.on('changeDate', function () {
            // @ts-ignore
            const dateIni = $(this).data('datepicker').getDate();
            const dateFim = campofim.data('datepicker').getDate();


            // @ts-ignore
            campofim.data('datepicker').setStartDate(dateIni);
            if (dateFim != null && dateFim < dateIni) {
                campofim.data('datepicker').setDate(dateIni);
            }

            setInicio(dateIni);
        });


        campofim.on('changeDate', function () {
            console.log('onchage', $(this).data('datepicker').getDate());
            // @ts-ignore
            setFinal($(this).data('datepicker').getDate());
        });

        campofim.on('clearDate', function () {
            console.log('clearDate');
        });

        campoini.on('blur', function () {
            if ($(this).val() === '') {
                setInicio(null);
            }
        });

        campofim.on('blur', function () {
            if ($(this).val() === '') {
                setFinal(null);
            }
        });
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched(fn) {
    }

    validate(control) {
        // Custom logic to validate the parent control. In this case,
        // we may choose to union all childrens' errors.

        // const errors = Object.assign(this.localControl.errors || {}, this.remoteControl.errors || {});
        // return Object.keys(errors).length ? errors : null;
    }
}

export interface DataRange {
    dataIni: Date;
    dataFim: Date;
}
