import {Component, OnInit} from '@angular/core';
import {EigMenuService} from '../eig-menu/service/menuService';
import {AutenticacaoService} from '../../services';

@Component({
    selector: 'eig-header',
    templateUrl: './templates/header.html',
    styleUrls: ['./templates/header.css']
})
export class EigHeaderComponent implements OnInit {

    private _usuarioLogado: any;

    constructor(private menuService: EigMenuService, private autenticacaoService: AutenticacaoService) {
        this.autenticacaoService.dadosUsuarioLogado.asObservable().subscribe(value => {
            this._usuarioLogado = value;
        });

        this._usuarioLogado = this.autenticacaoService.usuarioLogado;
    }

    ngOnInit(): void {
        if (this._usuarioLogado == null) {
            $('#sidebar-toggle').trigger('click');
        }
    }

    logout(): void {
        this.autenticacaoService.logout();
    }

    toogle(): void {
        this.menuService.toogleSub.next(true);
    }

    get usuarioLogado(): any {
        return this._usuarioLogado;
    }

}
