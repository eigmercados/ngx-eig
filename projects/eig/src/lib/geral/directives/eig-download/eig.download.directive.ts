import {BsModalService} from 'ngx-bootstrap/modal';
import {ComponentFactoryResolver, Directive, HostListener, Input, OnInit, ViewContainerRef} from '@angular/core';
import {EigModalService} from '../eig-modal/service/eigmodalService';
import * as _ from 'lodash';


@Directive({
    selector: '[eigDownload]',
    providers: [EigModalService]
})
export class EigDownloadDirective implements OnInit {

    // @Output() eigDownload: EventEmitter<any> = new EventEmitter();
    @Input() eigDownload: string;
    private parentComponent: any;
    private funcao: Function;


    constructor(private viewContainerRef: ViewContainerRef, private eigModalService: EigModalService, private modalService: BsModalService, private resolver: ComponentFactoryResolver) {
        this.parentComponent = viewContainerRef;
        this.parentComponent = this.parentComponent._view.component;

    }

    ngOnInit(): void {
        console.log('init', this.eigDownload);
        this.bindFuncao();
    }

    @HostListener('click', ['$event'])
    onClick(e) {
        // const resultado = this.download();
        this.funcao().then(resultado => {
            console.log('resultado', resultado);
            EigDownloadDirective.createAndDownloadBlobFile(resultado.base64, {type: resultado.contentType}, resultado.nome);
        });
    }

    private bindFuncao() {
        if (this.eigDownload != null && this.parentComponent != null) {

            let tmpObj;
            // recupera a funcao
            const tmp = this.eigDownload.split('.');
            tmpObj = tmp.reduce((p, c) => {
                if (_.isFunction(p[c])) {
                    return p[c].bind(p);
                } else {
                    return p[c];
                }
            }, this.parentComponent);

            this.funcao = tmpObj;
        }
    }

    public static createAndDownloadBlob(blob: Blob, filename: string) {
        if (navigator.msSaveBlob) {
            // IE 10+
            navigator.msSaveBlob(blob, filename);
        } else {
            const link = document.createElement('a');
            // Browsers that support HTML5 download attribute
            if (link.download !== undefined) {
                const url = URL.createObjectURL(blob);
                link.setAttribute('href', url);
                link.setAttribute('download', filename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }

    public static createAndDownloadBlobFile(body, options, filename) {

        const blob = EigDownloadDirective.getBlob(body, options);

        EigDownloadDirective.createAndDownloadBlob(blob, filename);

    }

    private static getBlob(body, options): Blob {
        try {
            return new Blob([body], options);
        } catch (e) {
        }

        const byteCharacters = atob(body);
        const byteNumbers = new Array(body.length);
        for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        return new Blob([byteArray], options);
    }
}

export interface IDownload {
    base64: string;
    contentType: string;
    nome: string;
}

