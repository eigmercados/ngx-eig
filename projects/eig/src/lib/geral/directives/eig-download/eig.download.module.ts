import {ModuleWithProviders, NgModule} from '@angular/core';
import {EigDownloadDirective} from './eig.download.directive';

const providers = [];

@NgModule({
  imports: [
  ],
  declarations: [
    EigDownloadDirective
  ],
  exports: [
    EigDownloadDirective
  ]
})
export class EigDownloadModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: EigDownloadModule,
      providers: [...providers]
    };
  }
}
