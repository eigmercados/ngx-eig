import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {ComponentFactoryResolver, Directive, EventEmitter, HostListener, OnInit, Output} from '@angular/core';
import {EigModalService} from '../eig-modal/service/eigmodalService';

@Directive({
  selector: '[eigConfirm]',
  providers: [EigModalService]
})
export class EigConfirmDirective implements OnInit {

  @Output() eigConfirm: EventEmitter<any> = new EventEmitter();
  private _mensagem = 'Confirma?';
  private bsModalRef: BsModalRef;

  constructor(private eigModalService: EigModalService, private modalService: BsModalService, private resolver: ComponentFactoryResolver) {
  }

  ngOnInit(): void {

  }

  @HostListener('click', ['$event'])
  onClick(e) {
    /*  this.bsModalRef = this.modalService.show(EigModalComponent);
      this.bsModalRef.content.titulo = 'Confirmar?';
      this.bsModalRef.content.texto = this._mensagem;*/

    this.eigModalService.openModal({
      titulo: 'Confirma\u00E7\u00E3o',
      mensagem: 'Confirmar a a\u00E7\u00E3o?',
      botoes: [{
        label: 'Sim',
        classes: ['btn', 'btn-success'],
        iconClasses: ['ico', 'ico-thumbs-up'],
        tipo: 'button',
        funcao: function () {
          this.eigConfirm.next();
          this.eigModalService.closeModal();
        }.bind(this)
      },
        {
          label: 'N\u00E3o',
          tipo: 'button',
          classes: ['btn', 'btn-danger'],
          iconClasses: ['ico', 'ico-thumbs-down'],
          funcao: this.eigModalService.closeModal
        }]
    });
  }
}

