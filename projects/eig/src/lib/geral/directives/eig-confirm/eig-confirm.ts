import {ModuleWithProviders, NgModule} from '@angular/core';
import {EigConfirmDirective} from './eig.confirm.directive';
import {ModalSharedModule} from '../../../ModalSharedModule';

const providers = [];

@NgModule({
    imports: [
        ModalSharedModule.forRoot()
    ],
    declarations: [
        EigConfirmDirective
    ],
    exports: [
        EigConfirmDirective
    ]
})
export class EigConfirmModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: EigConfirmModule,
            providers: [...providers]
        };
    }
}
