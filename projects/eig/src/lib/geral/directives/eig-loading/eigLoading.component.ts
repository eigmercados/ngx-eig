import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {EigLoadingService} from './service/eigLoadingService';
import {Observable} from 'rxjs';

@Component({
  selector: 'eigLoading',
  templateUrl: './template/eigLoading.component.html',
  styleUrls: ['./template/eigLoading.component.css']
})
export class EigLoadingComponent implements OnInit {

  @ViewChild('corpo', {read: ViewContainerRef})
  corpoViewContainerRef;

  constructor() {
  }

  titulo: string;
  texto: string;

  service: EigLoadingService;

  subject: Observable<any>;


  ngOnInit() {
    this.titulo = this.titulo || 'Carregando...';
  }
}
