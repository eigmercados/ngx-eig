import {ComponentRef, EventEmitter, Injectable} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {EigLoadingComponent} from '../eigLoading.component';
import {Subject} from 'rxjs';
import {EigAppModule} from '../../../../eig.app.module';


@Injectable()
export class EigLoadingService {

    private subject = new Subject<any>();

    private isOpened = false;

    private eigModalRef: BsModalRef;

    public onShown: EventEmitter<any>;

    constructor(private modalService: BsModalService) {
        this.onShown = modalService.onShown;

        EigAppModule.errorListener.asObservable().subscribe(() => this.close());
    }

    show(): void {
        eval(`HoldOn.open({
            theme: 'sk-rect',
            //message:"<h4>"+themeName+" Demo. Closing in 2 seconds</h4>"
        })`);
    }

    showComMensagem<T>(): void | ComponentRef<T> {
        // console.log('tentando abrir loading', this.isOpened);

        if (!this.isOpened) {
            this.eigModalRef = this.modalService.show(EigLoadingComponent, {
                ignoreBackdropClick: true,
                keyboard: true,
                class: 'modal-sm gray'
            });

            this.eigModalRef.content.service = this;
            this.eigModalRef.content.subject = this.subject.asObservable();

            this.modalService.onShown.subscribe(() => {
                // console.log('loading aberto');
                this.isOpened = true;

                if (this.eigModalRef.content.subject != null) {
                    this.eigModalRef.content.subject.subscribe(msg => {
                        this.eigModalRef.content.texto = msg;
                    });
                }
            });
        }
    }

    adicionarMensagem(mensagem: string) {
        this.subject.next(mensagem);
    }

    close() {
        eval(' HoldOn.close();');
        // console.log('tentando fechar o loading', this.isOpened, this.eigModalRef);
        if (this.eigModalRef != null) {
            // this.modalService.hide(1);
            setTimeout(function () {
                this.eigModalRef.hide();
            }.bind(this), 800);


            this.modalService.onHidden.subscribe(() => {
                this.isOpened = false;
                // console.log('loading fechado');
                // console.log(this.eigModalRef);
            });

        }
    }
}
