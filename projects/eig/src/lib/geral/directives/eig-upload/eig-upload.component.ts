import {Component, EventEmitter, forwardRef, Input, OnChanges, Output, Renderer2, SimpleChanges, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {FileItem, FileLikeObject, FileUploader, ParsedResponseHeaders} from 'ng2-file-upload';
import {ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALUE_ACCESSOR, Validators} from '@angular/forms';
import {AutenticacaoService, TradutorService} from '../../services';
import {AlertService} from '../../alert/alert-service';
import {isObject} from 'util';
import * as _ from 'lodash';


@Component({
    selector: 'eigUpload',
    templateUrl: './templates/eigupload.component.html',
    styleUrls: ['./templates/eigupload.component.css'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => EigUploadComponent),
        multi: true
    }]
})
export class EigUploadComponent implements ControlValueAccessor, OnChanges {

    id: string;
    formulario: FormGroup;

    @ViewChild('fileupload')
    fileupload;

    @Input()
    enviarItens: Map<string, any>;

    @Input()
    url: string;

    @Input()
    allowedMimeType: string[];

    @Input()
    fileSize: number;

    @Input()
    initLabel: string;

    @Input()
    editLabel: string;

    @Input()
    multiple = false;

    @Input()
    enviar: Subject<Subject<boolean>>;

    @Output()
    onCompleteEvent: EventEmitter<boolean> = new EventEmitter();

    @Output()
    onRemoveItem: EventEmitter<string> = new EventEmitter<string>();

    @Input()
    autenticado = false;

    @Input()
    autoUpload = false;

    @Input()
    modalFeedback = false;

    private envioCompleto: Subject<boolean>;

    upload: FileUploader;
    arquivoSrc: { url: string, tipo: string, erroTipo: boolean, erroEnvio: boolean } = {
        url: null,
        tipo: null,
        erroTipo: false,
        erroEnvio: false
    };

    itemUpload: FileItem;

    itensEnviados: FileItem[] = [];

    msgErros: Map<string, string>;
    msgErro: string;

    todosItens: FileItem[] = [];

    constructor(private fb: FormBuilder, renderer: Renderer2,
                private alertService: AlertService, private tradutorService: TradutorService,
                private autenticacaoService: AutenticacaoService) {


        // this.loaded = new Subject();
        this.upload = new FileUploader({isHTML5: true});

        /*this.loaded.asObservable().subscribe(prot => {
            this.protocolo = prot;
            this.init();
        });*/

        // loadJS(renderer, './assets/js/upload-file.js');

        this.formulario = fb.group({});
        this.id = Math.floor((Math.random() * 1000) + 1) + '';

        this.formulario.valueChanges.subscribe(function () {
            setTimeout(function () {
                this.propagateChange(this.formulario.controls['arquivo'].value);
            }.bind(this), 300);

        }.bind(this));
        this.msgErros = new Map<string, string>();
        this.msgErros.set('mimeType', 'eigUpload.tipo_invalido');
        this.msgErros.set('fileSize', 'eigUpload.tamanho_maximo');
        this.msgErros.set('erroEnvio', 'eigUpload.erro_envio');
    }

    writeValue(value: {}[]) {

        this.criarControls();
        this.init();
    }

    private propagateChange = (_: any) => {
    };

    ngOnChanges(changes: SimpleChanges) {
        // console.log('input changed', changes);
        if (changes.url != null && changes.url.firstChange) {
            this.writeValue(null);
        }

        if (changes.enviar != null && changes.enviar.firstChange) {
            changes.enviar.currentValue.subscribe(value => {
                this.envioCompleto = value;
                this.uploadManual();
            });
        }
    }

    private criarControls() {

        this.formulario.addControl('arquivo', new FormControl(null, Validators.required));
    }


    private init() {

        this.setOptions();

        this.upload.onAfterAddingFile = function (fileItem: FileItem) {
            // this.adicionarControl(fileItem);
            fileItem.withCredentials = false;
            this.msgErro = null;
            this.todosItens.push(fileItem);
        }.bind(this);

        this.upload.onBeforeUploadItem = function (fileItem: FileItem) {
            this.arquivoSrc.erroTipo = false;
            this.arquivoSrc.erroEnvio = false;
        }.bind(this);

        this.upload.onWhenAddingFileFailed = function (item: FileLikeObject, filter: any, options: any) {
            console.log('onWhenAddingFileFailed', item, filter, options);
            this.arquivoSrc.erroTipo = true;
            const msg = this.msgErros.get(filter.name);
            if (msg != null) {
                this.msgErro = this.tradutorService.traduzir(msg);
            } else {
                this.msgErro = filter.name;
            }

            // @ts-ignore
            $(this.fileupload.nativeElement).fileupload('clear');
            this.remover(item);
        }.bind(this);

        this.upload.onCancelItem = function (item: FileItem, response: string, status: number, headers: ParsedResponseHeaders) {
            this.msgErro = null;
            this.remover(item);
        };

        this.upload.onSuccessItem = function (item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
            console.log('onSuccessItem', response);

            if (this.modalFeedback) {
                this.alertService.sucessoModal(`${this.tradutorService.traduzir('sucesso')}!`,
                    this.tradutorService.traduzir('arquivo_enviado_sucesso'));
            }

            if (this.envioCompleto != null) {
                this.envioCompleto.next(true);
            }
            this.onCompleteEvent.emit(true);
        }.bind(this);

        this.upload.onErrorItem = function (item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
            console.log('onErrorItem', response);
            if (this.modalFeedback) {
                this.alertService.erroModal(`${this.tradutorService.traduzir('erro')}!`,
                    this.tradutorService.traduzir(this.msgErros.get('erroEnvio')));
            }

            if (this.envioCompleto != null) {
                this.envioCompleto.next(false);
            }
            this.onCompleteEvent.emit(false);
        }.bind(this);

    }

    /**
     * seta as opcoes do uploader,  caso true, o autoUpload funcionarah
     */
    private setOptions(): void {
        this.upload.setOptions({
            isHTML5: true,
            url: this.url,
            formatDataFunction: function (a) {

                const formData = new FormData();

                if (this.enviarItens != null) {
                    this.enviarItens.forEach((value, key) => {
                        formData.append(key, new Blob([isObject(value) ? JSON.stringify(value) : value], {type: 'application/json'}));
                    });
                }

                formData.append('arquivo', a.file.rawFile);
                return formData;
            }.bind(this),
            disableMultipart: true,
            autoUpload: this.autoUpload,
            method: 'POST',
            allowedMimeType: this.allowedMimeType,
            removeAfterUpload: true,
            maxFileSize: (this.fileSize != null ? this.fileSize : 10) * 1000000, // 10MB
            // authToken: `Bearer ${this.autenticacaoService.getToken()}`,
            headers: [{name: 'charset', value: 'UTF-8'}]
        });

        if (this.autenticado) {
            this.upload.options.authToken = `Bearer ${this.autenticacaoService.getToken()}`;
        }
    }

    uploadManual(): void {

        this.setOptions();

        this.upload.uploadAll();
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched(fn) {
    }

    removerItem(event, item: FileItem): void {
        event.preventDefault();
        try {
            item.remove();
        } catch (e) {
            this.remover(item);
        }
        this.onRemoveItem.emit(item.file.name);
        $(event.target).parents('span:eq(0)').remove();
    }

    private remover(item: FileItem): void {
        _.remove(this.todosItens, valor => _.isEqual(valor, item));
    }
}
