export const pt = {
    'eigUpload': {
        'tipo_invalido': 'Tipo de arquivo inv\u00E1lido',
        'erro_envio': 'Erro ao enviar o arquivo',
        'tamanho_maximo': 'Tamanho m\u00E1ximo do arquivo atingido',
        'arquivo_enviado_sucesso': 'Arquivo enviado com sucesso!'
    }
};
