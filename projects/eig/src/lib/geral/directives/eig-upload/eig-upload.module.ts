import {ModuleWithProviders, NgModule} from '@angular/core';
import {FileUploadModule} from 'ng2-file-upload';

import * as pt from './i18n/pt';
import * as en from './i18n/en';
import {EigUploadComponent} from './eig-upload.component';
import {FormSharedModule} from '../../../FormSharedModule';
import {TradutorService} from '../../services';


const providers = [];

@NgModule({
    imports: [
        FormSharedModule.forRoot()
        , FileUploadModule
        // , BrowserAnimationsModule
    ],
    declarations: [
        EigUploadComponent
    ],
    exports: [
        EigUploadComponent
    ]
})
export class EigUploadModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: EigUploadModule,
            providers: [...providers]
        };
    }

    constructor(tradutorService: TradutorService) {
        tradutorService.addTraducoes({locale: 'pt', traducao: pt.pt}, {locale: 'en', traducao: en.en});
    }
}
