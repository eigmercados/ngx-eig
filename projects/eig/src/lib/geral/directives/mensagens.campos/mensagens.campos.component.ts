import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {CampoValidador, MensagemValidadores} from '../../../modelsuporte/IValidador';

@Component({
    selector: 'eig-mensagens-campo',
    templateUrl: './mensagens.campos.component.html',
    styleUrls: ['./mensagens.campos.component.css']
})
export class MensagensCamposComponent implements OnInit {

    @Input()
    private formulario: FormGroup;
    @Input()
    private campo: string;
    @Input()
    private validadores: CampoValidador[];
    @Input()
    css: string;


    mensagens: string[] = [];

    private erros = [];
    private errosProcessados = [];
    private labelCampo: string;

    constructor() {

        this.erros['required'] = Validators.required;
        this.erros['maxlength'] = Validators.maxLength(1);
        this.erros['minlength'] = Validators.minLength(1);
        this.erros['email'] = Validators.email;
        this.erros['pattern'] = Validators.pattern;
        this.erros['nullvalidator'] = Validators.nullValidator;
        this.erros['min'] = Validators.min(1);
        this.erros['max'] = Validators.max(1);

    }

    ngOnInit() {

        this.css = this.css == null ? 'help-block with-errors' : this.css;

        setTimeout(function () {
            // console.log('init mensagens');
            this.obterMensagens();

            this.formulario.valueChanges.subscribe(function (campo) {
                let tmp = [];
                for (let key in this.formulario.controls) {
                    const control = this.formulario.controls[key];
                    if (control.invalid) {
                        tmp.push({campo: key, control: control});
                    }
                }

                console.log('campos invalidos', tmp);

            }.bind(this));


            const control = this.getControl();
            if (control != null) {
                control.statusChanges.subscribe(status => {
                    console.log('status', control.dirty, control.pristine, control.status, control.value);
                    this.mensagens = [];
                    if (status === 'INVALID' && control.dirty) {
                        const errors = control.errors;
                        console.log('erros', control, control.value, control.errors, control.status);
                        if (errors) {
                            for (const err in errors) {
                                this.mensagens.push(this.getMensagemErro(err, errors));
                            }
                        }
                    }
                });
            }
        }.bind(this), 600);
    }

    private obterMensagens() {
        const validador: CampoValidador = this.obterValidador();
        if (validador) {
            this.labelCampo = validador._label;
            const tmp = [];
            validador.validadores.forEach(function (itemV: MensagemValidadores) {
                const mensagem = null;
                for (const key in this.erros) {
                    if (this.erros[key].toString() == itemV.validador.toString()) {
                        tmp[key] = itemV.mensagem;
                        break;
                    }
                }
            }.bind(this));
            // console.log('erros antes',this.erros);
            this.errosProcessados = tmp;
            // console.log('erros despois',this.errosProcessados);
        }
    }

    private getMensagemErro(errorCode: string, errors: ValidationErrors): string {
        console.log('getMensagemErro', errors, errorCode, errors[errorCode]);
        let mensagem = this.errosProcessados[errorCode];
        try {
            if (errorCode == 'maxlength' || errorCode == 'minlength') {
                mensagem = mensagem.format(errors[errorCode].requiredLength);
            } else if (errorCode == 'min') {
                mensagem = 'O valor n\u00E3o pode ser inferior a {0}';
                mensagem = mensagem.format(errors[errorCode].min);
            } else if (errorCode == 'max') {
                mensagem = 'O valor n\u00E3o pode ser maior a {0}';
                mensagem = mensagem.format(errors[errorCode].min);
                mensagem = mensagem.format(errors[errorCode].max);
            } else if (errorCode == 'required') {
                mensagem = mensagem.format(this.labelCampo);
            } else if (errors[errorCode].mensagem) {
                mensagem = errors[errorCode].mensagem;
            }
        } catch (e) {
        }
        return mensagem;
    }

    private obterValidador(): CampoValidador {
        return this.validadores.find(function (item: CampoValidador) {
            return item._campoMensagem === this.campo;
        }.bind(this));
    }

    private getControl(): AbstractControl {
        return this.formulario.controls[this.campo];
    }
}
