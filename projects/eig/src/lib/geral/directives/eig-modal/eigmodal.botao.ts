import {Component} from '@angular/core';
import {BtnModal} from './eigmodal.component';

@Component({
  selector: 'eigBtnModal',
  template: `<button [type]="tipo" [class]="classe" [disabled]="disabled" (click)="funcao()">
            {{label}} <i [class]="icon"></i>
          </button>`

})
export class EigModalBotao {

  conf: BtnModal;

  constructor() {
  }

  funcao() {
    // console.log('chamou funcao', this.conf);
    if (this.conf && this.conf.funcao != null) {
      this.conf.funcao();
    }
  }

  get disabled() {
    return this.conf && this.conf.fnDisabled ? this.conf.fnDisabled() : false;
  }

  get label() {
    return this.conf ? this.conf.label || '' : null;
  }

  get tipo() {
    return (this.conf != null) ? this.conf.tipo || 'button' : null;
  }

  get classe() {
    return this.conf && this.conf.classes ? this.conf.classes.reduce((previousValue, currentValue) => previousValue + ' ' + currentValue, '') : null;
  }

  get icon() {
    return this.conf && this.conf.iconClasses ? this.conf.iconClasses.reduce((previousValue, currentValue) => previousValue + ' ' + currentValue, '') : null;
  }
}
