import {ComponentFactoryResolver, ComponentRef, Injectable, Injector, Type} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {BtnModal, EigModalComponent} from '../eigmodal.component';
import {Subject} from 'rxjs';
import {Subscriber} from 'rxjs';
import {EigAppModule} from '../../../../eig.app.module';

@Injectable()
export class EigModalService {

    private eigModalRef: BsModalRef;

    public onShown: Subject<any>;

    constructor(private modalService: BsModalService) {
        EigAppModule.errorListener.asObservable().subscribe(() => this.closeModal());
    }

    openModal<T>(conf: OpenModal<T>): void | ComponentRef<T> {
        this.onShown = new Subject<any>();
        this.modalService.onShown.observers.forEach((obs: Subscriber<any>) => obs.unsubscribe());
        this.modalService.onShown.subscribe(resp => this.onShown.next());

        this.eigModalRef = this.modalService.show(EigModalComponent, {
            ignoreBackdropClick: false,
            keyboard: true,
            class: conf != null && conf.component && !conf.classSize ? 'modal-lg' : conf.classSize || 'modal-sm'
        });
        if (conf != null) {

            if (conf.componentFactoryResolver != null) {
                this.eigModalRef.content.resolver = conf.componentFactoryResolver;
            }

            this.eigModalRef.content.tituloHtml = conf.tituloHtml;

            this.eigModalRef.content.service = this;
            this.eigModalRef.content.titulo = conf.titulo;
            this.eigModalRef.content.texto = conf.mensagem || '';
            if (conf.botoes != null && conf.botoes.length > 0) {
                this.eigModalRef.content.addFooterButton(conf.botoes);
            }

            if (this.eigModalRef.content.init != null) {
                this.eigModalRef.content.init.next(true);
            }

            if (conf.component != null) {
                return this.eigModalRef.content.addBodyComponent(conf.component);
            }
        }
    }

    adicionarBotao(...botao: BtnModal[]) {
        this.eigModalRef.content.addFooterButton(botao);
    }

    adicionarHeader<T>(componente: Type<T>): void | ComponentRef<T> {
        return this.eigModalRef.content.addHeaderComponent(componente);
    }

    closeModal() {
        if (this.eigModalRef != null) {
            this.eigModalRef.hide();
            // console.log('observers', this.onShown.observers);
            this.onShown.observers.forEach((obs: Subscriber<any>) => obs.unsubscribe());
            // console.log('observers', this.onShown.observers, this.modalService.onShown);
            this.modalService.onShown.observers.forEach((obs: Subscriber<any>) => obs.unsubscribe());

        }
    }
}

export interface OpenModal<T> {
    titulo: string;
    tituloHtml?: string;
    component?: Type<T>;
    mensagem?: string;
    botoes?: BtnModal[];
    componentFactoryResolver?: ComponentFactoryResolver;
    injector?: Injector;
    classSize?: string;
}
