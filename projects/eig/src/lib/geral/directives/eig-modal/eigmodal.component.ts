import {Component, ComponentFactoryResolver, ComponentRef, ElementRef, OnInit, Type, ViewChild, ViewContainerRef} from '@angular/core';
import {EigModalService} from './service/eigmodalService';
import {EigModalBotao} from './eigmodal.botao';
import {ModalService} from '../../helpers/imodal-service';
import {Subject} from 'rxjs';
import 'jquery';


@Component({
    selector: 'eigModal',
    templateUrl: './template/eigmodal.component.html',
    styleUrls: ['./template/eigmodal.component.css']
})
export class EigModalComponent implements OnInit {

    @ViewChild('corpo', {read: ViewContainerRef})
    corpoViewContainerRef;

    @ViewChild('footer', {read: ViewContainerRef})
    footerViewContainerRef: ViewContainerRef;

    @ViewChild('header', {read: ViewContainerRef})
    headerViewContainerRef;

    @ViewChild('tituloHtmlEl')
    tituloHtmlEl: ElementRef;

    init: Subject<boolean>;

    mostrarHeaderPadrao = true;

    constructor(private _resolver: ComponentFactoryResolver) {
        this.init = new Subject<boolean>();
        this.init.asObservable().subscribe(() => {
            this.ngOnInit();
        });
    }

    titulo: string;
    texto: string;
    tituloHtml: string;


    service: EigModalService;

    ngOnInit() {
        console.log('tituloHtml', this.tituloHtmlEl);
        if (this.tituloHtml != null && this.tituloHtml.length > 0) {

            $(this.tituloHtmlEl.nativeElement).append($(`<div>${this.tituloHtml}</div>`));
        }
    }

    addFooterButton(botao: BtnModal[]): void {
        this.footerViewContainerRef.clear();
        botao.forEach(btn => this.addButton(btn));
    }

    private addButton(btn: BtnModal): void {

        const botaoRef = this.createBotaoRef();

        botaoRef.instance.conf = btn;
        botaoRef.instance.conf.funcao = btn.funcao.bind(this.service);

    }

    addBodyComponent<T extends ModalService<T>>(component: Type<T>): ComponentRef<T> {
        const componentRef: ComponentRef<T> = this.createComponent(component, this.corpoViewContainerRef);
        componentRef.instance._fromModal = true;
        return componentRef;
    }

    addHeaderComponent<T>(component: Type<T>): ComponentRef<T> {
        this.mostrarHeaderPadrao = false;
        return this.createComponent(component, this.headerViewContainerRef);
    }

    private createComponent<T>(component: Type<T>, container: ViewContainerRef): ComponentRef<T> {
        const factory = this._resolver.resolveComponentFactory(component);
        container.clear();
        return container.createComponent(factory);
    }

    private createBotaoRef(): ComponentRef<EigModalBotao> {
        const factory = this._resolver.resolveComponentFactory(EigModalBotao);
        const componentRef: ComponentRef<EigModalBotao> = this.footerViewContainerRef.createComponent(factory);
        return componentRef;
    }

    set resolver(resolver: ComponentFactoryResolver) {
        this._resolver = resolver;
    }

}

export interface BtnModal {
    label: string;
    classes?: string[];
    iconClasses?: string[];
    fnDisabled?: Function;
    tipo: string;
    funcao: Function;
}
