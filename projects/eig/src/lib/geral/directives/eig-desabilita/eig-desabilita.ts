import {Directive, ElementRef, Input, OnChanges} from '@angular/core';

@Directive({
    selector: '[eigDesabilita]',
})
export class EigDesabilitaDirective implements OnChanges {

    @Input()
    eigDesabilita = false;

    constructor(private el: ElementRef) {

    }

    ngOnChanges(changes) {
        // console.log('input changed', this.eigDesabilita);
        this.desabilitaFilhos(this.eigDesabilita);
    }

    private desabilitaFilhos(habilita: boolean): void {
        const lista = $(this.el.nativeElement).find(':input');
        if (!habilita) {
            lista.attr('disabled', 'disabled');
        } else {
            lista.removeAttr('disabled');
        }
    }
}

