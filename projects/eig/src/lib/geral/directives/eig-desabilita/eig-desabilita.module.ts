import {ModuleWithProviders, NgModule} from '@angular/core';
import {SharedModule} from '../../../SharedModule';
import {EigDesabilitaDirective} from './eig-desabilita';


const providers = [];

@NgModule({
    imports: [
        SharedModule.forRoot()
    ],
    declarations: [
        EigDesabilitaDirective
    ],
    exports: [
        EigDesabilitaDirective
    ]
})
export class EigDesabilitaModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: EigDesabilitaModule,
            providers: [...providers]
        };
    }
}
