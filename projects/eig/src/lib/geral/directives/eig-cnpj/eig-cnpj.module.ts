import {ModuleWithProviders, NgModule} from '@angular/core';
import {EigCnpjDirective} from './eig-cnpj.directive';
import {SharedModule} from '../../../SharedModule';

const providers = [];

@NgModule({
  imports: [
    SharedModule.forRoot()
  ],
  declarations: [
    EigCnpjDirective
  ],
  exports: [
    EigCnpjDirective
  ]
})
export class EigCnpjModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: EigCnpjModule,
      providers: [...providers]
    };
  }
}
