import {Directive, ElementRef, HostListener, OnInit} from '@angular/core';
import {NgControl, Validators} from '@angular/forms';

@Directive({
  selector: '[eigCpfCnpj]'
})
export class EigCnpjDirective implements OnInit {

  private MASK_CNPJ = '00.000.000/0000-00';
  private MASK_CPF = '000.000.000-00';
  private DEL_KEYS = [8, 46];
  private KEYS_PERMITIDAS = [35, 36, 37, 39].concat(this.DEL_KEYS);
  private CNPJ_INVALIDO = 'cnpj-invalido';
  private CPF_INVALIDO = 'cpf-invalido';

  constructor(private control: NgControl, private el: ElementRef) {
    // $(el.nativeElement).attr('maxlength', 18);
  }

  ngOnInit(): void {
    const validores = this.control.control.validator;
    this.control.control.setValidators(Validators.compose([validores, this.validar.bind(this)]));
    this.control.control.valueChanges.subscribe((resp) => {
      if (this.control.control.pristine) {
        console.log('control', resp, this.control.control);
        this.mascarar();
      }
    });
  }

  @HostListener('keydown', ['$event'])
  onKeyDown($event) {
    console.log('keydown', this.control.control.value, $event.keyCode);
    if ((this.KEYS_PERMITIDAS.indexOf($event.keyCode) != -1)
        || (this.control != null && this.control.control != null &&
        (this.control.control.value == null || this.control.control.value.removerMascara().length < 14))) {
      if (this.DEL_KEYS.indexOf($event.keyCode) != -1) {
        this.mascarar();
      }
      return true;
    }
    return false;
  }

  @HostListener('keyup', ['$event'])
  onKeyUp($event) {
    console.log('keyup', $event, this.control);

    if ($event.ctrlKey && $event.key === 'v') {
      if (!this.validaRegex()) {
        this.control.control.setValue('');
        return;
      }
    }
    if (this.control != null && this.control.control != null && this.control.control.value != null) {
      this.validaRegex();
      this.mascarar();
    }
  }

  private mascarar() {
    if (this.control != null && this.control.control != null && this.control.control.value != null) {
      this.control.control.setValue(this.control.control.value.aplicarMascara(this.control.control.value.length <= 14 ? this.MASK_CPF : this.MASK_CNPJ)
        , {
          onlySelf: true,
          emitEvent: false,
          emitModelToViewChange: true,
          emitViewToModelChange: false
        });
    }
  }

  private validaRegex() {
    if (this.control != null && this.control.control != null && this.control.control.value != null) {
      if (/\D+/g.test(this.control.control.value.replace(/[./-]+/g, ''))) {
        console.log('reprovado');
        this.control.control.setValue(this.control.control.value.substring(0, this.control.control.value.length - 1));
        return false;
      }
      return true;
    }
    return false;
  }

  private validar() {
    if (this.control != null && this.control.control != null && this.control.control.value != null) {

      if (this.control.control.value.length <= 14) {
        if (!this.validarCPF()) {
          const tmp = [];
          tmp[this.CPF_INVALIDO] = {mensagem: 'O CPF informado é inválido'};
          return tmp;
        }
      } else {
        if (!this.validarCNPJ()) {
          const tmp = [];
          tmp[this.CNPJ_INVALIDO] = {mensagem: 'O CNPJ informado é inválido'};
          return tmp;
        }
      }
    }
    return null;
  }

  private validarCNPJ(): boolean {
    let cnpj = this.control.control.value;

    cnpj = cnpj.replace(/[^\d]+/g, '');

    if (cnpj == '') return false;

    if (cnpj.length != 14)
      return false;

    // Elimina CNPJs invalidos conhecidos
    if (cnpj == '00000000000000' ||
      cnpj == '11111111111111' ||
      cnpj == '22222222222222' ||
      cnpj == '33333333333333' ||
      cnpj == '44444444444444' ||
      cnpj == '55555555555555' ||
      cnpj == '66666666666666' ||
      cnpj == '77777777777777' ||
      cnpj == '88888888888888' ||
      cnpj == '99999999999999')
      return false;

    // Valida DVs
    let tamanho: any = cnpj.length - 2;
    let numeros: any = cnpj.substring(0, tamanho);
    const digitos: any = cnpj.substring(tamanho);
    let soma = 0;
    let pos = tamanho - 7;
    for (let i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
        pos = 9;
    }
    let resultado: any = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
      return false;

    tamanho = tamanho + 1;
    numeros = cnpj.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (let i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
        pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
      return false;

    return true;

  }

  validarCPF() {
    let cpf = this.control.control.value;
    cpf = cpf.replace(/[^\d]+/g, '');

    if (cpf == null) {
      return false;
    }
    if (cpf.length != 11) {
      return false;
    }
    if ((cpf == '00000000000') || (cpf == '11111111111') || (cpf == '22222222222') || (cpf == '33333333333') || (cpf == '44444444444') || (cpf == '55555555555') || (cpf == '66666666666') || (cpf == '77777777777') || (cpf == '88888888888') || (cpf == '99999999999')) {
      return false;
    }
    let numero: number = 0;
    let caracter: string = '';
    let numeros: string = '0123456789';
    let j: number = 10;
    let somatorio: number = 0;
    let resto: number = 0;
    let digito1: number = 0;
    let digito2: number = 0;
    let cpfAux: string = '';
    cpfAux = cpf.substring(0, 9);
    for (let i: number = 0; i < 9; i++) {
      caracter = cpfAux.charAt(i);
      if (numeros.search(caracter) == -1) {
        return false;
      }
      numero = Number(caracter);
      somatorio = somatorio + (numero * j);
      j--;
    }
    resto = somatorio % 11;
    digito1 = 11 - resto;
    if (digito1 > 9) {
      digito1 = 0;
    }
    j = 11;
    somatorio = 0;
    cpfAux = cpfAux + digito1;
    for (let i: number = 0; i < 10; i++) {
      caracter = cpfAux.charAt(i);
      numero = Number(caracter);
      somatorio = somatorio + (numero * j);
      j--;
    }
    resto = somatorio % 11;
    digito2 = 11 - resto;
    if (digito2 > 9) {
      digito2 = 0;
    }
    cpfAux = cpfAux + digito2;
    if (cpf != cpfAux) {
      return false;
    }
    else {
      return true;
    }
  }

}
