import {ModuleWithProviders, NgModule} from '@angular/core';
import {EigCheckboxesDirective} from './eig-checkboxes.component';
import {FormSharedModule} from '../../../FormSharedModule';

const providers = [];

@NgModule({
  imports: [
    FormSharedModule.forRoot()
  ],
  declarations: [
    EigCheckboxesDirective
  ],
  exports: [
    EigCheckboxesDirective
  ]
})
export class EigCheckboxesModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: EigCheckboxesModule,
      providers: [...providers]
    };
  }
}
