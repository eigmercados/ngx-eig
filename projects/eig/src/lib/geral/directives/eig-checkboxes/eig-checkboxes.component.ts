import {Component, forwardRef, Input} from '@angular/core';
import {ICheckbox} from '../../helpers/iCheckbox';
import {ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALUE_ACCESSOR} from '@angular/forms';
import {EntidadeBasica} from '../../../modelsuporte/entidadeBasica';
import {isArray} from 'util';

@Component({
    selector: 'eig-checkboxes',
    templateUrl: './templates/checkboxes.html',
    styleUrls: ['./templates/checkboxes.css'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => EigCheckboxesDirective),
        multi: true
    }]
})
export class EigCheckboxesDirective implements ControlValueAccessor {

    checks: ICheckbox<EntidadeBasica>[];
    id: string;

    formulario: FormGroup;

    @Input('itens')
    set itens(itens: ICheckbox<EntidadeBasica>[]) {
        this.checks = itens;
        this.criarControls([]);
    }

    private propagateChange = (_: any) => {
    };


    constructor(private fb: FormBuilder) {
        this.formulario = fb.group({});
        this.id = Math.floor((Math.random() * 1000) + 1) + '';
    }

    writeValue(value: any) {
        if (value != null && isArray(value)) {
            // console.log('valores', value);
            this.checks = value;
            this.criarControls([]);
        }
    }

    private preparaItens() {
        if (this.checks && this.checks.length > 0) {
            this.checks = this.checks.map(function (valor) {
                valor.id = `${this.id}_${valor.entidade.codigo}`;
                return valor;
            }.bind(this));
        }
    }

    public criarControls(t: any) {
        if (this.checks && this.checks.length > 0) {
            this.formulario = this.fb.group({});

            this.formulario.valueChanges.subscribe(function (campo) {
                setTimeout(function () {
                    this.propagateChange(this.checks);
                }.bind(this), 300);

            }.bind(this));

            this.preparaItens();
            this.checks.forEach(item => {
                this.formulario.addControl(item.id, new FormControl(item.checked));
            });
        }
    }


    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched(fn) {
    }

    validate(control) {
        // Custom logic to validate the parent control. In this case,
        // we may choose to union all childrens' errors.

        // const errors = Object.assign(this.localControl.errors || {}, this.remoteControl.errors || {});
        // return Object.keys(errors).length ? errors : null;
    }
}
