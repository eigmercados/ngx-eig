export const pt = {
    'carregando': 'Carregando',
    'nome': 'Nome',
    'sistema': 'Sistema',
    'sistemas': 'Sistemas',
    'empresa': 'Empresa',
    'empresas': 'Empresas',
    'pessoa': 'Pessoa',
    'pessoas': 'Pessoas',
    'funcionalidade': 'Funcionalidade',
    'funcionalidades': 'Funcionalidades',
    'perfil': 'Perfil',
    'perfis': 'Perfis',
    'modalidades': 'Modalidades',
    'modalidade': 'Modalidade',
    'consulta': 'Consulta',
    'descricao': 'Descri\u00E7\u00E3o',
    'configuracoes': 'Configura\u00E7\u00F5es',
    'minhas_mensagens': 'Minhas mensagens',
    'sair': 'Sair',
    'procurar': 'Procurar',
    'filtrar_itens': 'Filtrar itens',
    'favoritos': 'Favoritos',
    'filtros_busca': 'Filtros para busca',
    'sigla': 'Sigla',
    'limpar': 'Limpar',
    'cadastrar': 'Cadastrar',
    'acessar': 'Acessar',
    'ou': 'ou',
    'novo': 'Novo',
    'excluir': 'Excluir',
    'editar': 'Editar',
    'voltar': 'Voltar',
    'Salvar': 'Salvar',
    'insira_cnpj': 'Insira o CNPJ',
    'informe_cnpj': 'Informe o CNPJ',
    'cnpj': 'CNPJ',
    'cpf': 'CPF',
    'cpf/cnpj': 'CPF/CNPJ',
    'informe_cpfcnpj': 'Informa o CPF/CNPJ',
    'telefone': 'Telefone',
    'informe_telefone': 'Informe o n\u00FAmero do telefone',
    'email': 'E-mail',
    'informe_email': 'Informe o E-mail',
    'cep': 'CEP',
    'informe_cep': 'Informe o CEP',
    'permissoes': 'Permiss\u00F5es',
    'estado': 'Estado',
    'informe_estado': 'Informe o Estado',
    'municipio': 'Munc\u00EDpio',
    'bairro': 'Bairro',
    'informe_bairro': 'Informe o Bairro',
    'logradouro': 'Logradouro',
    'informe_logradouro': 'Informe o Logradouro',
    'numero': 'N\u00FAmero',
    'informe_numero': 'Informe o n\u00FAmero',
    'informe_sigla': 'Informe a sigla',
    'complemento': 'Complemento',
    'informe_complemento': 'Informe o complemento',
    'usuario': 'Usu\u00E1rio',
    'usuarios': 'Usu\u00E1rios',
    'informe_usuario': 'Informe o login',
    'confirma_senha': 'Confirmar senha',
    'informe_confirma_senha': 'Confirme a senha do usu\u00E1rio',
    'selecione_sistema': 'Selecione o Sistema',
    'selecione_empresa': 'Selecione a Empresa',
    'selecione_estado': 'Selecione o Estado',
    'informe_senha': 'Informe a senha',
    'observacoes': 'Observa\u00E7\u00F5es',
    'informe_observacoes': 'Informe as observa\u00E7\u00F5es',
    'selecionar': 'Selecionar',
    'alterar': 'Alterar'

};
