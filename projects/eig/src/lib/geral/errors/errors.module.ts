import {NgModule, ErrorHandler} from '@angular/core';
import {ErrorsHandler} from './errors-handler';
import {ErrorsComponent} from './errors.component';
import {ErrorsService} from './service/errors.service';
import {ErrorRoutingModule} from './rotas/errors-rotas.module';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ErrorRoutingModule
  ],
  exports: [],
  declarations: [
    ErrorsComponent
  ],
  providers: [
    ErrorsService,
    {
      provide: ErrorHandler,
      useClass: ErrorsHandler
    }
  ],
})
export class ErrorsModule {
}
