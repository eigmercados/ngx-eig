import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {AlertService} from '../../geral/alert/alert-service';
import {Router} from '@angular/router';
import {ErrorsService} from './service/errors.service';
import {Response} from '@angular/http';
import {EigLoadingService} from '../../geral/directives/eig-loading/service/eigLoadingService';
import {Error} from 'tslint/lib/error';


@Injectable()
export class ErrorsHandler implements ErrorHandler {
  constructor(private injector: Injector) {
  }

  handleError(error: Error) {
    const alert = this.injector.get(AlertService);
    const errorService =  this.injector.get(ErrorsService);
    const router = this.injector.get(Router);
    const loading = this.injector.get(EigLoadingService);

    if (error instanceof Response) {
      console.error ('Error HTTP:', error);
      loading.close();
      // ALERTA ERRO DE CONEXÃO
      if (!navigator.onLine) {
        alert.erro('sem conexão com a internet');
        return false;
      }
      const body = error.json() || ''
      const err = body.error || JSON.stringify(body)
      // ALERTA OS ERROR DE REQUISIÇÃO HTTP ERROR
      alert.erro(`${error.url}: ${error.status}  - ${error.statusText || ''}  ${err}` );
      return false;
    }else {
      console.error('Error no Cliente da aplicação: ', error);
      errorService
        .sendlogError(error)
        .subscribe(contextErrorObj => {
          // NAVEGA PARA A PÁGINA DE ERROR
          router.navigate(['/error'], {queryParams: contextErrorObj});
        });
    }

  }

}
