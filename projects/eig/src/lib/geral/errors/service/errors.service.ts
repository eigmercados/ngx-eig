import {Injectable, Injector} from '@angular/core';
import {NavigationError, Router} from '@angular/router';
import {of} from 'rxjs';
import {LocationStrategy, PathLocationStrategy} from '@angular/common';


@Injectable()
export class ErrorsService {
  constructor(private router: Router, private injector: Injector) {
    this.router
      .events
      .subscribe((event) => {
        if (event instanceof NavigationError) {
          // Redirecionar para página e error
          this.sendlogError(event.error).subscribe((errorContextObj) => {
            this.router.navigate(['/error'], {queryParams : errorContextObj});
          });
        }
      });
  }


  sendlogError(error) {
    // ENVIA O ERRO PARA O SERVIDOR
    const errorToSend = this.addContextErrorInfo(error);
    // TODO: IMPLEMENTAR REQUEST PARA O SERVIDOR COM AS INFORMAÇÕES DOS ERROS AQUI E RETORNAR UM OBSERVADOR
    return of(errorToSend);
  }

  addContextErrorInfo(error) {
    const location = this.injector.get(LocationStrategy);
    const url = location instanceof PathLocationStrategy ? location.path() : '';
    const status = (error != null) ? error.status : null;
    const msg = (error != null) ? error.message : null;
    const errorObj = {url, status, msg};
    return errorObj;
  }
}
