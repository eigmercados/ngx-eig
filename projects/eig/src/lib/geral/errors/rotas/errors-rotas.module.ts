import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ErrorsComponent} from '../errors.component';

const errorRouting: Routes = [
  {
    path: 'error', component: ErrorsComponent
  },
  {
    path: '**', component: ErrorsComponent, data: { error: 404 }
  }
]

@NgModule({
  imports: [RouterModule.forChild(errorRouting)],
  exports: [RouterModule]
})
export class ErrorRoutingModule {}
