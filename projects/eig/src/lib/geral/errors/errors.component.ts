import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {EigLoadingService} from '../../geral/directives/eig-loading/service/eigLoadingService';
import {AutenticacaoService} from '../../geral/services';

@Component({
  selector: 'app-error',
  templateUrl: 'template/errors.component.html'
})

export class ErrorsComponent implements OnInit {
  public routeParams;
  public data;
  constructor(private activatedRoute: ActivatedRoute
              , private loading: EigLoadingService) {
  }


  ngOnInit() {
    this.routeParams = this.activatedRoute.snapshot.queryParams;
    this.data = this.activatedRoute.snapshot.data;
    this.data = (Object.keys(this.data).length > 0) ? this.data : null;
    this.loading.close();
  }

}
