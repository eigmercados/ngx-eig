import {ApoioServiceAbs} from '../services/apoio-support.service';
import {Observable} from 'rxjs';
import * as _ from 'lodash';
import {Injector, OnInit} from '@angular/core';
import {AlertService} from '../alert/alert-service';
import {TradutorService} from '../services';
import {TranslateService} from '@ngx-translate/core';

/**
 * Verifica se o usuario logado tem a Role especifica para a Funcionalidade
 * Caso a classe seja anotada e nao autorizada, sera redirecionado para a tela de permissao negada.
 * @param name
 * @param redirecionar - nao se aplica a Classes, apenas para metodos.
 */
export function Autorizado(name: string, redirecionar = false, callBack?: Function) {
    return function (target?: any, propertyKey?: string, descriptor?: PropertyDescriptor): any {

        // const apoioService: ApoioService = SharedModule.injector.get(ApoioService);
        // const alertService: AlertService = EigAppModule.alertService;

        function autoriza(metodoOriginal, args: any) {
            const apoioService: ApoioServiceAbs = this.apoioServiceAbs;
            const alertService: AlertService = this.alertService;

            const obs = Observable.create(function (observer) {
                apoioService.temPermissao(name).subscribe(acesso => {
                    observer.next(acesso.resultado);
                    observer.complete();
                });
            }.bind(this));

            obs.subscribe(function (autorizado) {
                if (autorizado) {
                    if (metodoOriginal != null) {
                        metodoOriginal.apply(this, args);
                    }
                } else {
                    if (callBack != null) {
                        callBack.apply(target, null);
                    } else {
                        if (redirecionar) {
                            apoioService.permissaoNegada();
                        } else {
                            alertService.erroModal('N\u00E3o autorizado!', 'Voc\u00EA n\u00E3o tem permiss\u00E3o para efetuar essa a\u00E7\u00E3o.', false);
                        }
                    }
                }
            }.bind(this));
        }

        if (descriptor != null) {
            const metodoOriginal = descriptor.value;

            descriptor.value = function (...args: any[]) {
                if (event) {
                    event.preventDefault();
                }

                autoriza.call(this, metodoOriginal, args);

            };
            return descriptor;
        } else {
            redirecionar = true;
            const novaClasse = class extends target {
                ngOnInit = _.bind(() => {
                    super.ngOnInit();
                    autoriza.call(this, null, null);
                }, target);
            };

            return novaClasse;

        }
    };
}


export function classDecorator<T extends { new(...args: any[]): {} }>(constructor: T) {
    return class extends constructor {
        newProperty = 'new property';
        hello = 'override';
        injector = null;
        alertService: null;
    };
}

export interface AnotacaoAware extends OnInit {
    injector: any;
    alertService: any;
    tradutorService: any;
    translateService: any;
}

/*
interface IMobileAwareDirective {
    injector: Injector;
    ngOnInit?: Function;
    ngOnDestroy?: Function;
}

export function MobileAwareListener(eventName) {
    return (classProto: IMobileAwareDirective, prop, decorator) => {
        if (!classProto['_maPatched']) {
            classProto['_maPatched'] = true;
            classProto['_maEventsMap'] = [...(classProto['_maEventsMap'] || [])];

            const ngOnInitUnpatched = classProto.ngOnInit;
            classProto.ngOnInit = function (this: IMobileAwareDirective) {
                const renderer2 = this.injector.get(Renderer2);
                const elementRef = this.injector.get(ElementRef);
                const eventNameRegex = /^(?:(window|document|body):|)(.+)/;

                for (const {eventName, listener} of classProto['_maEventsMap']) {
                    // parse targets
                    const [, eventTarget, eventTargetedName] = eventName.match(eventNameRegex);
                    const unlisten = renderer2.listen(
                        eventTarget || elementRef.nativeElement,
                        eventTargetedName,
                        listener.bind(this)
                    );
                    // save unlisten callbacks for ngOnDestroy
                    // ...
                }

                if (ngOnInitUnpatched)
                    return ngOnInitUnpatched.call(this);
            };
            // patch classProto.ngOnDestroy if it exists to remove a listener
            // ...
        }

        // eventName can be tampered here or later in patched ngOnInit
        classProto['_maEventsMap'].push({eventName, listener: classProto[prop]});
    };
}*/

