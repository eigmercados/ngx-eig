import {Constructor} from '../utils';
import {AnotacaoAware} from './permissao';


/**
 * Habilita a traducao no componente anotado
 * @param constructor
 */
export function Traduzir<T extends Constructor<AnotacaoAware>>(constructor: T) {


    const novaClasse = class extends constructor {
        ngOnInit() {
            if (this.tradutorService != null) {
                this.tradutorService.verificaLinguagem();
                this.translateService.use(this.tradutorService.localeCorrente());
            }

            super.ngOnInit();
        }

    };

    return novaClasse;

}
