export function logarTempoExecucao(emSegundos:boolean=false){
    return function(target: any, propertyKey:string,descriptor:PropertyDescriptor){
        let metodoOriginal=descriptor.value;
        descriptor.value=function(...args:any[]){
            let unidade="ms";
            let divisor=1;
            if(emSegundos){
                unidade="s"
                divisor=1000;
            };
            console.log(`decorator do metodo ${propertyKey}`);
            const t1 =performance.now();

            const retorno= metodoOriginal.apply(this,args);
            const t2= performance.now();
            console.log(`demorou ${(t1-t2)/divisor}${unidade}`);
            return retorno;
        }
        return descriptor;
    }
}