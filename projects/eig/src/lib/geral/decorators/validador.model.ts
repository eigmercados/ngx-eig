import {MensagemValidadores} from '../../modelsuporte/IValidador';

export function Validador(validador: ValidadorObj) {

  return function (target: any, key: string) {

    if (target._validadores == null) {
      target._validadores = [];
    }

    // console.log('target', target, target.constructor.name,key,Object.getPrototypeOf(target).constructor.name, target.constructor.name === Object.getPrototypeOf(target).constructor.name);
    target._validadores.push({
      campo: key,
      lookFor: validador.lookFor != null ? validador.lookFor : null,
      validadores: validador.validadores,
      _label: key,
      _construtor: target.constructor.name
    });
  };
}

export interface ValidadorObj {
  lookFor?: any;
  validadores?: MensagemValidadores[];
}
