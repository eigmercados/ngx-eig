export function domInject(seletor: string) {

  return function (target: any, key: string) {
    let elemento: any;

    const getter = function () {
      if (!elemento) {
        console.log(`buscando o elemento ${key}`)
        elemento = (seletor);
      }
      return elemento;
    }

    Object.defineProperty(target, key, {
      get: getter
    });
  }
}
