import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AutenticacaoService} from '../../../geral/services/autenticacao-service';
import {NotfoundComponent} from '../notfound.component';

const homeRoutes: Routes = [
    {
        path: '', component: NotfoundComponent,
        canActivate: [AutenticacaoService]
    }
];

@NgModule({
    imports: [RouterModule.forChild(homeRoutes)],
    exports: [RouterModule]
})
export class NotFoundRouteModule {
}
