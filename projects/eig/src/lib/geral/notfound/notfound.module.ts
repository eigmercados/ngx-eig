import {NgModule} from '@angular/core';
import {SharedModule} from '../../SharedModule';
import {NotFoundRouteModule} from './routes/rotas';
import {NotfoundComponent} from './notfound.component';


@NgModule({
    imports: [
        NotFoundRouteModule
        , SharedModule.forRoot()
    ],
    declarations: [
        NotfoundComponent
    ],
    entryComponents: [
    ]
})
export class NotfoundModule {
}
