import {Component, OnInit} from '@angular/core';
import {AutenticacaoService} from '../../geral/services/autenticacao-service';

@Component({
    selector: 'notfound',
    templateUrl: './templates/notfound.component.html',
    styleUrls: ['../../../assets/css/error-pages.css'],
    entryComponents: [],
    providers: []
})
export class NotfoundComponent implements OnInit {

    constructor(private autenticacaoService: AutenticacaoService) {
    }

    ngOnInit() {
    }

    isLogado(): boolean {
        return this.autenticacaoService.isLogado();
    }
}
