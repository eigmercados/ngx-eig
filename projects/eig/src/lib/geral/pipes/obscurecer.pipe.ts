import {Pipe, PipeTransform} from '@angular/core';


@Pipe({
    name: 'obscurecer'
})
export class ObscurecerPipe implements PipeTransform {

    transform(value: string, tipo: TipoObscuredor): string {
        return value != null && value.length > 0 ? this.transforma(value, tipo) : '';
    }

    private transforma(valor: string, tipo: TipoObscuredor): string {

        switch (tipo) {
            case 'telefone':
                return valor.replace(/\d{2}-\d{2}/g, '**-**');
            case 'e-mail':
                const tamanho = Math.round(valor.substring(0, valor.indexOf('@')).length * 0.4);
                const tamanho2 = Math.round(valor.substring(valor.indexOf('@'), valor.length).length * 0.4);

                const regex = new RegExp(`\\w{${tamanho}}@\\w{${tamanho2}}`, 'g');

                const inicio = function () {
                    return {inicio: this.getAsteriscos(tamanho), final: this.getAsteriscos(tamanho2)};
                }.bind(this)();

                return valor.replace(regex, `${inicio.inicio}@${inicio.final}`);
            default:
                const tl = (valor.length / 2);
                return valor.substring(0, tl * 0.2) + this.getAsteriscos(tl * .4) + valor.substring(tl, tl * .2);
        }
    }

    private getAsteriscos(tamanho: number): string {
        let tmp = '';
        for (let i = 0; i < tamanho; i++) {
            tmp += '*';
        }
        return tmp;
    }
}

export type TipoObscuredor = 'telefone' | 'e-mail' | 'outro';
