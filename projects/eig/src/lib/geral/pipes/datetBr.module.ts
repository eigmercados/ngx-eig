import {ModuleWithProviders, NgModule} from '@angular/core';
import {DateBrPipe} from './datebr.pipe';

const providers = [];

@NgModule({
    imports: [],
    declarations: [
        DateBrPipe
    ],
    exports: [
        DateBrPipe
    ]
})
export class DateBrModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: DateBrModule,
            providers: [...providers]
        };
    }
}
