import {Pipe, PipeTransform} from '@angular/core';


const MASK_CNPJ = '00.000.000/0000-00';
const MASK_CPF = '000.000.000-00';

@Pipe({
  name: 'cnpj'
})
export class CnpjPipe implements PipeTransform {

  transform(value: string, args?: string): string {
    const tmp = (value + '').padStart(14, '0');
    return tmp.aplicarMascara(MASK_CNPJ);
  }
}


@Pipe({
  name: 'cpf'
})
export class CpfPipe implements PipeTransform {

  transform(value: string, args?: string): string {
    const tmp = (value + '').padStart(11, '0');
    return tmp.aplicarMascara(MASK_CPF);
  }
}

@Pipe({
  name: 'cpfCnpj'
})
export class CpfCnpjPipe implements PipeTransform {

  transform(value: string, args?: string): string {
    const tmp = (value + '').length <= 11 ? ((value + '').padStart(11, '0')) : (value + '').padStart(14, '0');
    return tmp.aplicarMascara(tmp.length <= 11 ? MASK_CPF : MASK_CNPJ);
  }
}
