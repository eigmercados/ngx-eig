import {ModuleWithProviders, NgModule} from '@angular/core';
import {MascaraPipe} from './mask.pipe';

const providers = [];

@NgModule({
    imports: [],
    declarations: [
        MascaraPipe
    ],
    exports: [
        MascaraPipe
    ]
})
export class MaskModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: MaskModule,
            providers: [...providers]
        };
    }
}
