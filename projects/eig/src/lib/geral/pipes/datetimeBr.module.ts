import {ModuleWithProviders, NgModule} from '@angular/core';
import {DateTimeBrPipe} from './datebr.pipe';

const providers = [];

@NgModule({
  imports: [],
  declarations: [
    DateTimeBrPipe
  ],
  exports: [
    DateTimeBrPipe
  ]
})
export class DateTimeBrModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DateTimeBrModule,
      providers: [...providers]
    };
  }
}
