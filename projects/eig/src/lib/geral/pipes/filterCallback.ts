import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'filterCallback',
    pure: false
})
export class FilterCallback implements PipeTransform {
    transform(items: any[], callback: (item: any) => boolean): any {
        if (!items || !callback) {
            return items;
        }
        if (items.length > 0) {
            return items.filter(item => callback(item));
        }
        return items;
    }
}