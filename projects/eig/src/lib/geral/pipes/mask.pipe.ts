import {Pipe, PipeTransform} from '@angular/core';


@Pipe({
  name: 'mascara'
})
export class MascaraPipe implements PipeTransform {

  transform(value: string, mascara: string): string {
    return value != null && value.length > 0 ? value.aplicarMascara(mascara) : '';
  }
}
