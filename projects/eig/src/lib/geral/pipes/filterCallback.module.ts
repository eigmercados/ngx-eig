import {ModuleWithProviders, NgModule} from '@angular/core';
import {FilterCallback} from './filterCallback';

const providers = [];

@NgModule({
  imports: [],
  declarations: [
      FilterCallback
  ],
  exports: [
      FilterCallback
  ]
})
export class FilterCallbackModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: FilterCallbackModule,
      providers: [...providers]
    };
  }
}
