import {ModuleWithProviders, NgModule} from '@angular/core';
import {CnpjPipe, CpfCnpjPipe, CpfPipe} from './cnpj.pipe';

const providers = [];

@NgModule({
  imports: [],
  declarations: [
    CnpjPipe
    , CpfCnpjPipe
    , CpfPipe
  ],
  exports: [
    CnpjPipe
    , CpfCnpjPipe
    , CpfPipe
  ]
})
export class CnpjCpfModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CnpjCpfModule,
      providers: [...providers]
    };
  }
}
