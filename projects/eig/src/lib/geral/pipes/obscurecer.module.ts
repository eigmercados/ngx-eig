import {ModuleWithProviders, NgModule} from '@angular/core';
import {ObscurecerPipe} from './obscurecer.pipe';

const providers = [];

@NgModule({
    imports: [],
    declarations: [
        ObscurecerPipe
    ],
    exports: [
        ObscurecerPipe
    ]
})
export class ObscurecerModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ObscurecerModule,
            providers: [...providers]
        };
    }
}
