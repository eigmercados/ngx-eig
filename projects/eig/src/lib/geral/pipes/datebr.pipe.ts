import {Pipe, PipeTransform} from '@angular/core';
import {DatePipe} from '@angular/common';

@Pipe({
  name: 'dateBr'
})
export class DateBrPipe implements PipeTransform {

  transform(value: string, args?: string): string {
    const date = new DatePipe('pt');
    return date.transform(value, 'dd/MM/yyyy');
  }
}

@Pipe({
  name: 'dateTimeBr'
})
export class DateTimeBrPipe implements PipeTransform {

  transform(value: string, args?: string): string {
    const date = new DatePipe('pt');
    return date.transform(value, 'dd/MM/yyyy hh:mm');
  }

}

