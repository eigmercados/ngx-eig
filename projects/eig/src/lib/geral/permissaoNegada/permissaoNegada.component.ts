import {Component, OnInit} from '@angular/core';
import {AutenticacaoService} from '../../geral/services';

@Component({
    selector: 'denied',
    templateUrl: './templates/permissaoNegada.component.html',
    styleUrls: ['../../../assets/css/error-pages.css'],
    entryComponents: [],
    providers: []
})
export class PermissaoNegadaComponent implements OnInit {

    constructor(private autenticacaoService: AutenticacaoService) {
    }

    ngOnInit() {
    }

    sair(): void {
        this.autenticacaoService.logout();
    }
}
