import {NgModule} from '@angular/core';
import {SharedModule} from '../../SharedModule';
import {PermissaoNegadaComponent} from './permissaoNegada.component';
import {PermissaoNegadaRouteModule} from './routes/rotas';


@NgModule({
    imports: [
        PermissaoNegadaRouteModule
        , SharedModule.forRoot()
    ],
    declarations: [
        PermissaoNegadaComponent
    ],
    entryComponents: []
})
export class PermissaoNegadaModule {
}
