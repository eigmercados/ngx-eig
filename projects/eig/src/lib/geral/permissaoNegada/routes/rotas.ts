import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PermissaoNegadaComponent} from '../permissaoNegada.component';

const permissaoRoutes: Routes = [
    {
        path: '', component: PermissaoNegadaComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(permissaoRoutes)],
    exports: [RouterModule]
})
export class PermissaoNegadaRouteModule {
}
