import {ModuleWithProviders, NgModule} from '@angular/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {SharedModule} from './SharedModule';
import {FormSharedModule} from './FormSharedModule';

const providers = [];

@NgModule({
  imports: [
    SharedModule.forRoot()
    , FormSharedModule.forRoot()
    , NgxDatatableModule
  ],
  declarations: [],
  exports: [
    NgxDatatableModule
    , FormSharedModule
  ],
  entryComponents: []
})
export class ListSharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ListSharedModule,
      providers: [...providers]
    };
  }
}
