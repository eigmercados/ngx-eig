import {TranslateLoader} from '@ngx-translate/core';
import {Observable, of} from 'rxjs';
import {pt} from './geral/i18n/pt';
import {en} from './geral/i18n/en';
import {de} from './geral/i18n/de';

// @dynamic
export class CustomTranslateLoader implements TranslateLoader {

    private linguas: Map<string, {}>;

    static Translate(...i18n: I18n[]): Function {
        return () => {
            return new CustomTranslateLoader(...i18n);
        };
    }

    constructor(...i18n: I18n[]) {
        this.linguas = new Map<string, {}>();

        this.linguas.set('pt', pt);
        this.linguas.set('en', en);
        this.linguas.set('de', de);
        this.mergeTraducoes(...i18n);
    }

    public mergeTraducoes(...i18n: I18n[]) {
        if (i18n.length > 0) {
            i18n.forEach(valor => {
                const traducao = this.linguas.get(valor.locale);
                Object.assign(traducao, valor.traducao);
                this.linguas.set(valor.locale, traducao);
            });
        }
    }

    getTranslation(lang: string): Observable<any> {
        return of(this.linguas.get(lang));
    }

}

export interface I18n {
    locale: string;
    traducao: {};
}
