import {ModuleWithProviders, NgModule} from '@angular/core';
import {FormSharedModule} from './FormSharedModule';
import {EigModalComponent} from './geral/directives/eig-modal/eigmodal.component';
import {EigModalBotao} from './geral/directives/eig-modal/eigmodal.botao';
import {EigModalService} from './geral/directives/eig-modal/service/eigmodalService';

const providers = [
    EigModalService
];

@NgModule({
    imports: [
        FormSharedModule.forRoot()
    ],
    declarations: [
        EigModalComponent
        , EigModalBotao
    ],
    exports: [
        FormSharedModule
    ],
    entryComponents: [
        EigModalComponent
        , EigModalBotao
    ]
})
export class ModalSharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ModalSharedModule,
            providers: [...providers]
        };
    }
}
