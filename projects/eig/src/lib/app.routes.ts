import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainAppModule} from './componentes/main';
import {NotfoundModule, PaginaErroModule, PermissaoNegadaModule} from './geral';
import {LoginModule} from './componentes/login';
import {moduloIniciado} from './eig.app.module';


export const rotasSistemas: Routes = [];


/**
 * Rotas padroes
 * {[{path: string; component: LoginComponent} , {path: string; redirectTo: string}]}
 */
const routes: Routes = [
        {
            path: '',
            pathMatch: 'full',
            redirectTo: 'main'
        },
        {
            path: 'login', loadChildren: lazyModuleLoad(LoginModule)
        },
        {
            path: 'main', loadChildren: lazyModuleLoad(MainAppModule, moduloIniciado)
        },
        {
            path: 'denied', loadChildren: lazyModuleLoad(PermissaoNegadaModule)
        },
        {
            path: 'erro', loadChildren: lazyModuleLoad(PaginaErroModule)
        },
        {
            path: '**', loadChildren: lazyModuleLoad(NotfoundModule)
        }
    ]
;

routes.push(...(rotasSistemas != null ? rotasSistemas : []));

@NgModule({
    imports: [
        RouterModule.forRoot(
            routes
            /*, {
                preloadingStrategy: PreloadAllModules
            }*/)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {

    constructor() {
    }
}
