import {ModuleWithProviders, NgModule} from '@angular/core';
import {MensagensCamposComponent} from './geral/directives/mensagens.campos/mensagens.campos.component';
import {SharedModule} from './SharedModule';
import {ReactiveFormsModule} from '@angular/forms';

import 'jquery';

const providers = [];

@NgModule({
    imports: [
        SharedModule.forRoot()
        , ReactiveFormsModule
    ],
    declarations: [
        MensagensCamposComponent
    ],
    exports: [
        SharedModule
        , ReactiveFormsModule
        , MensagensCamposComponent
    ],
    entryComponents: []
})
export class FormSharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: FormSharedModule,
            providers: [...providers]
        };
    }
}
