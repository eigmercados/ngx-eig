export * from './eig.app.module';
export * from './FormSharedModule';
export * from './SharedModule';
export * from './ListSharedModule';
export * from './app.routes';
export * from './eig-config';
export * from './geral/index';
export * from './eig.app.component';
export * from './componentes/index';
export * from './modelsuporte/index';
export * from './CustomTranslateLoader';



